#ifndef _PDEF_H
# define _PDEF_H

#define MAX_BYTE 500000
#define VISIBLE_ITEMS  5
#define Max_Label_Width 100
#define Max_NumItems 40000
#define IDlen 40
#define DefaultSize 1500
#define DefaultSize10 5000

#define MAX_IF 10

typedef struct {
  char boardType[IDlen];
  char origin[IDlen],prefix[IDlen];
  char bus_slot[IDlen];
  unsigned int  boardspec[7];
  /*boardspec: BoardId, Serial(exluding prefix), Revision, asm_id, swVersion, designVersion, last: not used*/
  char ttyused[IDlen];
  /*int  boardNo;
  int  deviceNo;
  int  deviceId;
  unsigned int vendorId;
  int  serialNo;
  int  bus;
  int  slot;*/
  /*char fpgaType;
  char *teststat; 
  int RMsgini;*/
}robinInfo_t;

typedef struct {
  char if_name[50],if_hwadd[50],if_inetadd[50];
}if_info_t;

static const char *serial_prefix[2][2] = {{"20030841","Gebauer (D)"},{"20030821","Cemgraft (UK)"}};

#endif
