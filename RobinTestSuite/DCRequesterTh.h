#include "threads/Thread.h"
#include "DFThreads/DFThread.h"

#ifndef DCRequesterTh_H
#define DCRequesterTh_H

class ThreadDCR : public DFThread {

public:
  ThreadDCR();
  ThreadDCR(unsigned int);

protected:
  virtual void run();
  virtual void cleanup();

private:
  unsigned int buf_size;
};

ThreadDCR::ThreadDCR() {
  buf_size = 256;
}

ThreadDCR::ThreadDCR(unsigned int bf_size) : buf_size(bf_size) {
} 


/*void ThreadDCR::run() {

}

void ThreadDCR::cleanup() {
  //cerr<<"\nThread finished\n";
  }*/

#endif
