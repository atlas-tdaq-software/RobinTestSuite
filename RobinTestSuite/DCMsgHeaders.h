
#ifndef DCMSGHEADERS_H
#define DCMSGHEADERS_H


/* Modules Types */
enum ModuleType { ROD_TYPE = 0x00,
		  ROB_TYPE = 0x01,
		  ROS_TYPE = 0x02,
		  ROI_BUILDER_TYPE = 0x03,
		  L2SV_TYPE = 0x04,
		  L2PU_TYPE = 0x05,
		  SFI_TYPE = 0x06 };

static const unsigned int s_formatVersionNumber=0x02020000;

typedef struct {
  unsigned int marker;
  unsigned int totalSize;
  unsigned int hdrSize;
  unsigned int version;
  unsigned int srcId;
  unsigned int statCnt;
  unsigned int fragStat;
  unsigned int offsElemCnt;
  unsigned int offsElemId;
  unsigned int specCnt;
  unsigned int l1Id;
  unsigned int bxId;
  unsigned int l1Type;
  unsigned int evntType;
} RobMsgHdr;

typedef struct {
  unsigned int marker;
  unsigned int totalSize;
  unsigned int hdrSize;
  unsigned int version;
  unsigned int srcId;
  unsigned int statCnt;
  unsigned int fragStat;
  unsigned int offsElemCnt;
  unsigned int offsElemId;
  unsigned int specCnt;
  unsigned int runNum;
  unsigned int bxId;
  unsigned int l1Id;
} RosMsgHdr;

#endif

