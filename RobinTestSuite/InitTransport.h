#ifndef MSGTRP_INITTRP_H_
#define MSGTRP_INITTRP_H_

#include <vector>
#include <string>
#include <map>

#include "msg/Types.h"
#include "msg/Node.h"

using namespace std;

//bool init_tran(string&, MessagePassing::NodeID, vector<string>&, MessagePassing::NodeID);
//bool init_tran(string&, MessagePassing::NodeID, MessagePassing::NodeID, vector<string>&);
bool init_tran(std::string&,multimap<string,int>&, unsigned int*);

const unsigned int ROS_SHIFT=12;
#endif // MSGTRP_INITTRP_H_
