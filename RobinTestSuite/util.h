#ifndef _UTIL_H
# define _UTIL_H

int splitext(char*,char**);
int splitarg(char*,char**);
int execute(char*);
int executedup(char*, char*,char*);
int opentty(int*, char*);
int openallttyPci(int,int**);
int runppc_select(int,char*,char*);
int readppcout(int,int*,char*);
int loadppc(int,int*,char*, char*,short*);
int findStr(char*,char**, int);
void getmac(char*, char*);
void writepipelog(struct pollfd*,int,char*);
int pingrobin(int,char*);
int kermitconfig(int,char*);
int findactivetty(int,int,int**,char*,char*,robinInfo_t*,short,short*);
int getserial(int,char*);
int initRobinMsg(int,char*,char*,short*);
int checRepeat(char*, char*);
int pciscript(char*,char*,char*,int,unsigned int*); 
int chckpci(char*,int,robinInfo_t*);
int getifname(if_info_t *iflist);
void add2str(unsigned char*,char*);
void inetadd2str(unsigned char*,char*);
unsigned char *doioctl(int,int,struct ifreq*);

/* #define DefaultSize 500 */
/* #define MAX_BYTE 40000 */
#define MAX_CmdLen 1000
/* #define Max_NumItems 400 */
#define TimeOut 1000

extern int pipefdloc[2],pipefderrloc[2];
extern struct pollfd ufdsloc[2];
extern short StopAll;

#define VER 2.0
#endif
