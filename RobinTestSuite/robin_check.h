#define MAX_BYTE 500000
#define VISIBLE_ITEMS  5
#define Max_Label_Width 100
#define Max_NumItems 40000
#define IDlen 40
#define DefaultSize 2500
#define DefaultSize10 5000
#define MAXCARDS 10
#define MAX_CmdLen 1024

#define NRols 3
#define TimeOut 1000

typedef struct {
  /* char *label; */
  char **testlog,**testerrlog;
  char log[MAX_BYTE];
  char errlog[MAX_BYTE];
}cbdata; 

typedef struct {
  char boardType[IDlen],origin[IDlen],prefix[IDlen];
  int serial_nr;
  int boardspec[10];
  short cardSelect,robin_config,critical_err,rconfig;
}robin_Inf_t;

int get_cards(char*);
short process_input(int,char**,robin_Inf_t*,int);
int executedup(char*, char*, char*);
int appendlog(char*, char*);
int checkNlines(char*);
int splitarg(char*,char**);
int split2lines(char*,char**);
int initRobinMsg(int,char*);
int runRobins(char*);
int runBist(int,cbdata*);
int ebistErrCheck(cbdata*,char*);
int restartPPC(int, int*, cbdata*);
void stopPpc(int*, cbdata*);

int run_ppccmd(int,cbdata*,int*, char*);
int runppc_select(int,char*,char*);
int checkerr(char*,char*,const char**,unsigned int,char*);
int newstyle_ebist(cbdata*,char*);
int selftest();
int getDeviceInfo(char*,robin_Inf_t*);
int opentty(int*, char*);
void extract_str(char*,char*,const char*);

typedef struct {
  const int errshift;
  const char *errstring;
} errlist_t;

typedef enum {
  enum_bistDesIdWarning   = (1<<0),
  enum_bistDesIdWrong     = (1<<1),
  enum_bistResetError     = (1<<2),
  
  enum_bistBufEnableError = (1<<3),
  enum_bistBufDataError   = (1<<4),
  enum_bistBufAddrError   = (1<<5),
  enum_bistBufExtError    = (1<<6),  
  
  enum_bistNetGmiiTimeout = (1<<7),
  enum_bistNetRxError     = (1<<8),
  enum_bistNetTxError     = (1<<9),
  enum_bistNetMacCfgError = (1<<10),
  enum_bistNetPhyCfgError = (1<<11),
  
  enum_bistRolEnableError = (1<<12),
  enum_bistRolTlkPrbs     = (1<<13),
  enum_bistRolTlkLoop     = (1<<14),
  enum_bistRolTlkExtLoop  = (1<<15),
  
  enum_bistParmError      = (1<<16),  /* wrong call paramter */
  
  enum_bistLoadCfgError   = (1<<17),  /* Initialisation error */
  enum_bistInitPpcError   = (1<<18),  /* Initialisation error*/
  enum_bistInitMacError   = (1<<19),  /* Initialisation error*/
  enum_bistInitRolError   = (1<<20),  /* Initialisation error*/
  enum_bistRclkSpeedError = (1<<21),  /* wrong RCLK speed*/
  enum_bistSecSiInvalid   = (1<<22),  /* SecSi sector not valid */
  
  enum_bistErrorTypes     = 22,        /* number of errors*/
  enum_bistError          = (1<<31)   /* highest bit indicated hard error. else warning only*/
} BistCodes;


const char *bistExitCode = "Bist result, channel ";

errlist_t ebisterr[] = {
  {enum_bistSecSiInvalid,"SecSi sector magic word wrong"},
  {enum_bistSecSiInvalid,"SecSi sector not valid"},
  {enum_bistDesIdWrong,"Invalid FPGA DesignID"}, 
  {enum_bistRclkSpeedError,"S-Link clock wrong"},  /* all above: global check */
  {enum_bistNetPhyCfgError,"GbE PHY initialisation error"},
  {enum_bistRolTlkExtLoop,"ROL external loopback error"},
  {enum_bistBufExtError,"Extended buffer test error/warning"},
  /* all the rest goes into "general" category and will be reported in the main Ebist log */
  {enum_bistNetTxError,"GbE transmit error"},
  {enum_bistNetRxError,"GbE receive error"},{enum_bistRolEnableError,"ROL bypass enable error"}, 
  {enum_bistRolTlkPrbs,"ROL SerDes PRBS error"},{enum_bistRolTlkLoop,"ROL internal loopback error"},
  {enum_bistBufAddrError,"Buffer address error"},
  {enum_bistBufDataError,"Buffer data error"},
  {enum_bistParmError,"BIST parameter error (wrong ROL ID)"},{enum_bistNetGmiiTimeout,"GbE GMII timeout error"},
  {enum_bistNetMacCfgError,"GbE MAC initialisation error"},
  {enum_bistResetError,"FPGA register reset error"},{enum_bistInitPpcError,"PPC initialisation error"},
  {enum_bistInitMacError,"MAC initialisation error"},{enum_bistInitRolError,"ROL initialisation error"},
  {enum_bistLoadCfgError,"Configuration error"},
  {enum_bistError,"Hard error"},
};

const char *ppwar[] = {
  "Actual FPGA revision too high","WriteFpf warning: no free pages on stack",
  "Warning: value too small, limit to MIN","Warning: Main data cache not enabled\n"
  /* ,"Software warning" */
};

const char *buildstr = "build date:";
const int build0 = 732161; /*Dec  8 2005 */
const char *monthList[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
static const char *serial_prefix[2][2] = {{"20030841","Gebauer (D)"},{"20030821","Cemgraft (UK)"}};

enum	TestResult
{
	TmPass        = 0,
	TmUndef       = 182,
	TmFail        = 183,
	TmUnresolved  = 184,
	TmUntested    = 185,
	TmUnsupported = 186
};
