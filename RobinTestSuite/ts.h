#ifndef _TS_H
# define _TS_H

#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>
#include <sys/poll.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

#include <Xm/Xm.h>

#include <Xm/DialogS.h>
#include <Xm/RowColumn.h>
#include <Xm/Text.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/PushB.h>
#include <Xm/PanedW.h>
#include <Xm/LabelG.h>
#include <Xm/ToggleB.h>
#include <Xm/MessageB.h>
#include <Xm/SelectioB.h>
#include <Xm/FileSB.h>
#include <X11/cursorfont.h>

#define MAXCRD 10
#define NRols 3
#define NSERTTY 5   /* number of serial ports */

#define CONF_FILES 4

typedef struct {
  /* char *label; */
  char **testlog,**testerrlog;
  char log[MAX_BYTE];
  char errlog[MAX_BYTE];
  Widget cwid;
}cbdata; 

typedef struct {
  short autorun;
  unsigned int nfields;
  unsigned int Neth;
  Widget runButtons[3];
  Widget *statButtons,*errButtons,*TButt,*TBActCard,*CardstatButt;
  Widget stopButton,cdWriteButton;
  Widget runSelectedTests;
  Widget shell;
  cbdata *currentLog;
  short *select;
  robinInfo_t *robinBoardsInfo;
  short *cardSelect;
  int NCards,NCardsSel,Ndolar;
  short Dolar,robintty,nettran,extLoop,ethdev;
  int *ttyfd;  
} runshell_t;

extern int splitarg(char*,char**);
extern int splitext(char*,char**);
extern int opentty(int*, char*); 
extern int runppc_select(int, char*,char*);
extern int execute(char*);
extern int executedup(char*, char*,char*);
extern int openalltty(int,int**);
extern void writepipelog(struct pollfd*,int,char*);
extern int pingrobin(int, char*);
extern int findactivetty(int,int,int**,char*,char*,robinInfo_t*,short,short*);
extern int kermitconfig(int,char*); 
extern int initRobinMsg(int,char*,char*,short*);
extern void getmac(char*, char*);
extern void getip(char*, char*); 
extern int closeSerial(int**, int);
extern int driverUseCheck(char*,char*);
extern int pciscript(char*,char*,char*,int,unsigned int*);
extern int checRepeat(char*, char*);
extern int chckpci(char*,int,robinInfo_t*);
extern int getifname(if_info_t*);

Widget CreateConsole(Widget,Widget);

void run_test(Widget,char**);
int get_cards(Widget,cbdata*,short);
int card_write(char*,int,cbdata*);
int create_eeprom(char*,int,char*);
int load_fpga(int,cbdata*);
int robinsetenv(char*,char*,int);
int run_ppccmd(int,cbdata*,int*,char*);
int runBist(int,cbdata*,int*,short,char**);
int checkBuildDate(char*);
/* int analyse_ebist(cbdata*,char*,char**); */
int oldstyle_ebist(cbdata*,char*,char**);
int newstyle_ebist(cbdata*,char*,char**);
int getmacnode(char*,char*,char**);
int getinterface(char*,cbdata*,short);
int run_netreq(int,cbdata*,int*);
int run_pcireq(int,cbdata*,int*);
int fpga_check(int,cbdata*,int*);
int appendlog(char*,char*,short);
int checkerr(char*,char*,const char**,unsigned int,char*);
int checkppcounters(char*,char*);
/* int loadppc(int,char*,char*); */
int loadppc(int,int*,char*,char*,short*);
int loadppcpci(int,char*,char*,short*,char*);
int readppcout(int,int*,char*);
int getDeviceInfo(char*,cbdata*);
void setstatus(int,short,char*);
void resetstatus();
int check_active();
int removeAllocWid(int,Widget*);
int ebistErrCheck(cbdata*,char*);
int runRobins(char*);
int restartPPC(int, int*,cbdata*);
void stopPpc(int*, cbdata*);
int dolar_scripts(char*,char*);
void selftest(Widget);

int passed(Widget);
int passedwarrnings(Widget);
int failed(int,runshell_t*);
void testEnd(FILE*,FILE*,cbdata*, char*,int);
void notice_win(Widget, char*,char*);
void info_win(Widget, char*,char*); 

void addappwork(XtPointer);
void updatewind(XtPointer);
void refreshwind(int);

const char *runtimeerr[] = {
  "Command not found","error while loading shared libraries","relocation error"
};

const char *systemerr[] = {
  "robin_firmware_update not found - check if PATH and LD_LIBRARY_PATH are correctly set",
  "shared libraries cannot be loaded, check if LD_LIBRARY_PATH is correctly set",
  "check if LD_LIBRARY_PATH is correctly set"
};
			   

const char *msgconf[] = {
  "error","Error","failed","configuration failed!","An exception","error while loading shared libraries"
};

const char *configerr[] = {
  "Error","Chip has JTag ID: 11111111111111111","failed","configuration failed!",
  "An exception","error while loading shared libraries"
};


const char *fpgaloaderr[] = {
  "Error","JTag ID: 11111111111111111111111111111111","An exception",
  "FPGA configuration FAILED","error while loading shared libraries"
};

const char *eprom_flasherr[] ={
  "ERROR","Wrong boardNo!","failed",/* "error while loading shared libraries", */
  "Reading Design ID (should be 12350102): ffffffff","An exception",
  "Exception","Reading FPGA Jtag ID 00000000000000000000000000000000","Wrong design",
  "Reading FPGA Jtag ID 11111111111111111111111111111111","WRITE error","Error"
};

const char *kermerr[] = {"Sorry, write access to UUCP lockfile directory denied","Too many retries"};

const char *ppcerr[] = {
  "SecSi sector magic word wrong",
  "No elf image at address",
  "Incompatible FPGA version","Unknown command","Illegal Instruction","Program Check Exception",
  "Initialisation failed","LSC Initialisation failed"
  "Init status after initPpc (should be 0):","Buffer initialisation failed",
  "Test input fifo not empty after fragment transmission. Writing fragment failed",
  "Adding single page fragment failed","with errors",
  "Formatting single fragment","Formatting page","Adding fragment failed",
  "error code","popFree error: TOS already at 0","DPM error",
  "Verify error (header) event","Verify error (data) event","Verify error (trailer) event",
  "Bus Fault","Fault","Unknown command","BufferTest: Invalid rolNumber:","ERROR"
};

const char *ppwar[] = {
  "Actual FPGA revision too high","WriteFpf warning: no free pages on stack",
  "Warning: value too small, limit to MIN","Warning: Main data cache not enabled\n"
  /* ,"Software warning" */
};


const char *rcounters[] = {
  "Hardware errors:","Software errors:","ROL error:",
  "Buffer manager receive errors:","Buffer manager request errors:",
  /* "Frags not available:", */"Frags corrupted:","Frags rejected:",
  "Messages lost:","Messages invalid:"
};

const char *ROSerr[] ={
  "ROBIN initialization error!!","Fpga not ready for access","An exception",
  "Wrong ROBIN reply message","error while loading shared libraries","Wrong ROBIN reply message",
  "UnknownSource: Package ROSRobin","Status word of ROB header contains error flag",
  "Error in ROB status word"
};

const char *commonerr[] = {
  "invalid MAC address","Command not found","Time out","ERROR"
};

const char *OTPerr[] = {"ERROR","Exception"};
const char *ROSwarr[] ={"WARNING:"};

const char *actred = "Red";
const char *actgreen = "Green";
const char *actyell = "Yellow";
const char *actwhite = "White";
const char *gold4 = "gold4";
const char *lightgray = "light slate gray";
const char *darkOrchid = "dark orchid";
const char *darkBlue  = "dark blue";
const char *LightYellow1 = "LightYellow1";
const char *black = "black";
const char *royalBlue = "royal blue";
const char *LightBlue = "LightBlue";
const char *darkSlateBlue = "dark slate blue";
const char *gray87 = "gray87";
const char *azure = "azure";
const char *plum1 = "plum1";

const char *userSelLabels[][2] = {{"Int.Gen.","Dolar"},{"/dev/tty","robintty"},
				   {"RawSocket","UDP"},{"LoopBack","no LoopBack"},
				   {"eth0","eth1"}};

if_info_t ifInfo[MAX_IF];

const char *confname = ".tscon";

/*test test */
const   char *test_str = "\n   boardType boardNo deviceNo deviceId vendorId    serialNo bus slot fpgaType\n \
    unknown       0        0     9656     10b5          49   3    1  INVALID\n \
    unknown      1        1      324     10dc           6   2    1  INVALID";
/*test test */


typedef struct {
  const int errshift;
  const char *errstring;
} errlist_t;

typedef enum {
  enum_bistDesIdWarning   = (1<<0),
  enum_bistDesIdWrong     = (1<<1),
  enum_bistResetError     = (1<<2),
  
  enum_bistBufEnableError = (1<<3),
  enum_bistBufDataError   = (1<<4),
  enum_bistBufAddrError   = (1<<5),
  enum_bistBufExtError    = (1<<6),  
  
  enum_bistNetGmiiTimeout = (1<<7),
  enum_bistNetRxError     = (1<<8),
  enum_bistNetTxError     = (1<<9),
  enum_bistNetMacCfgError = (1<<10),
  enum_bistNetPhyCfgError = (1<<11),
  
  enum_bistRolEnableError = (1<<12),
  enum_bistRolTlkPrbs     = (1<<13),
  enum_bistRolTlkLoop     = (1<<14),
  enum_bistRolTlkExtLoop  = (1<<15),
  
  enum_bistParmError      = (1<<16),  /* wrong call paramter */
  
  enum_bistLoadCfgError   = (1<<17),  /* Initialisation error */
  enum_bistInitPpcError   = (1<<18),  /* Initialisation error*/
  enum_bistInitMacError   = (1<<19),  /* Initialisation error*/
  enum_bistInitRolError   = (1<<20),  /* Initialisation error*/
  enum_bistRclkSpeedError = (1<<21),  /* wrong RCLK speed*/
  enum_bistSecSiInvalid   = (1<<22),  /* SecSi sector not valid */
  
  enum_bistErrorTypes     = 22,        /* number of errors*/
  enum_bistError          = (1<<31)   /* highest bit indicated hard error. else warning only*/
} BistCodes;

const char *bistExitCode = "Bist result, channel ";

errlist_t ebisterr[] = {
  {enum_bistSecSiInvalid,"SecSi sector magic word wrong"},
  {enum_bistSecSiInvalid,"SecSi sector not valid"},
  {enum_bistDesIdWrong,"Invalid FPGA DesignID"}, 
  {enum_bistRclkSpeedError,"S-Link clock wrong"},  /* all above: global check */
  {enum_bistNetPhyCfgError,"GbE PHY initialisation error"},
  {enum_bistRolTlkExtLoop,"ROL external loopback error"},
  {enum_bistBufExtError,"Extended buffer test error/warning"},
  /* all the rest goes into "general" category and will be reported in the main Ebist log */
  {enum_bistNetTxError,"GbE transmit error"},
  {enum_bistNetRxError,"GbE receive error"},{enum_bistRolEnableError,"ROL bypass enable error"}, 
  {enum_bistRolTlkPrbs,"ROL SerDes PRBS error"},{enum_bistRolTlkLoop,"ROL internal loopback error"},
  {enum_bistBufAddrError,"Buffer address error"},
  {enum_bistBufDataError,"Buffer data error"},
  {enum_bistParmError,"BIST parameter error (wrong ROL ID)"},{enum_bistNetGmiiTimeout,"GbE GMII timeout error"},
  {enum_bistNetMacCfgError,"GbE MAC initialisation error"},
  {enum_bistResetError,"FPGA register reset error"},{enum_bistInitPpcError,"PPC initialisation error"},
  {enum_bistInitMacError,"MAC initialisation error"},{enum_bistInitRolError,"ROL initialisation error"},
  {enum_bistLoadCfgError,"Configuration error"},
  {enum_bistError,"Hard error"},
};

const char *buildstr = "build date:";
const int build0 = 732161; /*Dec  8 2005 */
const char *monthList[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

const char *Ver = "7.1";

int pipefdloc[2],pipefderrloc[2];
struct pollfd ufdsloc[2];
const char *default_conf_files[] = {"robin.elf","proj_1.bit","ppcflash3L2k.bit",""};
char fpga_elf[CONF_FILES][500];
short StopAll;

static const char *help4help[] = {"Click on the help button, then on the element you want to obtain help for.\n" \
				  "If help is available, the popup window will appear on your screen.\nDismiss it by clicking on the "\
				  "right top corner of the window."};

static const char *ppcmd_help[] = {"You can use this button to execute a specific command on the Robin card.\n" \
				    "This requires tty serial connection to be open.\nTo open serial connection:\n-press "\
				    "\"Run config\" button,\n" \
				    "-select card(s)\n-select \"Open tty\"\n-press \"Run button\".\nOnly then you can use "
				    "ppcCmd button.\n"};

static const char *selectrun_help[] = {"Press this button to:\n-Run automatic, extended Ebist test on selected cards."\
				       "\nButton is disabled when any of the tests is already running.",
				       "Press this button to:\n-Check how many cards are visible on the PCI bus.\n" \
				       "-Read PLX EEPROM\n-Write/update Robin monitor program (u-boot)." \
				       "-Write/update Robin card environment.\n" \
				       "Run configuration on selected cards:\n"	\
				       "-Write PPC/FPGA bitfile\n-Configure FPGA\n-Open serial tty connection to " \
				       "the selected cards\n-Run extended Ebist test\n-Restart PPC application." \
				       "\nButton is disabled when any of the tests is already running.",
				       "Press this button to:\n-Run Network or/and PCI data transfer test.\nEither internal " \
				       "Robin data generator or Dolar card will be used, depending on user selection." \
				       "\nButton is disabled when any of the tests is already running."};

const char *conffileshelp[] = {"Select Power PC software,\n(usuall) file extension: \"elf\".\nUse \"Filter\" to narrow selection.",
			       "Select FPGA bitfile\n(usuall) extension: \"bit\".\nUse \"Filter\" to narrow selection.",
			       "Select special configuration file\n (usuall) extension: \"bit\".\nUse \"Filter\" to narrow selection."};

static const char *runconfhelp[] = {"Select data source: either Dolar card or Robin internal data generator.\n" \
				    "This is used when running network/PCI data transfer tests.\n" \
				    "In case of \"Dolar\" selection, the data source card has to be on the same machine\n" \
				    "which run TestSuite (this program).\nMake sure \"Dolar\" card is connected to \n" \
				    "Robin card via fiber optics cables.",
				    "Select serial connection: either via serial cable (/dev/tty) or robindriver (robintty)." \
				    "\nIf /dev/tty is selected, make sure Robin card is connected via card serial interface to\n" \
				    "the host machine serial or USB port. In case of the USB port, use serial to USB adapter.",
				    "Select network protocol, either \"raw socket\" or UDP.\nThis is used when running network test."\
				    "Note that the \"raw socket\"\nrequires \"root\" privilage to use, or capserver running on the " \
				    "host machine.\n",
				    "Loopback is a fiber optic cable connected to the Robin ROLs.\n" \
				    "This is used by the extended Ebist test. If you want to use loopback for testing,\n"\
				    "make sure that the loopback cable is plugged in into each ROL of the Robin card.",
				    "Ethernet device to be used when testing data transfer\nover the network. " \
				    "This ethernet device is located on\na machine which runs \"requester\" program."};

static const char *cardsel_help[] = {"Click here to select Robin card.\nTests/configuration steps will run only on selected cards."};

static const char *confprog_help[] = {"This test checks number of Robin cards seen on the PCI bus.",
				      "This test reads PLX eeprom information. The validity of the PLX content can be" \
				      "verify by checking test log.",
				      "This test is used to update Robin monitor program.",
				      "This test is used to write/modify Robin card internal environment.\nUser can supply " \
				      "his/her/it own environment by creating env_core.env file,\notherwise the default setting" \
				      " will be used.",
				      "Writes selectd by user PPC (*.elf) file to the Robin memory.",
				      "Writes selectd by user FPGA bitfile (*.bit) file to the Robin memory.",
				      "Configures FPGA with the user specified FPGA bitfile.",
				      "Opens serial connection to the Robin.",
				      "Runs extended Ebist test.",
				      "Stops (if necessary) and restarts PPC application on the Robin card."};

static const char *tests_help[] = {"This test performs data transfer test over the network\nMake sure card has ethernet cable" \
				   " plugged in.\nUse \"Select configuration\" buttons to choose:\n" \
				   "-An approprate host ethernet device,\n" \
				   "-Select protocol: UPD or RawSocket,\n"\
				   "-Data source: \"Dolar\" or Robin internal data generator.\n" \
				   "In case of \"Dolar\" selection, the data source card has to be on the same machine\n" \
				   "which run TestSuite (this program). Make sure \"Dolar\" card is connected to \n" \
				   "Robin card via fiber optics cables.",
				   "This test performs data transfer test over the PCI bus.\n" \
				   "Use \"Select configuration\" buttons to choose:\n"
				   "-Data source: \"Dolar\" or Robin internal data generator.",
				   "This test restarts Power PC application."};

static const char *autoebist_help[] = {"Runs automatic, extended eBist test on all selected cards."};

static const char *teststat_help[] = {"After tests complition, click here to see the full log\nfor this test.",
				      "After tests complition, in case of errors, click here \nto obtain error "\
				      "report relevant for a failed test."};


#endif
