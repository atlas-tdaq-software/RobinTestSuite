#include <memory>
#include <new>
#include <sys/time.h>
#include <limits.h>
#include <sys/types.h>

#include "DFThreads/DFMutex.h"

#include "msg/Port.h"
#include "msg/Buffer.h"
#include "msginput/MessageHeader.h" // DC MessageHeader

#include "InitTransport.h"
#include "clocks/Clock.h"

#include "localdef.h"

typedef struct g_header {      //message structure
  unsigned int magic_word;
  unsigned short mes_type_id;
  unsigned short sender_id;
  unsigned int mlength;
  unsigned int timestemp;
  unsigned int transaction_id;
  unsigned int req_length;
} g_header;

/* General ROB header */
typedef struct RobHdr {
  unsigned int marker;
  unsigned int totalSize;
  unsigned int hdrSize;
  unsigned int version;
  unsigned int srcId;
  unsigned int statCnt;
  unsigned int fragStat;
  unsigned int mrrEvId;
  /*unsigned int fragAttn; 
  unsigned int offsElemCnt;
  unsigned int offsElemId;*/
  unsigned int specCnt;
  /*unsigned int l1Id;
  unsigned int bxId;
  unsigned int l1Type;
  unsigned int evntType;*/
} RobHdr;

/* General ROS header */
typedef struct RosHdr {
  unsigned int marker;
  unsigned int totalSize;
  unsigned int hdrSize;
  unsigned int version;
  unsigned int srcId;
  /*unsigned int runNum;*/
  unsigned int statCnt;
  unsigned int fragStat;
  /*unsigned int offsElemCnt;
    unsigned int offsElemId;*/
  unsigned int specCnt;
  unsigned int runNum;
  unsigned int l1Id;
  unsigned int bxId;
} RosHdr;
  
typedef struct trg_msg {
  g_header msg_header;
  unsigned int* msg_body;
} trg_msg;

typedef struct FragHdr {
  g_header GenHdr;
  /* RosHdr RosHrd; */
  RobHdr RobHrd;
  unsigned int rod_words[5];
  unsigned int evId;
} FragHdr;

typedef struct RobinRodHdr {
  RobHdr RobHrd;
  unsigned int rod_words[5];
  unsigned int evId;
} RobinRodHdr_t;

const unsigned int TrailesSize = 28; //bytes

/*typedef struct FragHdr {
  g_header GenHdr;
  RobHdr RobHrd;
  unsigned int rod_words[5];
  unsigned int evId;
  } FragHdr_NoROS;*/


const unsigned int MESSAGE_MAGIC_WORD = 0x12345678;
const unsigned int L2PU_Data_Request = 0x1bbb;
const unsigned int L2PU_Data_Request_all = 0xfbbb;
const unsigned int ROS_Fragment	= 0x2bbb;
const unsigned int DFM_Decision	= 0xfccc;
const unsigned int DFM_Clear = 0x6ccc;
const unsigned int ROS_Event_Fragment = 0x1ccc;

const unsigned int respClearAck = 0x18;           // ClearAck acknoledge
const unsigned int ClearAll = 0x19;               // ClearAll acknoledge
const unsigned int ClearAllRols = 0x11;
const unsigned int StatusMonitorData = 0x20;      // status and monitoring data
const unsigned int EventLog = 0x28;               // Event log. Warning: this is a LARGE data set
const unsigned int CfgItem = 0x30;                // Get configuration item
const unsigned int ReadMem = 0x58;                // data from cpu memory
const unsigned int DumpBuf = 0x5f;                // data from buffer
const unsigned int Ack = 0x00;                    // general positive acknowledge
const unsigned int Nack = 0x08;                   // general negative acknowledge
  
/* test test test */
enum MessageSource {ROSapp = 1, L2SV = 2, L2PU = 3, 
		    DFM = 4, SFI = 5, SF0 = 6, 
		    EFPU = 7};
/* end of test test test */

enum HeaderMarker { RODMarker = 0xee1234ee, 
		    ROBMarker = 0xdd1234dd, 
		    ROSMarker = 0xcc1234cc, 
		    SUB_DETECTOR = 0xbb1234bb, 
		    FULL_EVENT = 0xaa1234aa,     /* event marker */
		    LVL1_RESULT = 0x99123499};   /* L1 result marker */

struct EthernetAddress {
  unsigned short oct12;
  unsigned short oct34;
  unsigned short oct56;
};

typedef struct EthernetAddress EthernetAddress;
typedef struct Eth_Soc EthSoc;

struct Eth_Soc {
  EthernetAddress loc_eth;
  char str_add[20];
  EthSoc *next_eth; 
};

const unsigned int TimeStamp = 0xabcdabcd;
unsigned int MAX_EV_ID = (1<<29) - 1;
const unsigned int MAX_TRAN_ID = 0xFFFFF800;

const u_int NROLS = 3;

using namespace MessagePassing;

void in_par(string);
void connect(unsigned int*);
//void snd_dcreq(void);
//int rcv_dcmsg(void);

void *snd_dcreq(void*);
void *rcv_dcmsg(void*);

int check_gh(Buffer*);
int check_robh(Buffer*);
void get_info(void);
void write_log();
void dump_fullevent(Buffer*,u_short),dumpErrEvent(Buffer*);
void RodDataDump(Buffer*);
int RodDataCheck(Buffer*);
void add2str(EthSoc*);
void dec2bin(unsigned long,ofstream*,short);
void rcv_cleanup(void*),snd_cleanup(void*);
void delete_globals();
extern void catcher(int);
void crc_init();
u_int crctablefast16(Buffer::iterator&, u_int, u_int*);

unsigned int nloop; 
volatile double msg_sent,msg_rcv;
double del_sent,total_sent,total_received;
unsigned int m_invalidmsg,m_rosmark,m_robmark,m_rodmark,m_invalidCRC;
unsigned int *req_add,req_length,*del_add;
unsigned int datacheckloop,debugloop;
unsigned int maxRunTime = 100;
unsigned int m_msg_snd_size = sizeof(g_header);
unsigned int throttle = 10;
unsigned int remote_node = 39;
unsigned int del_group = 10;
unsigned int firstId = 0;
unsigned int hexordec = 1;
unsigned int reqrat = 10;
unsigned int lost_msg = 0;
unsigned int checked_pac = 0;
unsigned int wrongDataRcv = 0;
unsigned int single = 0;
unsigned int nre_requested = 0;
short perf = 0;
short resetid = 0;
string trafic_type;
g_header *pg_header;
short endoftime = 0;
vector<MessagePassing::Port*> remotehosts;
multimap<string,int> RhostsNode;     /*where the remode hosts and nodes go */
unsigned int m_NumRemHosts;
vector<MessagePassing::Port*> deletehosts;
unsigned int m_NumDelHosts;
Port *port_del = NULL;
short left = 1;
static const unsigned int Max_msgsize = 65536;
unsigned int min_size,max_size,empty_msg,not_found;
unsigned int max_ev_id;
list<unsigned int> reqIds;
short rcv_cancel_count,snd_cancel_count;

RealTimeClock dcclock;
Time m_timeElapsed_main,startOfRun;
double idle = 0;
double del_idle = 0;

string logname,filedata;
ofstream *rodDataDump,*rodErrData;

pthread_t rcv_thr,snd_thr;

const char twoseven = 27;
const unsigned int LSIZE = sizeof(long)*8;

pthread_mutex_t snd_lock=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t rcv_lock=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t rcvIds=PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t  rcv_started=PTHREAD_COND_INITIALIZER;
pthread_cond_t  snd_done=PTHREAD_COND_INITIALIZER;
pthread_cond_t  rcv_done=PTHREAD_COND_INITIALIZER;

const u_int c_crctab_dim=65536;
u_long crctab16[c_crctab_dim];
u_long crcinit_direct;
const u_int c_order = 16;
u_int NumberOfEvents = 0;
u_int NumOfSelNodes = 0;
u_int selectedNodes[NROLS];
u_int rqtimeout = 2;

// generate request of a size_t
/*class ReqEmu {
 public:
  ReqEmu();
  ~ReqEmu();
  unsigned int *gen_request(int,unsigned int);
  
 protected:
  trg_msg  gen_req;
  
 private:
  const static unsigned int max_size = 1024;
  unsigned int *dp;   

  };*/

const unsigned int data1k[] = {
  0x0, 0x0, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5,
  0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd,
  0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
  0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
  0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25,
  0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d,
  0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35,
  0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d,
  0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45,
  0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d,
  0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55,
  0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d,
  0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65,
  0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d,
  0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75,
  0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d,
  0x7e, 0x7f, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
  0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d,
  0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95,
  0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d,
  0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5,
  0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad,
  0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5,
  0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd,
  0xbe, 0xbf, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5,
  0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd,
  0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5,
  0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd,
  0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5,
  0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed,
  0xee, 0xef, 0xf0, 0xf1, 0x0, 0x1, 0xf2, 0x1
};

const char *Ver = "netrequester version 10.1";
