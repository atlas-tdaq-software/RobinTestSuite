/*#define _GNU_SOURCE*/
#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dir.h>
#include <unistd.h>
#include <sys/poll.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/time.h>
#include <termios.h>
#include <fcntl.h>
#include <ctype.h>

#include "RobinTestSuite/robin_check.h"

robin_Inf_t *RobinInfo = NULL;
short ebist_enable;
cbdata gLog;
char rlog[MAX_BYTE];

short driversok;

int main(int argc, char **argv)
{
  FILE *fprep;
  char prefix[DefaultSize],repname[DefaultSize];
  char *ptmpstr;
  time_t ltime;
  int nRobins/*,iarg*/;

  if( argc > 1 ) {
    if( strstr(argv[1],"-h") != NULL || strstr(argv[1],"-help") != NULL || strstr(argv[1],"help") != NULL ) {
      fprintf(stdout,"Usage: robinCheck [-eb] [-ncrd 0 1 2...] [-v]\nWhere\n-eb: without -ncrd runs eBist check on " \
	      "all cards,\n-ncrd card(s) selection, example:\n"\
	      "-eb -ncrd 0 2: runs eBist on the first and third Robin cards only (cards count starts from \"0\"),\n" \
	      "\"robinCheck\" without options runs PCI and /proc/robin tests only.\n");
      return TmPass;
    }
  }
  /*for( iarg = 0; iarg < argc; iarg++ ) 
    if( strstr(argv[iarg],"-v") != NULL ) 
    doverbose = 1;*/

  time(&ltime); ctime_r(&ltime,prefix);
  ptmpstr = prefix; 
  while( *ptmpstr != '\0' ) {
    if( *ptmpstr == ' ' || *ptmpstr == '\n' || *ptmpstr == ':' ) 
      *ptmpstr = '_';
    ptmpstr++;
  }

  sprintf(repname,"robinCheck%s.log",prefix);
  if( !(fprep = fopen(repname, "w")) ) {
    perror("logfile: ");
    return TmFail;
  }

  /* check if all the driver are present */
  if( selftest() < 0 ) {
    fprintf(fprep,"%s\n",rlog);
    /*fclose(fprep);*/
    /*return TmFail;*/
    driversok = 0;
  }
  else
    driversok = 1;

  /* detect number of cards, do PCI and /proc/robin/ test, robinconfig if necessary*/
  nRobins = get_cards(rlog);
  fprintf(fprep,"%s\n",rlog); rlog[0] = '\0';
  if( nRobins < 0 ) {
    fclose(fprep);
    return TmFail;
  }

  if( (ebist_enable = process_input(argc,argv,RobinInfo,nRobins)) ) {
    runBist(nRobins,&gLog);
    appendlog(rlog,gLog.errlog);
  }

  fprintf(fprep,"%s\n",rlog);
  fclose(fprep);
  if( RobinInfo != NULL )
    free(RobinInfo);
 
  return TmPass;
}

int get_cards(char *locrlog) {
  char loclog[MAX_BYTE],devicelog[MAX_BYTE],tmpstr[DefaultSize];
  const char *pcicmd[] = {"/sbin/lspci -vvv","/sbin/lspci -b -vvv"};
  const char *scancmd = "robin_firmware_update --mode=scan";
  const char *robin_design[] = {"CERN/ECP/EDU: Unknown device 0144"};
  const char *lookfor[] = {"(rev ac)","Subsystem: Unknown device 2151:1087","Control:",
			   "Status:","Latency:","Interrupt:"};
  const char *check_mem[] = {"Region 0: Memory","Region 1: I/O ports","Region 2: Memory"};
  const char *drvlookfor[] = {"Number of cards detected =","card | FPGA design ID |       BAR2 |",
			      "Dumping status of Message FIFOs"};
  const char *healthy_card = "There are 32 FIFO slots available on card";
  const char *errinf_prefix[] = {"ERROR: PCI system-centric info corrupted, cannot read",
				 "ERROR: PCI bus-centric info corrupted, cannot read"};
  const char *err_proc[] = {"is out of date","robinconfig has not yet been run on this Robin"};
  const char *mem_errinfo = "PCI scan indicates problem with memory resources\n" \
    "This might be due to problems with the PLX chip.\nMark card as faulty, inform Robin experts\n";
  /* const char *scancmd = "robin_firmware_update --mode=scan"; */
  /* const char *labstring[] = {"swVersion","designVersion","Robin temperature"}; */
  char cmd[DefaultSize],scratchbuf[DefaultSize];
  char *pbuf,*pbufnext,*ptmp,*endofinfo;
  char *lineadd[MAXCARDS];
  int icmd,ides,irob/* ,nargs,iargs */;
  unsigned int ilab;
  int found,drv_cards;
  unsigned int reg_info[3];
  short memerr,notconfig;

  fprintf(stdout,"INFO: Running PCI check, please wait..."); fflush(stdout);

  for( icmd = 0; icmd < (int)(sizeof(pcicmd)/sizeof(pcicmd[0])); icmd++ ) {
    loclog[0] = '\0';
    strcpy(cmd,pcicmd[icmd]);
    if( executedup(cmd,loclog,loclog) < 0 )
      perror("execute");

    found = 0; 
    for( ides = 0; ides < (int)(sizeof(robin_design)/sizeof(robin_design[0])); ides++ ) {
      pbuf = loclog;
      while( (pbuf = strstr(pbuf,robin_design[ides])) != NULL ) {
	if( found < MAXCARDS ) 
	  lineadd[found] = pbuf++;
	found++;
      }
    }
    if( !found  ) {
      fprintf(stdout,"ERROR: No Robin cards detected on this system\n");
      strcat(locrlog,"ERROR: No Robin cards detected on this system\n");
      return -1;
    }
    
    if( icmd == 0 ) {
      if( (RobinInfo = calloc(found,sizeof(robin_Inf_t))) == NULL ) {
	perror("RobinInfo: ");
	return -1;
      } 

      if( driversok ) {
	devicelog[0] = '\0';
	if( executedup((char*)scancmd,devicelog,devicelog) < 0 ) {
	  fprintf(stderr,"ERROR: robin_firmware_update failed, %s\nprogram terminated\n",devicelog); 
	free(RobinInfo);
	return -1;
	}
	else {
	  if( getDeviceInfo(devicelog,RobinInfo) < 0 ) {
	    free(RobinInfo);
	    return -1;
	  }
	}
      }
    }
    
    /* check revision and subsystem id */
    for( irob = 0; irob < found; irob++ ) {
      memerr = 0;
      pbufnext = pbuf = lineadd[irob];
      endofinfo = strstr(pbuf,"\n");
      while( (endofinfo - pbufnext) != 0 && *endofinfo != '\0' ) {
	pbufnext = endofinfo + 1;
	endofinfo = strstr(pbufnext,"\n");
      }
      scratchbuf[0] = *endofinfo; *endofinfo = '\0';

      /* check each line of the PCI info */
      for( ilab = 0; ilab < (sizeof(lookfor)/sizeof(lookfor[0])); ilab++ ) {
	if( icmd == 0  && (pbufnext = strstr(pbuf,"\n")) == NULL ) {
	  fprintf(stdout,"ERROR: card serial nr %d, PCI info corrupted, cannot read next line\n",
		  RobinInfo[irob].boardspec[1]);
	  strcat(locrlog,loclog);
	  strcat(locrlog,"ERROR: PCI info corrupted, cannot read next line\n");
	  RobinInfo[irob].critical_err = 1;
	  break;
	  /* return -1; */
	}
	
	if( icmd == 0  && ((ptmp = strstr(pbuf,lookfor[ilab])) == NULL || ptmp > pbufnext) ) {
	  sprintf(tmpstr,"%s \"%s\" card serial nr %d, lspci: PCI info, card %d as seen on the PCI bus\n",
		  errinf_prefix[icmd],lookfor[ilab],RobinInfo[irob].boardspec[1],irob);
	  fprintf(stdout,"%s",tmpstr);
	  strcat(locrlog,tmpstr);
	}
	pbuf = pbufnext + 1;
      }
      /* check memory regions */
      for( ilab = 0; ilab < (sizeof(check_mem)/sizeof(check_mem[0])); ilab++ ) {
	if( icmd == 0  && strstr(pbuf,check_mem[ilab]) == NULL ) {
	  sprintf(tmpstr,"ERROR: card serial nr %d, lspci: PCI card memory info corrupted or missing.\n" \
		  "ERROR: expected %s, got %s\n",RobinInfo[irob].boardspec[1],check_mem[ilab],pbuf);
	  fprintf(stdout,"%s",tmpstr);
	  strcat(locrlog,tmpstr);
	  memerr = 1;
	}

	if( icmd == 0  && strstr(pbuf,"disabled") != NULL ) {
	  sprintf(tmpstr,"ERROR: card serial nr %d, lspci returns: %s Card memory disabled\n",
		  RobinInfo[irob].boardspec[1],check_mem[ilab]);
	  fprintf(stdout,"%s",tmpstr);
	  strcat(locrlog,tmpstr);
	  memerr = 1;
	}
	pbuf = strstr(pbuf,"\n") + 1;
      }
      if( memerr ) {
	sprintf(tmpstr,"\nERROR: card serial nr %d, %s",RobinInfo[irob].boardspec[1],mem_errinfo);
	fprintf(stdout,"%s",tmpstr);
	strcat(locrlog,tmpstr);
	RobinInfo[irob].critical_err = 1;
      }
      *endofinfo = scratchbuf[0];
    }
  }

  fprintf(stdout,"\n");
  for( irob = 0; irob < found; irob++ ) {
    fprintf(stdout,"INFO: %s %s serial %d seen on PCI as card nr. %d \n",
	    RobinInfo[irob].boardType,RobinInfo[irob].origin,RobinInfo[irob].boardspec[1],irob);
  }
  fprintf(stdout,"\n");

  if( !driversok ) 
    return -1;

  /* check /proc/robin */
  fprintf(stdout,"\nINFO: Checking /proc/robin:\n\n"); fflush(stdout);
  strcpy(cmd,"/bin/cat /proc/robin");
  if( executedup(cmd,loclog,loclog) < 0 )
    perror("ERROR: execute");
  if( (pbuf = strstr(loclog,drvlookfor[0])) == NULL || strstr(loclog,"No such file or directory") != NULL ) {
    sprintf(tmpstr,"ERROR: error when reading /proc/robin - check if the file exists.\n" \
	    "ERROR: check if robindriver is loaded\n");
    fprintf(stdout,"%s",tmpstr);
    strcat(locrlog,tmpstr);
    return -1;
  }
  ptmp = strstr(pbuf,"\n"); *ptmp = '\0';
  pbuf = pbuf + strlen(drvlookfor[0]);
  sscanf(pbuf,"%d",&drv_cards);
  if( found != drv_cards ) {
    sprintf(tmpstr,"ERROR: PCI bus scan returns differen number of cards then reported by the driver.\n" \
	    "ERROR: PCI scan: %d card(s), robin driver reports: %d card(s)\n",found,drv_cards);
    fprintf(stdout,"%s",tmpstr);
    strcat(locrlog,tmpstr);
    /*return -1;*/
  }
  *ptmp = '\n';

  if( (pbufnext = strstr(pbuf,drvlookfor[1])) == NULL ) {
    sprintf(tmpstr,"ERROR: cannot read FPGA design ID form /proc/robin. /proc/robin might be corrupted\n");
    fprintf(stdout,"%s",tmpstr);
    strcat(locrlog,tmpstr);
  }
  ptmp = strstr(pbufnext,"\n"); 
  for( irob = 0; irob < found; irob++ ) {
    pbufnext = ptmp + 1;
    ptmp = strstr(pbufnext,"\n"); *ptmp = '\0';
    sscanf(pbufnext,"%ud",&reg_info[0]);
    if( (int)reg_info[0] != irob ) {
      sprintf(tmpstr,"WARNING: card serial nr %d, mismatch between expectd card number and read from  the driver\n" \
	      "WARNING: expected card %d, got %d /proc/robin might be corrupted\n",
	      RobinInfo[irob].boardspec[1],irob,reg_info[0]);
      fprintf(stdout,"%s",tmpstr);
      strcat(locrlog,tmpstr);
    }

    if( strstr(pbufnext,err_proc[0]) != NULL || strstr(pbufnext,err_proc[1]) != NULL ) {
      sprintf(tmpstr,"ERROR: card %d %s\n",RobinInfo[irob].boardspec[1],pbufnext);
      fprintf(stdout,"%s",tmpstr);
      strcat(locrlog,tmpstr);
      RobinInfo[irob].critical_err = 1;
    }
    else {
      if( (pbufnext = strstr(pbufnext,"0x")) == NULL ) {
	sprintf(tmpstr,"ERROR: Cannot read FPGA design id for card %d\n",RobinInfo[irob].boardspec[1]);
	fprintf(stdout,"%s",tmpstr);
	strcat(locrlog,tmpstr);
	RobinInfo[irob].critical_err = 1;
      }
      else {
	sscanf(pbufnext,"%x",&reg_info[1]);
	if( strstr(pbufnext,"0xffff") != NULL ) {
	  sprintf(tmpstr,"ERROR: card %d - invalid FPGA design id: 0x%x\n",
		  RobinInfo[irob].boardspec[1],reg_info[1]);
	  fprintf(stdout,"%s",tmpstr);
	  strcat(locrlog,tmpstr);
	  RobinInfo[irob].critical_err = 1;
	}
	sprintf(tmpstr,"INFO: card %d ser. %d, FPGA version 0x%x\n",
		irob,RobinInfo[irob].boardspec[1],reg_info[1]);
	fprintf(stdout,"%s",tmpstr);
	strcat(locrlog,tmpstr);
      }
    }
    *ptmp = '\n'; 
  }

  if( (pbuf = strstr(pbufnext,drvlookfor[2])) == NULL ) {
    fprintf(stdout,"ERROR: cannot read status of Message FIFOs form /proc/robin. /proc/robin might be corrupted\n");
    strcat(locrlog,"ERROR: cannot read status of Message FIFOs form /proc/robin. /proc/robin might be corrupted\n");
    /*return -1;*/
  }
  ptmp = strstr(pbuf,"\n"); 
  notconfig = 0;
  for( irob = 0; irob < found; irob++ ) {
    pbufnext = ptmp + 1;
    ptmp = strstr(pbufnext,"\n"); *ptmp = '\0';
    if( strstr(pbufnext,healthy_card) == NULL ) {
      sprintf(tmpstr,"ERROR: card %d not configured: %s, run robinconfig\n",
	      RobinInfo[irob].boardspec[1],pbufnext);
      fprintf(stdout,"%s",tmpstr);
      strcat(locrlog,tmpstr);
      RobinInfo[irob].robin_config = 0;
      notconfig = 1;
    }
    else 
      RobinInfo[irob].robin_config = 1;
    *ptmp = '\n'; 
  }

  if( notconfig ) {
    initRobinMsg(found,loclog);
    appendlog(locrlog,loclog);
    /* fprintf(stdout,"\nSome cards are not configured, do you want to run robinconfig now ?"); */
  }

  return found;
}

short process_input(int uargc,char **uargv,robin_Inf_t *locRobinInfo, int maxcrd) {
  int iarg,robinId,irob;
  short ebtestEnable,crdselect;

  crdselect = ebtestEnable = 0;
  for( iarg = 1; iarg < uargc; iarg++ ) {
    if( strstr(uargv[iarg],"-eb") != NULL ) 
      ebtestEnable = 1;

    if( strstr(uargv[iarg],"-ncrd") != NULL ) {
      crdselect = 1;
      iarg++;
      for( ; iarg < uargc; iarg++ ) {
	if( strstr(uargv[iarg],"-eb") != NULL ) {
	  ebtestEnable = 1;
	  break;
	}

	sscanf(uargv[iarg],"%d",&robinId);
	if( robinId >= 0 && robinId < maxcrd ) 
	  locRobinInfo[robinId].cardSelect = 1;
      }
    }
  }

  if( ebtestEnable && !crdselect ) 
    for( irob = 0; irob < maxcrd; locRobinInfo[irob++].cardSelect = 1 );

  return ebtestEnable;
}

int executedup(char *cmd, char *buf, char *buferr) {
  int  pid,retpid,status;
  ssize_t  in_bytes;
  int pipefd[2],pipefderr[2];
  char tmpbuf[MAX_BYTE],loccmd[MAX_CmdLen];
  char *args[Max_NumItems];
  long fdflags;
  struct pollfd ufds[2];
  int j,nfdesc,nfds,timeout,waittime,nfdescmain;

  /* stdout pipe */
  if( pipe(pipefd) < 0 ) {
    perror("pipe");
    return -1;
  }

  /* stderr pipe */
  if( pipe(pipefderr) < 0 ) {
    perror("pipeerr");
    return -1;
  }

  /*Get a child process.*/
  if( (pid = fork()) < 0 ) {
    perror("fork");
    exit(1);
  }

  appendlog(buf,cmd);
  appendlog(buf,(char*)"::\n");
  /*child redirects its stdout and stderr to the pipes */
  if( pid == 0 ) {
    /* dup both: stdout and stderr */
    dup2(pipefd[1],1);
    dup2(pipefderr[1],2);
    close(pipefd[1]);
    close(pipefderr[1]);

    strncpy(loccmd,cmd,MAX_CmdLen-1);
    splitarg(loccmd,args);
    execvp(*args, args);

    perror(*args);
    exit(1);
  }

  if( (fdflags = fcntl(pipefd[0],F_GETFL)) == -1 ) {
    perror("ERROR: fcntl getflag stdout::");
  }
  fdflags |= O_NONBLOCK;
  if( fcntl(pipefd[0],F_SETFL,fdflags)  == -1 ) {
    perror("ERROR: fcntl setflag stdout::");
  }

  if( (fdflags = fcntl(pipefderr[0],F_GETFL)) == -1 ) {
    perror("ERROR: fcntl getflag stderr::");
  }
  fdflags |= O_NONBLOCK;
  if( fcntl(pipefderr[0],F_SETFL,fdflags)  == -1 ) {
    perror("ERROR: fcntl setflag stderr::");
  }

  waittime = 0; nfds = 0;
  timeout = 2000;
  nfdescmain = nfdesc = 2; /*stdout and stderr */ 
  ufds[0].fd = pipefd[0];  ufds[1].fd = pipefderr[0];
  ufds[0].events = ufds[1].events = (POLLIN | POLLPRI); 
  while( (retpid = waitpid(pid,&status,WNOHANG)) != pid && retpid != -1 ) {
    if( (nfds = poll(ufds,nfdesc,timeout)) > 0  ) {
      for( j = 0; j < nfdesc; j++ ) {
	if( ufds[j].revents != 0 ) {
	  if( (in_bytes = read(ufds[j].fd,tmpbuf,(DefaultSize-1))) > 0 ) {
	    tmpbuf[(in_bytes/sizeof(char))] = '\0';
	    appendlog(buf,tmpbuf);
	    waittime = 0;	    
	  }
	  if( --nfds == 0 )
	    break;
	}
	
      }
    }
    else {
      if( (waittime+= timeout) > 180000 ) {
	sprintf(tmpbuf,"ERROR: process %s not responding\n",cmd);
	sprintf(tmpbuf,"kill -9 %d\n",pid);
	system(tmpbuf);
	return -1;
      }
    }
  }

  for( j = 0; j < nfdesc; j++ ) {
    if( ufds[j].revents != 0 ) {
      if( (in_bytes = read(ufds[j].fd,tmpbuf,(DefaultSize-1))) > 0 ) {
	tmpbuf[(in_bytes/sizeof(char))] = '\0';
	appendlog(buf,tmpbuf);
      }
    }
    if( --nfds == 0 )
      break;
  }

  /* stdout */
  while( (in_bytes = read(pipefd[0],tmpbuf,(DefaultSize -1))) > 0 ) {
    tmpbuf[(in_bytes/sizeof(char))] = '\0';
    appendlog(buf,tmpbuf);
  }
    
  /*stderr */
  while( (in_bytes = read(pipefderr[0],tmpbuf,(DefaultSize -1))) > 0 ) {
    tmpbuf[(in_bytes/sizeof(char))] = '\0';
    appendlog(buferr,tmpbuf);
  }

  if( close(pipefd[1]) || close(pipefd[0]) || close(pipefderr[0])  )
    appendlog(buferr,(char*)"closing executedup pipes:");

  
  if( strstr(buf,"No such file or directory") != NULL || strstr(buf,"Command not found") != NULL ) {
    sprintf(tmpbuf,"ERROR: no such file or directory when executing: %s\n",cmd);
    appendlog(buferr,tmpbuf);
    return -1;
  }

  /*if( WIFEXITED(status) ) 
    return( WEXITSTATUS(status) );
    else */
    return(pid);
}

int appendlog(char *currentLog, char *strappend) {
  int space_left,statapp;
  static int minimufree = 0;

  if( !minimufree ) 
    minimufree = strlen("\n\tLOG FULL !!!\n") + 1;

  statapp = 0;
  
  if( strstr(currentLog,"LOG FULL") == NULL ) {
    if( (space_left = (MAX_BYTE - (strlen(currentLog) + strlen(strappend)))) < minimufree ) {
      strncat(currentLog,strappend,(MAX_BYTE-(minimufree+strlen(currentLog))));
      strcat(currentLog,"\n\tLOG FULL !!!\n");
      return -1;
    }
    else
      strcat(currentLog,strappend);
  }
  else
    statapp = -1;

  return statapp;
}

int split2lines(char* test_str,char** argline) {
  int narg;
  char *eol;

  if( test_str == NULL || strlen(test_str) == 0 ) 
    return 0;

  narg = 0;
  while( *test_str != '\0' ) {

    if( (eol = index(test_str,'\n')) !=NULL ) {
      *argline++ = test_str;
      narg++;
      test_str = eol + 1;
    }
    else { /* no more lines to read */
      if( test_str != NULL && strlen(test_str) > 0 ) {
	*argline++ = test_str;
	return(++narg);
      }
    }
  }

  return (narg);
}

int splitext(char* test_str,char** argline) {
  int narg;
  char *eol;

  if( test_str == NULL || strlen(test_str) == 0 ) 
    return 0;

  narg = 0;
  while( *test_str != '\0' ) {

    if( (eol = index(test_str,'\n')) !=NULL ) {
      *eol = '\0';
      *argline++ = test_str;
      narg++;
      test_str = eol + 1;
    }
    else { /* no more lines to read */
      if( test_str != NULL && strlen(test_str) > 0 ) {
	*argline++ = test_str;
	return(++narg);
      }
    }
  }

  return (narg);
}

int splitarg(char* test_str,char** argline) {
  int narg;

  if( test_str == NULL ) 
    return 0;

  narg = 0;
  while( *test_str != '\0' ) {
    while( (*test_str == ' ') || (*test_str == '\t') || (*test_str == '\n') ) 
      *test_str++ = '\0';

    if( *test_str != '\0' ) {
      *argline++ = test_str; 
      narg++;
    
      while( (*test_str != '\0') && (*test_str != ' ') && 
	     (*test_str != '\t') && (*test_str != '\n') )
	test_str++;
    }
  }
  *argline = '\0';

  return (narg);
}

int checkNlines(char* test_str) {
  int nlines;
  char *eol;

  nlines = 0;
  while( *test_str != '\0' ) {
    if( (eol = index(test_str,'\n')) !=NULL ) {
      nlines++;
      test_str = eol + 1;
    }
    else {
      if( strlen(test_str) > 0 ) /* a line without '\n'*/
	return(++nlines);
    }
  }

  return nlines;
}

int initRobinMsg(int Ncrd,char *cardlog) {
  int icrd;
  char cmd[DefaultSize],tmpstr[DefaultSize],*loclog;
  const char *failmsg = "INFO: initRobinMsg failed. Check the amount of bigphys memory allocated\n" \
    "INFO: This error might be caused due to too small bigpys area and/or two many cards in the system\n" \
    "INFO: Try to reduce number of cards in the test system\n";
  char *buferr,*cbuf;
  int card_stat;

  fprintf(stdout,"\nINFO: Configuring Robin cards, please wait...\n"); fflush(stdout);
  loclog = cardlog + strlen(cardlog);
  sprintf(cmd,"cat /proc/robin");
  if( executedup(cmd,loclog,loclog) < 0 ) 
    return -1;

  card_stat  = 0; cbuf = loclog;
  for( icrd = 0; icrd < Ncrd; icrd++ ) {
    if( loclog != NULL && strlen(loclog) > 0 ) {
      sprintf(cmd,"There are 0 FIFO slots available on card %d",icrd);
      if( strstr(loclog,cmd) != NULL ) {
	fprintf(stdout,"INFO: Configuring card %d\n",icrd); fflush(stdout);
	sprintf(cmd,"robinconfig %d 5",icrd);
	if( executedup(cmd,loclog,loclog) < 0 || strstr(loclog,"exception") != NULL ) {
	  sprintf(tmpstr,"ERROR: Card %d:\n%s\n",(icrd+1),failmsg);
	  card_stat++;
	}
	if( (buferr = strstr(cbuf,"Error")) == NULL ) {
	  fprintf(stdout,"%s\n",buferr); fflush(stdout);
	  cbuf = buferr;
	}
      } 
      else
	appendlog(cardlog,(char*)"Card already configured\n");
    }
    else {
      appendlog(cardlog,(char*)"ERROR: cat /proc/robin failed\n");
      if( loclog == NULL )
	appendlog(cardlog,(char*)"ERROR: too few entrues in /proc/robin\n");
      return -1;
    }
  }    
  fprintf(stdout,"\nINFO: All cards configured\n"); fflush(stdout);
  
  return card_stat;
}

int runBist(int ncards,cbdata *currentLog) {
  unsigned int icmd;
  int statapp,icard,istr;
  int crd_err;
  short brkapp;
  /* char *tmppos,*tempinfo; */
  char tmpbuf[DefaultSize],ittydev[DefaultSize],*logpos,*cardlogpos,*errlogpos;
  const char *ttydev = "/dev/robintty_";
  /* const char *extloopback = "setenv Rolextloop 1"; */
  const char *bcmd[] = {"setenv SkipReload 1","setenv EbistEnabled 1","printenv","bootelf 0xff800000","0","0"};
  const char *post_act[] = {"setenv SkipReload 0","setenv bootcmd bootelf 0xff800000","setenv EbistEnabled 0","setenv Rolextloop 0","printenv"};
  const char *str2extract[] = {"temperatur value"};
  /*int testBuilddate;*/
  int ttyfd;
  
  if( ncards == 0 ) {
    appendlog(currentLog->errlog,(char*)"\nError: unknown number of cards, \ntry 'available cards' option first");
    return -1;
  }

  fprintf(stdout,"\nINFO: Runnig eBist test\n"); 
  statapp = 0; 
  for( icard = 0; icard < ncards; icard++ ) {
    if( RobinInfo[icard].cardSelect == 1 ) {
      sprintf(ittydev,"%s%d",ttydev,icard);
      opentty(&ttyfd,ittydev); 
      if( ttyfd > 0 ) { 
	sprintf(tmpbuf,"\n\nINFO:----- Processing CARD %d %s, serial %d --------\n",
		icard,RobinInfo[icard].origin,RobinInfo[icard].boardspec[1]);
	appendlog(currentLog->log,tmpbuf);
	appendlog(currentLog->errlog,tmpbuf);
	fprintf(stdout,"%s",tmpbuf); fflush(stdout);
	cardlogpos = currentLog->log + strlen(currentLog->log);
	crd_err = 0; brkapp = 0; 
	
	fprintf(stdout,"INFO:Stopping PPC..."); fflush(stdout);
	stopPpc(&ttyfd,currentLog);
	fprintf(stdout,"Done\n"); fflush(stdout);
	
	/*if( !runBase.extLoop ) 
	  statapp-= run_ppccmd(1,currentLog,(ttyfd+icard),(char*)extloopback);*/
	
	logpos = currentLog->log + strlen(currentLog->log);
	errlogpos = currentLog->errlog + strlen(currentLog->errlog);
	fprintf(stdout,"INFO: Restarting PPC and running eBist test\n"); fflush(stdout);
	for( icmd = 0; icmd < (sizeof(bcmd)/sizeof(bcmd[0])); icmd++ ) {
	  statapp-= run_ppccmd(1,currentLog,&ttyfd,(char*)bcmd[icmd]);
	  
	  if( strstr(logpos,"Bus Fault") != NULL || strstr(logpos,"Application terminated") != NULL  )
	    break;
	}
	if( statapp != -5 ) {
	  for( icmd = 0; icmd < (sizeof(post_act)/sizeof(post_act[0])); icmd++ ) 
	    run_ppccmd(1,currentLog,&ttyfd,(char*)post_act[icmd]);
	}

	for( istr = 0; istr < (int)(sizeof(str2extract)/sizeof(str2extract[0])); istr++ ) 
	  extract_str(logpos,currentLog->errlog,str2extract[istr]);
	/*tempinfo = NULL; tmppos = logpos;
	while( strstr(tmppos,"temperatur value") != NULL ) {
	  tempinfo = strstr(tmppos,"temperatur value");
	  tmppos+= strlen("temperatur value");
	}
	if( tempinfo != NULL ) {
	  if( (tmppos = index(tempinfo,'\n')) != NULL ) {	    

	    *tmppos = '\0';
	    sprintf(tmpbuf,"INFO: %s\n",tempinfo);

	    *tmppos = '\n';
	    appendlog(currentLog->errlog,tmpbuf);
	  }
	  else
	    appendlog(currentLog->errlog,(char*)"Error reading Robin temperature\n");
	    }*/

	fprintf(stdout,"INFO: eBist test done, checking for errors...\n"); fflush(stdout);
	if( ebistErrCheck(currentLog,logpos) != 0 ) {
	  statapp--;
	  crd_err = 1;
	}
	  
	strcat(tmpbuf,"Warning(s):\n");
	if( checkerr(cardlogpos,currentLog->errlog,ppwar,(sizeof(ppwar)/sizeof(ppwar[0])),tmpbuf) )
	  statapp-=crd_err = 1;
	
	if( newstyle_ebist(currentLog,cardlogpos) != 0 ) 
	  crd_err = 1;

	if( crd_err ) {
	  sprintf(tmpbuf,"ERROR: Ebist failed %s\n",errlogpos); 
	  fprintf(stdout,"%s",tmpbuf);
	  appendlog(currentLog->errlog,tmpbuf);
	}
	else {
	  fprintf(stdout,"INFO: Ebist OK\n");
	  appendlog(currentLog->errlog,(char*)"INFO: Ebist OK\n");
	}
	fflush(stdout);

	restartPPC(1,&ttyfd,currentLog);
	close(ttyfd); 
	ttyfd = 0;
      }
      else {
	sprintf(tmpbuf,"ERROR: File descriptor invalid, check tty connection to the card serial %d\n",
		RobinInfo[icard].boardspec[1]);
	appendlog(currentLog->log,(char*)tmpbuf);
	fprintf(stdout,"%s\n",tmpbuf); fflush(stdout);
      }
      /*sprintf(tmpbuf,"robinscope -m %d -R",icard);
	executedup(tmpbuf,currentLog->log,currentLog->errlog);*/      
    }
  }

  return statapp;
}

int ebistErrCheck(cbdata *logs,char *logpos) {
int ebistStat;
  unsigned int errcode,irol,icheck;
  const char *serchstr = "BIST result ";
  char *pos,*tmppos;
  char tmpstr[DefaultSize];

  ebistStat = 0;
  for( irol = 0; irol < NRols; irol++ ) {
    if( (pos = strstr(logpos,serchstr)) != NULL ) {
      pos+= strlen(serchstr);
      tmppos = strstr(pos,"\n");
      if( (tmppos - pos) < DefaultSize ) {
	strncpy(tmpstr,pos,(size_t)(tmppos - pos));
	tmpstr[(unsigned int)(tmppos - pos)] = '\0';
	sscanf(tmpstr,"%xd",&errcode);
      }
      
      for( icheck = 0; icheck < sizeof(ebisterr)/sizeof(ebisterr[0]); icheck++ ) {
	if( errcode & ebisterr[icheck].errshift ) {
	  sprintf(tmpstr,"ERROR: ROL %d: failed test %s\n",irol,ebisterr[icheck].errstring);
	  appendlog(logs->errlog,tmpstr);
	  ebistStat--;
	}
      }
      logpos = pos;
    }
    else {
      sprintf(tmpstr,"ERROR: no statistic available for the ROL %d\n",irol);
      appendlog(logs->errlog,tmpstr);
      return -1;
    }
  }

  return ebistStat;
}

int restartPPC(int ncards, int *ttyfd, cbdata *currentLog) {
  int icrd,crd_err;
  char *logpos;
  
  crd_err = 0;
  for( icrd = 0; icrd < ncards; icrd++ ) {
    if( RobinInfo[icrd].cardSelect == 1 ) {
      /*appendlog(currentLog->log,"restartPPC\nDevice: ");
	appendlog(currentLog->log,runBase.robinBoardsInfo[icrd].ttyused);*/
      appendlog(currentLog->log,(char*)"\n");
      /*if( opentty(&ttyfd[icrd],runBase.robinBoardsInfo[icrd].ttyused) != 0 ) {
	appendlog(currentLog->log,"restartPPC: opentty failed\n");
	appendlog(currentLog->log,runBase.robinBoardsInfo[icrd].ttyused);
	return -1;
	}*/

      crd_err = 0;
      logpos = currentLog->log + strlen(currentLog->log);

      stopPpc(&ttyfd[icrd],currentLog);
      if( strstr(logpos,"=>") != NULL ) {
	if( run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"bootelf 0xff800000") )
	  crd_err = 1;
      }
      else {
	if( run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"x") )
	  crd_err = 1;
      }

      /* close(ttyfd[icrd]); */
    }
    else
      appendlog(currentLog->log,(char*)"card disabled\n");
  }

  return crd_err;
}

void stopPpc(int *ttyfd, cbdata  *currentLog) {
  char *logpos;
  
  logpos = currentLog->log + strlen(currentLog->log);
  run_ppccmd(1,currentLog,ttyfd,(char*)"\r");
  if( strstr(logpos,"0 : Terminate") != NULL ) {
    run_ppccmd(1,currentLog,ttyfd,(char*)"0");
    run_ppccmd(1,currentLog,ttyfd,(char*)"\r");
  }
}

int run_ppccmd(int ncards,cbdata* cardPciLog,int *fdtty, char *PPCcmd) {
  int icrd,statapp,cmdDone,nrepeat;
  char *logpos;

  if( fdtty == NULL ) {
    appendlog(cardPciLog->log,(char*)"\nERROR: No tty dev opened, select \"Open tty\" and try again\n");
    return -1;
  }

  nrepeat = cmdDone = statapp = 0;
  for( icrd = 0; icrd < ncards; icrd++ ) {
    while( !cmdDone && nrepeat++ < 5 ) { 
      logpos = cardPciLog->log + strlen(cardPciLog->log);

      /* log file for the test begins here */
      runppc_select(fdtty[icrd],PPCcmd,cardPciLog->log);
      if( strstr(logpos,"ERROR: Application terminated") != NULL && strstr(logpos,"FATAL ERROR") != NULL ) {
	appendlog(cardPciLog->errlog,(char*)"\nApplication terminated, FATAL ERROR\n");
	statapp = 1;
	cmdDone = 1;
      }

      if( strstr(logpos,"Unknown command") != NULL ) /*recover from the unknown command */
	runppc_select(fdtty[icrd],(char*)"\r",cardPciLog->log);
      else
	cmdDone = 1;
    }
  }

  return statapp;
}

int runppc_select(int tty_fd,char *command,char *buflog) {
  struct pollfd ufds;
  short brkapp;
  unsigned int istr;
  int nfds,count,timeout;
  ssize_t  in_byte;
  char io_buf[DefaultSize],loclog[MAX_BYTE];
  char *logpos,*pcmd,*brk_pos,*except_pos;
  size_t shift_pos;
  const char *act_str[] = {"=>","Entering main loop\n","x : Continue",
			   "Entering main loop. Press any key to enter user interface",
			   "Application terminated"};
  int appstat;

  if( tty_fd <= 0 ) {
    fprintf(stderr,"\nERROR: runppc: tty device not open, or open failed\n");
    return -2;
  }  
  tcflush(tty_fd,TCIOFLUSH);

  strcpy(io_buf,command);
  if( strlen(io_buf) > 1  && strstr(io_buf,"\r") == NULL )
    strcat(io_buf,"\r");
  write(tty_fd,io_buf,(strlen(io_buf)*sizeof(char)));
  tcdrain(tty_fd);

  count = -40;
  /*if( strstr(command,"saveenv") != NULL )
    count = 4;
  if( strstr(command,"bootelf") != NULL )
    count = 20;
  if( strstr(command,"reset") != NULL )
  count = 20;*/
    /*count = 20;*/
  if( strcmp(command,"\r") == 0 ) 
    timeout = 500;
  else
    timeout = count*TimeOut;

  ufds.fd = tty_fd;
  ufds.events = (POLLIN | POLLPRI);
  loclog[0] = '\0'; logpos = loclog; brkapp = 0; in_byte = 10; appstat = 0;
  while( (nfds = poll(&ufds,1,timeout)) > 0  ) {
    shift_pos = 0;
    if( (in_byte = read(ufds.fd,io_buf,(DefaultSize-1))) > 0 ) {
      io_buf[(in_byte/sizeof(char))] = '\0';
      appendlog(loclog,io_buf);
      shift_pos = shift_pos + strlen(io_buf);
    }
   
    if( strstr(logpos,"stop autoboot") != NULL && (pcmd = strstr(command,"reset")) == NULL ) {
      write(tty_fd,"\r",sizeof(char));
      tcdrain(tty_fd);
      logpos = logpos + strlen(loclog);
      break;
    }
    for( istr = 0; istr < (sizeof(act_str)/sizeof(act_str[0])); istr++ )
      if( (brk_pos = strstr(logpos,act_str[istr])) != NULL  ) {
	except_pos = strstr(logpos,"=> data");
	if( brk_pos > except_pos )
	  brkapp = 1;
	if( strstr(logpos,"Application terminated") != NULL )
	  appstat = 1;
      }

    /*appendlog(buflog,logpos,0);*/
    if( brkapp == 1 ) {
      break;    
    }
    /* logpos = logpos + shift_pos; */
  }
  appendlog(buflog,loclog);

  tcflush(tty_fd,TCIFLUSH);
  
  return appstat;
}

int checkerr(char *currentLog,char *errlog,const char **errlist, unsigned int elistsize,  char *header) {
  unsigned int ierr;
  int head,err_size,errdet;
  char *pos,*pos_end,*log_pos,cerr[DefaultSize];

  errdet = head = 0;
  for( ierr = 0; ierr < elistsize; ierr++ ) {
    log_pos = currentLog;
    while( (pos = strstr(log_pos,errlist[ierr])) != NULL ) {
      if( !head ) {
	appendlog(errlog,header);
	head = 1;
      }

      pos_end = strstr(pos,"\n");
      err_size = (int)(pos_end - pos) < DefaultSize ? (int)(pos_end - pos):DefaultSize;
      strncpy(cerr,pos,err_size); cerr[(err_size/sizeof(char))] = '\0';
      appendlog(errlog,cerr);
      appendlog(errlog,(char*)" see main log for details\n");
      log_pos = pos + strlen(cerr);
      errdet++;
    }
  }

  return errdet;
}

int newstyle_ebist(cbdata *logs,char *logpos) {
  unsigned int errcode,irol,icheck,irolfound,crderr;
  const char *serchstr = "BIST result ";
  char *pos,*tmppos;
  char tmpstr[DefaultSize];

  crderr = irolfound = 0;
  for( irol = 0; irol < NRols; irol++ ) {
    sprintf(tmpstr,"%s",serchstr);
    if( (pos = strstr(logpos,tmpstr)) != NULL ) {
      irolfound++;
      pos+= strlen(tmpstr);
      tmppos = strstr(pos,"\n");
      if( (tmppos - pos) < DefaultSize ) {
	strncpy(tmpstr,pos,(size_t)(tmppos - pos));
	tmpstr[(unsigned int)(tmppos - pos)] = '\0';
	sscanf(tmpstr,"%xd",&errcode);
      }
      
      for( icheck = 0; icheck < 6; icheck++ ) {
	if( errcode & ebisterr[icheck].errshift ) {
	  sprintf(tmpstr,"ERROR: ROL %d: Invalid/failed test eBist %s\n",irol,ebisterr[icheck].errstring);
	  appendlog(logs->errlog,tmpstr);
	  crderr = 1;
	}
	else {
	  sprintf(tmpstr,"eBist OK\n");
	  appendlog(logs->log,tmpstr);
	}
      }   
    } 
  }
  if( irolfound < NRols ) {
    appendlog(logs->errlog,(char*)"Not all ROL statistic is available\n");
    return -1;
  }

  return crderr;
}

int selftest() {
  char logapp[MAX_BYTE],errmsg[MAX_BYTE];
  const char *modules[] = {"robin","robintty","io_rcc","cmem_rcc"};
  unsigned int imod;

  logapp[0] = errmsg[0] = '\0';
  if( executedup((char*)"/sbin/lsmod",logapp,logapp) < 0 ) {
    perror("execute");
  }
  
  for( imod = 0; imod < sizeof(modules)/sizeof(modules[0]); imod++ ) 
    if( strstr(logapp,modules[imod]) == NULL ) {
      if( imod == 0 ) 
	appendlog(errmsg,(char*)"ERROR: ");
      appendlog(errmsg,(char*)modules[imod]);
      appendlog(errmsg,(char*)" ");
    }

  if( strlen(errmsg) > 0  ) {
    appendlog(errmsg,(char*)"missing, program will not run properly, quitting now\n\n");
    appendlog(errmsg,logapp);
    fprintf(stdout,"%s\n",errmsg);
    strcpy(rlog,errmsg);
    return -1;
  }
  
  return 0;
}

int getDeviceInfo(char *scanlogpos,robin_Inf_t *RobinInf) {
  char *scanlog,*pser;
  char *argstline[Max_NumItems],*argstitem[Max_NumItems];
  unsigned int item_nr;
  unsigned int j;
  int device_nr,iarg,jarg,line_nr;
  const char *labels = "BoardId";

  if( (scanlog = malloc((strlen(scanlogpos)+1)*sizeof(char))) == NULL ) {
    fprintf(stdout,"ERROR: getDeviceInfo: Cannot allocate temporary buffer\nProgram terminated\n");
    return -1;
  }
  if( (pser = strstr(scanlogpos,labels)) == NULL )
    return -1;
  sprintf(scanlog,"%s",pser);
  jarg = splitext(scanlog,argstline);
  
  device_nr = 0;
  for( line_nr = 1; line_nr < jarg; line_nr++ ) {
    if( strstr(argstline[line_nr],"ROBIN") != NULL ) {
      iarg = splitarg(argstline[line_nr],argstitem);
      for( item_nr = 0; item_nr < (unsigned int)iarg; item_nr++ ) {
	if( item_nr == 1 )
	  sprintf(RobinInf[device_nr].boardType,"%s",argstitem[item_nr]);
	else {
	  if( item_nr == 2 ) {
	    for( j = 0; j < (sizeof(serial_prefix)/sizeof(serial_prefix[0])); j++ ) 
	      if( strncmp(argstitem[item_nr],serial_prefix[j][0],strlen(serial_prefix[j][0])) == 0 ) {
		pser = argstitem[item_nr] + strlen(serial_prefix[j][0]);
		sscanf(pser,"%d",&(RobinInf[device_nr].boardspec[1]));
		sscanf(serial_prefix[j][1],"%s",RobinInf[device_nr].origin);
		sscanf(serial_prefix[j][0],"%s",RobinInf[device_nr].prefix);
		break;
	      }
	    if( j == (sizeof(serial_prefix)/sizeof(serial_prefix[0])) ) {
	      sprintf(RobinInf[device_nr].origin,"Unknown origin");
	      RobinInf[device_nr].prefix[0] = '\0';
	      sscanf(argstitem[item_nr],"%d",&(RobinInf[device_nr].boardspec[1]));
	    }
	  }
	  else 
	    sscanf(argstitem[item_nr],"%d",&(RobinInf[device_nr].boardspec[item_nr]));
	}
      }
      ++device_nr;
    }
  }
  free(scanlog);

  return (device_nr-1);
}

int opentty(int *tty_fd, char *device) {
  struct termios ttyter;

  if( (*tty_fd = open(device,(O_RDWR|O_NONBLOCK|O_NOCTTY))) == -1 ) { 
    perror(device);
    return -1;
  }

  /* modify parameters of tty device */  
  tcgetattr(*tty_fd,&ttyter);

  ttyter.c_iflag =  (IGNCR | ICRNL); 
  ttyter.c_oflag = ( OCRNL );

  ttyter.c_cflag = (ttyter.c_cflag);
  /* allow single character input */
  ttyter.c_lflag = ((ttyter.c_lflag & ~ICANON) & ~ECHO);

  cfsetispeed(&ttyter,B115200);
  cfsetospeed(&ttyter,B115200);
  tcsetattr(*tty_fd, TCSANOW, &ttyter);

  return 0;
}

void extract_str(char* log, char *destlog, const char *test_str) {
  char *tmppos,*tempinfo;
  char tmpbuf[DefaultSize];

  tempinfo = NULL; tmppos = log;
  while( strstr(tmppos,test_str) != NULL ) {
    tempinfo = strstr(tmppos,test_str);
    tmppos+= strlen(test_str);
  }
  if( tempinfo != NULL ) {
    if( (tmppos = index(tempinfo,'\n')) != NULL ) {	    
      /* terminate the string */
      *tmppos = '\0';
      sprintf(tmpbuf,"INFO: %s\n",tempinfo);
      /* restore the original value */
      *tmppos = '\n';
      appendlog(destlog,tmpbuf);
    }
    else {
      sprintf(tmpbuf,"WARNING: Error reading %s\n",test_str);
      appendlog(destlog,tmpbuf);
    }
  }
}
