#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#define _XOPEN_SOURCE 600

#include "RobinTestSuite/pdef.h"
#include "RobinTestSuite/ts.h"

/* Callbacks */
static void runselected();
static void process_log();
static void process_errlog();
/* static void stoppushed(); */
static void runpushed();
static void ppccmd();
static void get_ppccmd();
static void button_set();
static void activate_crd();
static void qpushed();
static void blowupShell();
static void removeShell();
static void userSelect();
static void fileSelect();
static void fconf_sel();
static void sens_help();
static void ts_help();
void heartbit(XtPointer);

extern XtAppContext app;
XtWorkProcId procid = -1; 
runshell_t runBase;
Widget logwin = NULL;
Widget TestBase;
short *SpecialConf;
char *p_prefix;

/*Widget mainBase,mshell,*iconW;
  XtPointer pass_client_data;*/

Widget CreateConsole(Widget parent,Widget base) {
  Widget cntFrame,cntbutFrame,cmdFrame,AuxFrame;
  Widget ppccmdButton,quit_button,help_button;
  long fdflags;
  unsigned int imenu;
  const char *runbutt[] = {"RUN\nEBist","RUN\nconfig","RUN\ntests"};
  const char *testsel[] = {"ebist","config","tests"};

  strcpy(fpga_elf[0],default_conf_files[0]);
  strcpy(fpga_elf[1],default_conf_files[1]);
  strcpy(fpga_elf[2],default_conf_files[2]);
  strcpy(fpga_elf[3],default_conf_files[3]);

  /* Program will handle stdout and stderr streams. */
  /* stdout pipe */
  if( pipe(pipefdloc) < 0 ) {
    perror("pipe");
    return NULL;
  }

  /* stderr pipe */
  if( pipe(pipefderrloc) < 0 ) {
    perror("pipeerr");
    return NULL;
  }

  if( dup2(pipefdloc[1],1) < 0 )
    perror("dup: ");
  if( (fdflags = fcntl(pipefdloc[0],F_GETFL)) == -1 ) {
    perror("fcntl getflag stdout::");
    fprintf(stdout,"errrr"); fflush(stdout);
  }
  fdflags |= O_NONBLOCK;
  if( fcntl(pipefdloc[0],F_SETFL,fdflags)  == -1 ) {
    perror("fcntl setflag stdout::");
  }

  if( dup2(pipefderrloc[1],2) < 0 )
    perror("dup: ");
  if( (fdflags = fcntl(pipefderrloc[0],F_GETFL)) == -1 ) {
    perror("fcntl getflag stdout::");
    fprintf(stdout,"errrr"); fflush(stdout);
  }
  fdflags |= O_NONBLOCK;
  if( fcntl(pipefderrloc[0],F_SETFL,fdflags)  == -1 ) {
    perror("fcntl setflag stdout::");
  }

  ufdsloc[0].fd = pipefdloc[0];  ufdsloc[1].fd = pipefderrloc[0];
  ufdsloc[0].events = ufdsloc[1].events = (POLLIN | POLLPRI);
  
  /* initialise/reset basic parameters, create currentLog object */
  runBase.shell = NULL; runBase.ttyfd = NULL;
  if( (runBase.currentLog = calloc(1,sizeof(cbdata))) == NULL ) {
    perror("currentLog alloc:");
    return NULL;
  }

  /* check if the drivers are loaded */
  selftest(base);

  /* check number of etherent interfaces (excluding loopback) */
  runBase.Neth = getifname(ifInfo);
  /* Frame container for the control buttons */
  cntFrame = XtVaCreateManagedWidget("Control frame",xmFrameWidgetClass, parent,
				     XmNtopAttachment, XmATTACH_FORM,
				     XmNrightAttachment, XmATTACH_FORM,NULL);
  
  /* manager for the config/test buttons */
  cntbutFrame = XtVaCreateManagedWidget("cnt butframe",xmRowColumnWidgetClass,cntFrame, NULL );
  XtVaSetValues(cntbutFrame,XtVaTypedArg,XmNbackground,XmRString,LightYellow1,
		(strlen(LightYellow1)+1),NULL);

  for( imenu = 0; imenu < (sizeof(runbutt)/sizeof(runbutt[0])); imenu++ ) {
    runBase.runButtons[imenu] = XtVaCreateManagedWidget(runbutt[imenu],xmPushButtonWidgetClass, 
							cntbutFrame,XmNshowAsDefault, True, 
							XmNdefaultButtonShadowThickness, 1,NULL);

    XtVaSetValues(runBase.runButtons[imenu],XtVaTypedArg,XmNforeground,XmRString,actgreen,(strlen(actgreen)+1),
		  XtVaTypedArg,XmNbackground,XmRString,royalBlue,(strlen(royalBlue)+1),NULL); 
    XtAddCallback(runBase.runButtons[imenu],XmNactivateCallback,runpushed,(XtPointer)testsel[imenu]);
    if( imenu < XtNumber(selectrun_help) )
      XtAddCallback(runBase.runButtons[imenu],XmNhelpCallback,ts_help,(XtPointer)selectrun_help[imenu]);
    else
      XtAddCallback(runBase.runButtons[imenu],XmNhelpCallback,ts_help,NULL);
  }

  cmdFrame = XtVaCreateManagedWidget("cmdFrame",xmFrameWidgetClass, parent,
				     XmNtopAttachment, XmATTACH_WIDGET,
				     XmNtopWidget, parent,XmNtopOffset, 5,NULL);
  XtVaSetValues(cmdFrame,XtVaTypedArg,XmNbackground,XmRString,lightgray,strlen(lightgray)+1,NULL); 

  ppccmdButton = XtVaCreateManagedWidget("ppcCmd",xmPushButtonWidgetClass,cmdFrame,
					/* XmNbottomAttachment, XmATTACH_FORM, */
					XmNtopAttachment, XmATTACH_FORM, 
					/* XmNrightAttachment, XmATTACH_FORM,  */
					 XmNleftAttachment, XmATTACH_FORM,
					XmNshowAsDefault, True, 
					XmNdefaultButtonShadowThickness, 2,NULL);

  XtVaSetValues(ppccmdButton,XtVaTypedArg,XmNforeground,XmRString,actwhite,(strlen(actwhite)+1),
		XtVaTypedArg,XmNbackground,XmRString,darkSlateBlue,(strlen(darkSlateBlue)+1),NULL); 
  XtAddCallback(ppccmdButton, XmNactivateCallback, ppccmd, NULL);
  XtAddCallback(ppccmdButton,XmNhelpCallback,ts_help,(XtPointer)ppcmd_help[0]);


  XtSetSensitive(ppccmdButton, True /* False */);

  /* QUIT button frame */
  AuxFrame = XtVaCreateManagedWidget("AuxFrame",xmRowColumnWidgetClass, parent,
				     XmNbottomAttachment, XmATTACH_WIDGET,
				     XmNbottomWidget, parent,
				     XmNbottomOffset, 5, /* XmNbottomOffset, 10, */
				     /*XmNtopWidget,togFrame, */ NULL);
  XtVaSetValues(AuxFrame,XtVaTypedArg,XmNbackground,XmRString,lightgray,(strlen(lightgray)+1),NULL);

  help_button = XtVaCreateManagedWidget("Help",xmPushButtonWidgetClass, AuxFrame ,
					XmNtopAttachment, XmATTACH_FORM,
					/* XmNrightAttachment, XmATTACH_FORM, */
					/*XmNleftAttachment, XmATTACH_FORM, */
					XmNshowAsDefault, True, 
					XmNdefaultButtonShadowThickness, 1,NULL);
  XtVaSetValues(help_button,XtVaTypedArg,XmNforeground,XmRString,"white",(strlen("white")+1),
		XtVaTypedArg,XmNbackground,XmRString,"slate gray",(strlen("slate gray")+1),NULL); 
  XtAddCallback(help_button,XmNactivateCallback,sens_help,NULL);
  /* XtAddCallback(help_button,XmNhelpCallback,help4_help,NULL); */
  XtAddCallback(help_button,XmNhelpCallback,ts_help,(XtPointer)help4help[0]);


  quit_button = XtVaCreateManagedWidget("Quit",xmPushButtonWidgetClass, AuxFrame ,
					XmNbottomAttachment, XmATTACH_FORM, 
					/* XmNrightAttachment, XmATTACH_FORM,  */
					/* XmNleftAttachment, XmATTACH_FORM, */
					XmNshowAsDefault, True, 
					XmNdefaultButtonShadowThickness, 2,NULL);
  XtVaSetValues(quit_button,XtVaTypedArg,XmNforeground,XmRString,black,(strlen(black)+1),
		XtVaTypedArg,XmNbackground,XmRString,actgreen,(strlen(actgreen)+1),NULL); 
  XtAddCallback(quit_button,XmNactivateCallback,qpushed,NULL);


  XtManageChild(parent);
  XmUpdateDisplay(parent);

  return parent;
}

static void runpushed(Widget rbutt, XtPointer client_data,XmAnyCallbackStruct *call_data) { 
  FILE *fdcon;
  Widget mshell,form,field,confile_select;
  Widget *statButtons,*errButtons,okButt;
  Widget Conffile_butt[CONF_FILES];
  Widget *RTConf_butt;
  Widget *TButt,runSelected;
  XmString xms;
  int iwid,Max_menu,Max_helpmenu,butt_pos;
  short autorun;  
  const char bgcol[] = "azure";
  static const char *conffileLabels[] = {"PPC ELF","FPGA bitfile","FLASH bitfile"};
  static const char *fieldLabels[] = {"PCI bus scan:","PLX EPROM read:","u-boot write:",
				      "environment write:","PPC write to FLASH:","FPGA write to FLASH:",
				      "Configure FPGA:","Open tty:","Run eBIST:","Restart PPC"};
  static const char *ebist_test[] = {"Open tty:","Run eBIST:","OTP check","FPGA validation",
				     "S-link Clock","PHY check","TLK check","ROL memory check",};
  static const char *funct_test[] = {"Net Requester:","PCI Requester:","Restart PPC"};
  char **pmenu,**phelp;
  int n;
  Arg args[10];
  char tmpstr[DefaultSize];

  StopAll = 0;

  if( strstr((const char*)"ebist",(char*)client_data) != NULL ) {
    runBase.autorun = autorun = 1;
    pmenu = (char**)ebist_test;
    Max_menu = XtNumber(ebist_test);
    phelp = (char**)autoebist_help;
    Max_helpmenu = XtNumber(autoebist_help);
  }
  else {
    if( strstr((const char*)"config",(char*)client_data) != NULL ) {
      runBase.autorun = autorun = 0;
      pmenu = (char**)fieldLabels;
      Max_menu = XtNumber(fieldLabels); 
      phelp = (char**)confprog_help;
      Max_helpmenu = XtNumber(confprog_help);
    }
    else {
      runBase.autorun = autorun = 0;
      pmenu = (char**)funct_test;
      Max_menu = XtNumber(funct_test); 
      phelp = (char**)tests_help;
      Max_helpmenu = XtNumber(tests_help);
    }
  } 

  XtSetSensitive(runBase.runButtons[0], False);
  XtSetSensitive(runBase.runButtons[1], False);
  XtSetSensitive(runBase.runButtons[2], False);
  runBase.shell = mshell = XtVaCreatePopupShell((char*)client_data,xmDialogShellWidgetClass, rbutt,NULL);
  
  TestBase = XtVaCreateWidget("base",xmPanedWindowWidgetClass,mshell,
			  XmNsashWidth, 5,XmNsashHeight, 5, NULL);
  XtVaSetValues(TestBase,XtVaTypedArg,XmNbackground, XmRString,actwhite,strlen(actwhite)+1,NULL);

  runBase.nfields = Max_menu;
  runBase.statButtons = statButtons = calloc(Max_menu,sizeof(Widget));
  runBase.errButtons  = errButtons = calloc(Max_menu,sizeof(Widget));
  runBase.TButt = TButt = calloc(Max_menu,sizeof(Widget));
  runBase.select = calloc(Max_menu,sizeof(short));
 
  /* pointers to log/errorlog for each test */
  (runBase.currentLog)->testlog = calloc(Max_menu,sizeof(char*));
  (runBase.currentLog)->testerrlog = calloc(Max_menu,sizeof(char*));

  /*check mallocs */
  if( statButtons == NULL || errButtons == NULL || TButt == NULL || 
      runBase.select  == NULL || (runBase.currentLog)->testlog == NULL || (runBase.currentLog)->testerrlog == NULL ) {
    notice_win(rbutt,(char*)"Initialisation failed (bad runBase.currentLog malloc in runpushed).\n Program terminated",
	       (char*)"ERROR");
    return;
  }
    
  /* reset push-button logs. At this stage there are not yet allocated*/
  runBase.robinBoardsInfo = NULL;

  /* reset logs */
  *(runBase.currentLog->log) = *(runBase.currentLog->errlog) = '\0';

  form = XtVaCreateWidget("fileselbut",xmFormWidgetClass,TestBase, XmNfractionBase, 12, NULL);
  XtVaSetValues(form,XtVaTypedArg,XmNbackground, XmRString,bgcol,strlen(bgcol)+1,NULL);

  confile_select = XtVaCreateManagedWidget("Configuration files:", xmLabelGadgetClass, form,
					   XmNtopAttachment, XmATTACH_FORM, 
					   XmNbottomAttachment, XmATTACH_FORM,
					   XmNleftAttachment, XmATTACH_FORM, 
					   XmNrightAttachment, XmATTACH_POSITION,
					   XmNrightPosition, 3, XmNalignment, XmALIGNMENT_END,
					   NULL );

  butt_pos = 3;
  for( iwid = 0; iwid < (CONF_FILES-1); iwid++ ) {
    Conffile_butt[iwid] = XtVaCreateManagedWidget(conffileLabels[iwid],xmPushButtonWidgetClass, form,
						  XmNrightAttachment,XmATTACH_FORM, 
						  XmNleftAttachment,XmATTACH_POSITION,
						  XmNleftPosition, butt_pos, 
						  XmNrightAttachment, XmATTACH_POSITION,
						  XmNrightPosition, (butt_pos+2),NULL);
    XtVaSetValues(Conffile_butt[iwid],XtVaTypedArg,XmNforeground,XmRString,"white",(strlen("white")+1),
		XtVaTypedArg,XmNbackground,XmRString,"royal blue",(strlen("royal blue")+1),NULL); 
    XtAddCallback(Conffile_butt[iwid],XmNactivateCallback,fileSelect,(XtPointer)iwid);
    if( iwid < (int)XtNumber(conffileshelp) )
      XtAddCallback(Conffile_butt[iwid],XmNhelpCallback,ts_help,(XtPointer)conffileshelp[iwid]);
    else
      XtAddCallback(Conffile_butt[iwid],XmNhelpCallback,ts_help,NULL);
    butt_pos+=2;
  }
 
  XtManageChild(form); 
  
  form = XtVaCreateWidget("selbut",xmFormWidgetClass,TestBase, XmNfractionBase, 12, NULL);
  XtVaSetValues(form,XtVaTypedArg,XmNbackground, XmRString,bgcol,strlen(bgcol)+1,NULL);

  /* read last configuration from the history file (if available) */
  n = 0;
  if( (fdcon = fopen(confname,"r")) != NULL ) {
    n = fscanf(fdcon,"%hd%hd%hd%hd%hd",&runBase.Dolar,&runBase.robintty,&runBase.nettran,
	       &runBase.extLoop,&runBase.ethdev);
    fclose(fdcon);
  }

  if( n < (int)(sizeof(userSelLabels)/sizeof(userSelLabels[0])) ) {
    runBase.Dolar = runBase.robintty = runBase.nettran = runBase.extLoop = 1;
    runBase.ethdev = 0;
  }

  /* Set run  configuration button */
  field = XtVaCreateManagedWidget("Select configuration:", xmLabelGadgetClass, form,
				  XmNtopAttachment, XmATTACH_FORM, 
				  XmNbottomAttachment, XmATTACH_FORM,
				  XmNleftAttachment, XmATTACH_FORM, 
				  XmNrightAttachment, XmATTACH_POSITION,
				  XmNrightPosition, 3, XmNalignment, XmALIGNMENT_END,
				  NULL );

  if( (RTConf_butt = malloc(sizeof(Widget)*sizeof(userSelLabels)/sizeof(userSelLabels[0]))) == NULL ) 
    return;

  RTConf_butt[1] = XtVaCreateManagedWidget(userSelLabels[1][runBase.robintty],xmPushButtonWidgetClass, form,
					   XmNrightAttachment,XmATTACH_FORM, 
					   XmNleftAttachment,XmATTACH_POSITION,
					   XmNleftPosition, 5, 
					   XmNrightAttachment, XmATTACH_POSITION,
					   XmNrightPosition, 7,NULL);

  /* Dolar or Int. gen, UDP or RAWSOCK, eth0 or eth1 */
  /*if( !autorun ) {*/
    RTConf_butt[0] = XtVaCreateManagedWidget(userSelLabels[0][runBase.Dolar],xmPushButtonWidgetClass, form,
					     XmNrightAttachment,XmATTACH_FORM, 
					     XmNleftAttachment,XmATTACH_POSITION,
					     XmNleftPosition, 3, 
					     XmNrightAttachment, XmATTACH_POSITION,
					     XmNrightPosition, 5,NULL);

    RTConf_butt[2] = XtVaCreateManagedWidget(userSelLabels[2][runBase.nettran],xmPushButtonWidgetClass,form ,
					     XmNrightAttachment,XmATTACH_FORM, 
					     XmNleftAttachment,XmATTACH_POSITION,
					     XmNleftPosition, 7, 
					     XmNrightAttachment, XmATTACH_POSITION,
					     XmNrightPosition, 9,NULL);

    RTConf_butt[4] = XtVaCreateManagedWidget(ifInfo[runBase.ethdev].if_name,xmPushButtonWidgetClass,form ,
					     XmNrightAttachment,XmATTACH_FORM, 
					     XmNleftAttachment,XmATTACH_POSITION,
					     XmNleftPosition, 11, 
					     XmNrightAttachment, XmATTACH_POSITION,
					     XmNrightPosition, 12,NULL);
  /* } */

  /* EXT loopback */
  RTConf_butt[3] = XtVaCreateManagedWidget(userSelLabels[3][runBase.extLoop],xmPushButtonWidgetClass,form ,
					   XmNrightAttachment,XmATTACH_FORM, 
					   XmNleftAttachment,XmATTACH_POSITION,
					   XmNleftPosition, 9, 
					   XmNrightAttachment, XmATTACH_POSITION,
					   XmNrightPosition, 11,NULL);

  /* set properties an callbacks for all the run-time configuration buttons */
  for( n = 0; n < (int)(sizeof(userSelLabels)/sizeof(userSelLabels[0])); n++ ) {
    XtVaSetValues(RTConf_butt[n],XtVaTypedArg,XmNforeground,XmRString,"white",(strlen("white")+1),
		  XtVaTypedArg,XmNbackground,XmRString,"royal blue",(strlen("royal blue")+1),NULL); 
    XtAddCallback(RTConf_butt[n],XmNactivateCallback,userSelect,(XtPointer)n);
    if( n < (int)XtNumber(runconfhelp) )
      XtAddCallback(RTConf_butt[n],XmNhelpCallback,ts_help,(XtPointer)runconfhelp[n]);
    else
      XtAddCallback(RTConf_butt[n],XmNhelpCallback,ts_help,NULL);
  }

  XtManageChild(form); 
  free(RTConf_butt);

  form = XtVaCreateWidget("form1",xmFormWidgetClass,TestBase, XmNfractionBase, 12, NULL);
  XtVaSetValues(form,XtVaTypedArg,XmNbackground, XmRString,bgcol,strlen(bgcol)+1,NULL);
  field = XtVaCreateManagedWidget("Select cards:", xmLabelGadgetClass, form,
				  XmNtopAttachment, XmATTACH_FORM, 
				  XmNbottomAttachment, XmATTACH_FORM,
				  XmNleftAttachment, XmATTACH_FORM, 
				  XmNrightAttachment, XmATTACH_POSITION,
				  XmNrightPosition, 3, XmNalignment, XmALIGNMENT_END,
				  NULL );
  
  runBase.NCards = get_cards(rbutt,runBase.currentLog,0);
  runBase.cardSelect = calloc(runBase.NCards,sizeof(short));
  SpecialConf = calloc(runBase.NCards,sizeof(short));
  if( runBase.NCards <= 0 ) {
    XtVaSetValues(field,XtVaTypedArg,XmNlabelString, XmRString,"NO CARD PRESENT",
		  strlen("NO CARD PRESENT")+1,NULL);
    XtVaSetValues(form,XtVaTypedArg,XmNbackground, XmRString,actred,strlen(actred)+1,NULL);
    XtVaSetValues(form,XtVaTypedArg,XmNforeground, XmRString,darkBlue,strlen(darkBlue)+1,NULL);
  }
  else { 
    if( (runBase.CardstatButt = calloc(runBase.NCards,sizeof(Widget))) == NULL ) {
      notice_win(rbutt,(char*)"Initialisation failed (bad CardstatButt malloc in runpushed).\n Program terminated",(char*)"ERROR");
      return;
    }
    for( iwid = 0; iwid < runBase.NCards; iwid++ ) {
      sprintf(tmpstr,"%3d",runBase.robinBoardsInfo[iwid].boardspec[1]);
      xms = XmStringCreateLtoR(tmpstr,XmSTRING_DEFAULT_CHARSET);
      runBase.CardstatButt[iwid] = XtVaCreateManagedWidget("TButt", xmToggleButtonWidgetClass, form,
							   XmNrightAttachment,XmATTACH_FORM, 
							   XmNleftAttachment,XmATTACH_POSITION,
							   XmNleftPosition, iwid+3, 
							   XmNrightAttachment, XmATTACH_POSITION,
							   XmNrightPosition, iwid+4,
							   XmNlabelString,xms,
							   NULL);
      if( runBase.robinBoardsInfo[iwid].boardspec[1] > 0 )
	XtVaSetValues(runBase.CardstatButt[iwid],XtVaTypedArg,XmNbackground, XmRString,
		      actwhite,strlen(actwhite)+1,NULL);
      else
	XtVaSetValues(runBase.CardstatButt[iwid],XtVaTypedArg,XmNbackground, XmRString,
		      actred,strlen(actred)+1,NULL);
      XtAddCallback(runBase.CardstatButt[iwid],XmNvalueChangedCallback,activate_crd,(XtPointer)iwid);
      XtAddCallback(runBase.CardstatButt[iwid],XmNhelpCallback,ts_help,(XtPointer)cardsel_help[0]);
      XmStringFree(xms);
    }
  }
  XtManageChild(form); 
  
  xms = XmStringCreateLtoR((char*)" ",XmSTRING_DEFAULT_CHARSET);
  for( iwid = 0; iwid < Max_menu; iwid++ ) {
    
    form = XtVaCreateWidget("form1",xmFormWidgetClass,TestBase, XmNfractionBase, 12, NULL);
    XtVaSetValues(form,XtVaTypedArg,XmNbackground, XmRString,bgcol,strlen(bgcol)+1,NULL);
    
    field = XtVaCreateManagedWidget(pmenu[iwid], xmLabelGadgetClass, form,
				    XmNtopAttachment, XmATTACH_FORM, 
				    XmNbottomAttachment, XmATTACH_FORM,
				    XmNleftAttachment, XmATTACH_FORM, 
				    XmNrightAttachment, XmATTACH_POSITION,
				    XmNrightPosition, 3, XmNalignment, XmALIGNMENT_END,
				    NULL );

    if( !autorun ) {
      TButt[iwid] = XtVaCreateManagedWidget("TButt", xmToggleButtonWidgetClass, form,
					    XmNrightAttachment,XmATTACH_FORM, 
					    XmNleftAttachment,XmATTACH_POSITION,
					    XmNleftPosition, 3, 
					    XmNrightAttachment, XmATTACH_POSITION,
					    XmNrightPosition, 4,
					    XmNlabelString,xms,NULL);
      XtVaSetValues(TButt[iwid],XtVaTypedArg,XmNbackground, XmRString,bgcol,strlen(bgcol)+1,NULL);
      XtAddCallback(TButt[iwid], XmNvalueChangedCallback, button_set, (XtPointer)iwid);
      if( iwid < Max_helpmenu ) 
	XtAddCallback(TButt[iwid],XmNhelpCallback,ts_help,(XtPointer)phelp[iwid]);
      else
	XtAddCallback(TButt[iwid],XmNhelpCallback,ts_help,NULL);
    }
    else {
      TButt[iwid] = NULL;
    }

    statButtons[iwid] = XtVaCreateManagedWidget("Done",xmPushButtonWidgetClass,form,
						XmNrightAttachment,XmATTACH_FORM, 
						XmNleftAttachment,XmATTACH_POSITION,
						XmNleftPosition, 4, 
						XmNrightAttachment, XmATTACH_POSITION,
						XmNrightPosition, 8,NULL);

    XtVaSetValues(statButtons[iwid],XtVaTypedArg,XmNbackground, XmRString, actwhite,
		  strlen(actwhite)+1, NULL);
    XtSetSensitive(statButtons[iwid], False);
    XtAddCallback(statButtons[iwid],XmNactivateCallback,process_log,&((runBase.currentLog)->testlog[iwid])); 
    XtAddCallback(statButtons[iwid],XmNhelpCallback,ts_help,(XtPointer)teststat_help[0]);
    

    errButtons[iwid] = XtVaCreateManagedWidget("Error",xmPushButtonWidgetClass,form,
					       XmNrightAttachment,XmATTACH_FORM, 
					       XmNleftAttachment,XmATTACH_POSITION,
					       XmNleftPosition, 8, NULL);

    XtVaSetValues(errButtons[iwid],XtVaTypedArg,XmNbackground, XmRString, gray87,strlen(gray87)+1,
		  XtVaTypedArg,XmNforeground, XmRString, actyell, strlen(actyell)+1,NULL);
    XtSetSensitive(errButtons[iwid], False);
    XtAddCallback(errButtons[iwid],XmNactivateCallback,process_errlog,&((runBase.currentLog)->testerrlog[iwid]));
    XtAddCallback(errButtons[iwid],XmNhelpCallback,ts_help,(XtPointer)teststat_help[1]);

    XtManageChild(form); 
  }
  XmStringFree(xms);

  form = XtVaCreateWidget("form2",xmFormWidgetClass,TestBase, XmNfractionBase, 7, NULL);
  XtVaSetValues(form,XtVaTypedArg,XmNbackground,XmRString,lightgray,strlen(lightgray)+1,NULL); 

  runSelected = XtVaCreateManagedWidget("Run",xmPushButtonWidgetClass,form,
					XmNrightAttachment,XmATTACH_FORM, 
					XmNleftAttachment,XmATTACH_POSITION, 
					XmNleftAttachment, XmATTACH_POSITION,XmNleftPosition, 0, 
					XmNrightAttachment, XmATTACH_POSITION,XmNrightPosition, 2,
					XmNshowAsDefault, True, XmNdefaultButtonShadowThickness, 2,
					NULL);
  
  XtVaSetValues(runSelected,XtVaTypedArg,XmNforeground,XmRString,darkBlue,strlen(darkBlue)+1,
		XtVaTypedArg,XmNbackground,XmRString,LightYellow1,strlen(LightYellow1)+1,NULL);
   
  XtAddCallback(runSelected,XmNactivateCallback,runselected, (XtPointer)pmenu); 
  runBase.runSelectedTests = runSelected;
  
  if( autorun )  {
    for( iwid = 0; iwid < Max_menu; iwid++ ) {
      if( strstr(pmenu[iwid],"Open tty:") != NULL || strstr(pmenu[iwid],"Run eBIST:") != NULL )  
	runBase.select[iwid] = 1;
    }
  }

  runBase.stopButton = XtVaCreateManagedWidget("Stop",xmPushButtonWidgetClass,form,
					       XmNrightAttachment,XmATTACH_FORM, 
					       XmNleftAttachment,XmATTACH_POSITION, 
					       XmNleftAttachment, XmATTACH_POSITION,XmNleftPosition, 3, 
					       XmNrightAttachment, XmATTACH_POSITION,XmNrightPosition, 5,
					       XmNshowAsDefault, True, XmNdefaultButtonShadowThickness, 2,
					       NULL);

  XtVaSetValues(runBase.stopButton,XtVaTypedArg,XmNforeground,XmRString,LightYellow1,strlen(LightYellow1)+1,
		XtVaTypedArg,XmNbackground,XmRString,darkBlue,strlen(darkBlue)+1,NULL);

  okButt = XtVaCreateManagedWidget("Exit",xmPushButtonWidgetClass,form,XmNrightAttachment,XmATTACH_FORM, 
				   XmNleftAttachment,XmATTACH_POSITION, 
				   XmNleftAttachment, XmATTACH_POSITION,XmNleftPosition, 5, 
				   XmNrightAttachment, XmATTACH_POSITION,XmNrightPosition, 7,
				   XmNshowAsDefault, True, XmNdefaultButtonShadowThickness, 2,
				   NULL);
  XtVaSetValues(okButt,XtVaTypedArg,XmNforeground,XmRString,darkBlue,strlen(darkBlue)+1,
		XtVaTypedArg,XmNbackground,XmRString,LightYellow1,strlen(LightYellow1)+1,NULL);
  XtAddCallback(okButt,XmNactivateCallback, removeShell, mshell);

  XtManageChild(form); 

  form = XtVaCreateWidget("form",xmFormWidgetClass,TestBase, XmNfractionBase, 7, NULL);
  XtVaSetValues(form,XtVaTypedArg,XmNbackground,XmRString,lightgray,strlen(lightgray)+1,NULL); 

  n = 0;
  XtSetArg(args[n],XmNscrollVertical,True); n++;
  XtSetArg(args[n],XmNscrollHorizontal,True); n++;
  XtSetArg(args[n],XmNeditMode,XmMULTI_LINE_EDIT); n++;
  XtSetArg(args[n],XmNeditable,False);n++;
  XtSetArg(args[n],XmNcursorPositionVisible,False); n++;
  XtSetArg(args[n],XmNwordWrap,False); n++;
  XtSetArg(args[n],XmNrows, 8); n++;

  logwin = XmCreateScrolledText(form,(String)"userlogwindow",args,n);
  XtVaSetValues(logwin,XtVaTypedArg,XmNforeground,XmRString,darkOrchid,strlen(darkOrchid)+1,
		XtVaTypedArg,XmNbackground,XmRString,actwhite,strlen(actwhite)+1,NULL);

  XtVaSetValues(XtParent(logwin),XmNleftAttachment, XmATTACH_FORM,XmNrightAttachment, XmATTACH_FORM,
		XmNtopAttachment, XmATTACH_FORM,NULL);
  /* procid = XtAppAddWorkProc(app,(XtWorkProc)updatewind,TestBase); */


  XtManageChild(logwin);
  XtManageChild(form); 
  XtManageChild(TestBase); 
  appendlog(runBase.currentLog->log,(char*)"\t\tCards list, as seen on the PCI bus:\n",1);
  for( iwid = 0; iwid < runBase.NCards; iwid++ ) {
    if( runBase.robinBoardsInfo[iwid].boardspec[1] > 0 ) 
      sprintf(tmpstr,"Card %d, serial %d (%s), initialisation OK\n",
	      iwid,runBase.robinBoardsInfo[iwid].boardspec[1],runBase.robinBoardsInfo[iwid].origin);
    else 
      sprintf(tmpstr,"Card %d, robin_firmware_update cannot read card serial number\n",iwid);
    appendlog(runBase.currentLog->log,tmpstr,1);
  }
  if( strlen(runBase.currentLog->errlog) > 1 ) 
    appendlog(runBase.currentLog->log,runBase.currentLog->errlog,1);

  call_data = NULL; /*unused */
}

static void button_set(Widget w, XtPointer client_data, XmAnyCallbackStruct *call_data)
{
  if( XmToggleButtonGetState(w) ) {
    XtVaSetValues(runBase.statButtons[(int)client_data],XtVaTypedArg,XmNbackground,XmRString,plum1,strlen(plum1)+1,
		  XtVaTypedArg,XmNforeground,XmRString,black,strlen(black)+1,
		  XtVaTypedArg,XmNlabelString, XmRString, "Active",strlen("Active")+1,NULL);
    runBase.select[(int)client_data] = 1;
  }
  else {
    XtVaSetValues(runBase.statButtons[(int)client_data],XtVaTypedArg,XmNbackground,
		  XmRString,actwhite,strlen(actwhite)+1,
		  XtVaTypedArg,XmNforeground,XmRString,black,strlen(black)+1,
		  XtVaTypedArg,XmNlabelString, XmRString, "Done",strlen("Done")+1,NULL);
    runBase.select[(int)client_data] = 0;  
  }
  XtSetSensitive(runBase.statButtons[(int)client_data],False);
  XtSetSensitive(runBase.errButtons[(int)client_data],False);
  call_data = NULL; /* not used */
}

static void activate_crd(Widget w, XtPointer client_data, XmAnyCallbackStruct *call_data)
{
  if( XmToggleButtonGetState(w) ) {
    runBase.cardSelect[(int)client_data] = 1;
    runBase.NCardsSel++;
    XtVaSetValues(runBase.CardstatButt[(int)client_data] ,XtVaTypedArg,XmNbackground, XmRString,actyell,
		  strlen(actyell)+1, NULL);
  }
  else {
    runBase.cardSelect[(int)client_data] = 0;
    runBase.NCardsSel--;
    XtVaSetValues(runBase.CardstatButt[(int)client_data] ,XtVaTypedArg,XmNbackground, XmRString,actwhite,
		  strlen(actwhite)+1, NULL);
  }
  call_data = NULL; /* not used */
}

static void runselected(Widget runSelected, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  unsigned int itest;

  call_data = NULL; /* unused */

  XtSetSensitive(runBase.runSelectedTests,False);
  if( runBase.NCardsSel > 0 ) {
    for( itest = 0; itest < runBase.nfields; itest++ ) {
      XtVaSetValues(runBase.statButtons[itest],XtVaTypedArg,XmNbackground, XmRString, actwhite,
		    strlen(actwhite)+1, NULL);
      XtSetSensitive(runBase.statButtons[itest], False);
      
      XtVaSetValues(runBase.errButtons[itest],XtVaTypedArg,XmNbackground, XmRString, gray87,strlen(gray87)+1,
		    XtVaTypedArg,XmNforeground, XmRString, actyell, strlen(actyell)+1,NULL);
      XtSetSensitive(runBase.errButtons[itest], False);
    }
    
    /* procid = XtAppAddWorkProc(app,(XtWorkProc)updatewind,NULL); */
    run_test(runSelected,(char**)client_data);
  }
  else {
    notice_win(runSelected,(char*)"No card(s) selected. Please, select at least one card.",(char*)"Do it to me one more time...");
    XtSetSensitive(runBase.runSelectedTests,True);
  }
}

void run_test(Widget rbutt, char** fieldLabels) {
  FILE *fdcon;
  unsigned int iwid,itest;
  int ncards,nactive;
  XSetWindowAttributes watch_conattr,normal_conattr;
  Display *dpy;
  short fubar,critical;
  FILE *fplog,*fprep;
  char fname[DefaultSize],repname[DefaultSize],tmpstr[DefaultSize];
  char scmd[DefaultSize],prefix[DefaultSize],crdnum[DefaultSize];
  char *ptmpstr;
  time_t ltime;
  short breakcond;

  time(&ltime); ctime_r(&ltime,prefix);
  ptmpstr = prefix; 
  while( *ptmpstr != '\0' ) {
    if( *ptmpstr == ' ' || *ptmpstr == '\n' || *ptmpstr == ':' ) 
      *ptmpstr = '_';
    ptmpstr++;
  }
    
  sprintf(fname,"robintest%s.log",prefix);
  if( !(fplog = fopen(fname, "w")) ) {
    notice_win(rbutt,(char*)"Critical error: cannot create log files, program will not run",(char*)"FUBAR");
    return;
  }

  sprintf(repname,"robintestReport%s.log",prefix);
  if( !(fprep = fopen(repname, "w")) ) {
    notice_win(rbutt,(char*)"Critical error: cannot create report, program will not run",(char*)"FUBAR");
    return;
  }
  
  strcpy(tmpstr,ctime(&ltime));
  fwrite(tmpstr,sizeof(char),strlen(tmpstr),fplog);
  fwrite(tmpstr,sizeof(char),strlen(tmpstr),fprep);

  /* get display, create cursors */
  dpy = XtDisplay(runBase.shell);
  normal_conattr.cursor = None;
  watch_conattr.cursor = XCreateFontCursor(dpy, XC_watch);

  for( iwid = 0; iwid < runBase.nfields; iwid++ ) {
    if( runBase.currentLog->testlog[iwid] != NULL ) {
      free(runBase.currentLog->testlog[iwid]);
      runBase.currentLog->testlog[iwid] = NULL;
    }
    
    if( runBase.currentLog->testerrlog[iwid] != NULL ) {
      free(runBase.currentLog->testerrlog[iwid]);
      runBase.currentLog->testerrlog[iwid] = NULL;
    }
  }
  /* reset text window to zero position, zero buffer size */
  appendlog(runBase.currentLog->log,(char*)"text reset",1);
  
  /* disable cards which failed during the prior run */
  for( iwid = 0; iwid < (unsigned int)runBase.NCards; iwid++ )
     if( runBase.cardSelect[iwid] == -1 )
       runBase.cardSelect[iwid] = 0;

  if( runBase.Dolar ) 
    if( dolar_scripts(runBase.currentLog->log,runBase.currentLog->errlog) != 0 ) {
      appendlog(runBase.currentLog->errlog,(char*)"\nError: cannot create dolar scripts\n",1);
      return;
    }

  critical = 0; breakcond = 0;
  ncards = runBase.NCards;
  for( iwid = 0; iwid < runBase.nfields; iwid++ ) { 
    runBase.currentLog->log[0] = runBase.currentLog->errlog[0] = '\0';
    if( runBase.select[iwid] ) {
      resetstatus();
      XChangeWindowAttributes(dpy, XtWindow(runBase.shell), CWCursor, &watch_conattr);
      XmUpdateDisplay(runBase.shell);
      fubar = 0; ncards = runBase.NCards;

      /* check number of available cards */
      if( strstr(fieldLabels[iwid],"Available cards:") != NULL )
	if( get_cards(rbutt,runBase.currentLog,1) < 1 ) 
	  fubar = 1; 

      /*EEPROM write */
      if( strstr(fieldLabels[iwid],"EPROM write:") != NULL ) 
	fubar = card_write((char*)"write",ncards,runBase.currentLog);
      
      /*EEPROM read */
      if( strstr(fieldLabels[iwid],"EPROM read:") != NULL ) 
	fubar = card_write((char*)"read",ncards,runBase.currentLog);
      
      /* u-boot write */
      if( strstr(fieldLabels[iwid],"u-boot write:") != NULL ) 
	fubar = loadppcpci(ncards,runBase.currentLog->log,runBase.currentLog->errlog,runBase.cardSelect,(char*)"uboot");

      /* environment write */
      if( strstr(fieldLabels[iwid],"environment write:") != NULL ) 
	fubar = loadppcpci(ncards,runBase.currentLog->log,runBase.currentLog->errlog,runBase.cardSelect,(char*)"env");

      /* load PPC */
      if( strstr(fieldLabels[iwid],"PPC write to FLASH:") != NULL )
	fubar = loadppcpci(ncards,runBase.currentLog->log,runBase.currentLog->errlog,runBase.cardSelect,(char*)"ppc");

      if( strstr(fieldLabels[iwid],"FPGA write to FLASH:") != NULL )
	fubar = loadppcpci(ncards,runBase.currentLog->log,runBase.currentLog->errlog,runBase.cardSelect,(char*)"fpga");

      /* load FPGA */
      if( strstr(fieldLabels[iwid],"Configure FPGA:") != NULL ) {
	fubar = load_fpga(ncards,runBase.currentLog);
      }
      
      /* open tty */
      if( strstr(fieldLabels[iwid],"Open tty:") != NULL ) {
	fubar = 0;
	/*if( runBase.robintty )
	  fubar = driverUseCheck(runBase.currentLog->log,runBase.currentLog->errlog);*/
	if( !fubar ) {
	  if( runBase.ttyfd != NULL ) {
	    for( itest = 0; itest < (unsigned int)ncards; itest++ ) 
	      if( runBase.ttyfd[itest] != 0 )
		close(runBase.ttyfd[itest]);
	    free(runBase.ttyfd);
	    runBase.ttyfd = NULL;
	  }
	  fubar = findactivetty(ncards,NSERTTY,&runBase.ttyfd,runBase.currentLog->log,runBase.currentLog->errlog,
				runBase.robinBoardsInfo,runBase.robintty,runBase.cardSelect);
	}
	else {
	  appendlog(runBase.currentLog->log,(char*)"ERROR: ttydriver in use, cannot continue ",1);
	  appendlog(runBase.currentLog->errlog,(char*)"ERROR: ttydriver in use, cannot continue ",1);
	}
      }
      
      /* run BIST */
      if( strstr(fieldLabels[iwid],"Run eBIST:") != NULL ) {
	fubar = runBist(ncards,runBase.currentLog,runBase.ttyfd,runBase.autorun,fieldLabels);
	if( runBase.autorun )
	  breakcond = 1;
      }
      
      /* run net requester */
      if( strstr(fieldLabels[iwid],"Net Requester:") != NULL ) { 
	if( runBase.ttyfd == NULL )
	  fubar = findactivetty(ncards,NSERTTY,&runBase.ttyfd,runBase.currentLog->log,runBase.currentLog->errlog,
				runBase.robinBoardsInfo,runBase.robintty,runBase.cardSelect);
	if( !fubar )
	  fubar = run_netreq(ncards,runBase.currentLog,runBase.ttyfd);	
      }
      
      /* run PCI requester */
      if( strstr(fieldLabels[iwid],"PCI Requester:") != NULL ) {
	if( runBase.ttyfd == NULL )
	  fubar = findactivetty(ncards,NSERTTY,&runBase.ttyfd,runBase.currentLog->log,
				runBase.currentLog->errlog,runBase.robinBoardsInfo,runBase.robintty,runBase.cardSelect); 
	if( !fubar )
	  fubar = run_pcireq(ncards,runBase.currentLog,runBase.ttyfd);
      }
    
      if( strstr(fieldLabels[iwid],"Restart PPC") != NULL ) {
	if( runBase.ttyfd == NULL )
	  fubar = findactivetty(ncards,NSERTTY,&runBase.ttyfd,runBase.currentLog->log,
				runBase.currentLog->errlog,runBase.robinBoardsInfo,runBase.robintty,runBase.cardSelect); 
	if( !fubar )
	  fubar = restartPPC(ncards,runBase.ttyfd,runBase.currentLog);
      }

      XChangeWindowAttributes(dpy, XtWindow(runBase.shell), CWCursor, &normal_conattr);
      /* disarm toggle button, mark test not active */
      if( runBase.TButt[iwid] != NULL ) /*disable the test */
	XmToggleButtonSetValue(runBase.TButt[iwid],False,False);

      if( !runBase.autorun ) /*in "auto" mode test stays selected */
	runBase.select[iwid] = 0;

      XFlush(dpy);
      XmUpdateDisplay(runBase.shell);
      if( fubar == 0 ) {
	if( (strstr(runBase.currentLog->log,"Warning") != NULL || strstr(runBase.currentLog->errlog,"Warning") != NULL) )
	  passedwarrnings(runBase.statButtons[iwid]);
	else
	  passed(runBase.statButtons[iwid]);
      }
      else {
	failed(iwid,&runBase);
	if( (nactive = check_active()) != 0 || critical ) {
	  appendlog(runBase.currentLog->log,(char*)"Test stopped ",1);
	  appendlog(runBase.currentLog->log,(char*)"no tty open",1);

	  for( itest = iwid; itest < runBase.nfields; itest++ ) {
	    if( runBase.TButt[itest] != NULL && runBase.select[itest] != 0 ) {
	      runBase.select[itest] = 0;
	      XmToggleButtonSetValue(runBase.TButt[itest],False,False);
	    }
	  } 
	  XFlush(dpy);
	  XmUpdateDisplay(runBase.shell);

	  appendlog(runBase.currentLog->log,(char*)"\nno more error-free cards or critical error\n",1);
	  testEnd(fplog,fprep,runBase.currentLog,fieldLabels[iwid],iwid); 
	  break;
	}       
      }
    }
    testEnd(fplog,fprep,runBase.currentLog,fieldLabels[iwid],iwid);
    if( breakcond )
      break;
  }
  XtSetSensitive(runBase.runSelectedTests,True);

  fclose(fplog); fclose(fprep);
  sprintf(tmpstr,"%scards_",prefix);
  for( iwid = 0; iwid < (unsigned int)runBase.NCards; iwid++ ) {
    if( runBase.cardSelect[iwid] == 1 || runBase.cardSelect[iwid] == -1 ) {
      sprintf(crdnum,"%d_",runBase.robinBoardsInfo[iwid].boardspec[1]);
      if( (DefaultSize - (strlen(tmpstr) + strlen(crdnum))) > 0 )
	strcat(tmpstr,crdnum);
    }
  }
  sprintf(scmd,"mv %s robintest%s.log",fname,tmpstr);
  system(scmd);
  sprintf(fname,"robintest%s.log",tmpstr);
  sprintf(scmd,"mv %s robintestReport%s.log",repname,tmpstr);
  system(scmd);
  sprintf(repname,"robintest%s.log",tmpstr);

  sprintf(tmpstr,"Log files saved to %s\nReport saved to %s",fname,repname);
  notice_win(rbutt,tmpstr,(char*)"End of program");  
  
  if( (fdcon = fopen(confname,"w")) != NULL ) {
    fprintf(fdcon,"%hd %hd %hd %hd %hd",runBase.Dolar,runBase.robintty,runBase.nettran,
	    runBase.extLoop,runBase.ethdev);
    fclose(fdcon);
  }
}

static void process_log(Widget statbutt, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  call_data = NULL; /* unused */
  notice_win(statbutt,(char*)(*(char**)client_data),(char*)"LOG");
}

static void process_errlog(Widget statbutt, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  call_data = NULL; /* unused */
  notice_win(statbutt,(char*)(*(char**)client_data),(char*)"ERROR LOG");
}

/*static void stoppushed(Widget stopbutt, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  if( procid > 0 ) { 
    XtRemoveWorkProc(procid);
    procid = -1;
  }
  }*/

static void ppccmd(Widget w, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  Widget PPCcmd,WorkWid;
  Arg args[2];
  int n;
  XmString cmd; 

  cmd = XmStringCreateLocalized((char*)"Enter Robin command:");
  n = 0;
  XtSetArg(args[n],XmNselectionLabelString,cmd); n++;
  XtSetArg(args[n],XmNautoUnmanage, False); n++;
  PPCcmd = XmCreatePromptDialog(w,(String)"ppcmd",args, n);
  XmStringFree(cmd);
  XtVaSetValues(PPCcmd,XtVaTypedArg,XmNbackground, XmRString,"medium purple",strlen("medium purple")+1,NULL);

  WorkWid = XmSelectionBoxGetChild(PPCcmd,XmDIALOG_TEXT);
  XtVaSetValues(WorkWid,XtVaTypedArg,XmNbackground, XmRString, LightBlue,strlen(LightBlue)+1 ,NULL);

  XtSetSensitive(XmSelectionBoxGetChild(PPCcmd,XmDIALOG_HELP_BUTTON), False);

  XtAddCallback(PPCcmd,XmNokCallback,get_ppccmd,NULL);
  XtAddCallback(PPCcmd,XmNcancelCallback,(XtCallbackProc)XtDestroyWidget,NULL); 
  
  XtManageChild(PPCcmd);
  XtPopup(XtParent(PPCcmd),XtGrabNone);
  call_data = NULL; /* unused */
  client_data = NULL; /* unused */
}

static void get_ppccmd(Widget w, XtPointer client_data,XmSelectionBoxCallbackStruct *call_data) {
  cbdata localLog;
  char *ppcmd;
  int icrd;

  if( runBase.ttyfd == NULL ) {
    notice_win(w,(char*)"\nNo tty dev opened\nRun config, select \"Open tty\", press RUN",(char*)"ERROR");
    return;
  }

  XmStringGetLtoR(call_data->value,XmSTRING_DEFAULT_CHARSET, &ppcmd);

  for( icrd = 0; icrd < runBase.NCards; icrd++ ) {
    if( runBase.cardSelect[icrd] == 1 ) {
      stopPpc(runBase.ttyfd+icrd,&localLog);
      run_ppccmd(1,&localLog,&(runBase.ttyfd[icrd]),ppcmd);
    }
  }
  notice_win(w,localLog.log,(char*)"LOG");
  XtFree(ppcmd);
  call_data = NULL; /* unused */
  client_data = NULL; /* unused */
}

static void qpushed(Widget w, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  int icrd;

  if( runBase.ttyfd != NULL ) {
    for( icrd = 0; icrd < runBase.NCards; icrd++ ) {
      close(runBase.ttyfd[icrd]); 
    }
    free(runBase.ttyfd);
  }
  free(runBase.currentLog);
  XtDestroyWidget(w);

  client_data = NULL; /* unused */
  call_data = NULL; /* unused */
  exit(0);
}

static void sens_help(Widget w, XtPointer client_data,XtPointer call_data) {
  Cursor cursor;
  Display *display;
  Widget help_widget;
  XmAnyCallbackStruct *cbs,*newcbs;
  XEvent event;

  cbs = (XmAnyCallbackStruct*)call_data;
  newcbs = cbs;
  display = XtDisplay(w);
  cursor = XCreateFontCursor(display, XC_question_arrow);

  help_widget = XmTrackingEvent(w,cursor,False,&event);

  while( help_widget != NULL ) {
    if( XtHasCallbacks(help_widget,XmNhelpCallback) == XtCallbackHasSome ) {
      newcbs->reason = XmCR_HELP;
      newcbs->event = &event;
      XtCallCallbacks(help_widget, XmNhelpCallback,(XtPointer)newcbs);
    }
    help_widget = NULL;
  }
  XFreeCursor(display,cursor);
  client_data = NULL; /* unused */
}

static void ts_help(Widget w, XtPointer client_data,XtPointer call_data) {
  const char *label = "Help";
  char *help_items;

  help_items = (char*)client_data;

  if( help_items == NULL || strlen(help_items) < 1 ) 
    info_win(w,(char*)"Help not available for this item\n",(char*)label);
  else
    info_win(w,help_items,(char*)label);
  call_data = NULL; /* unused */
}

int get_cards(Widget rbutt,cbdata* cardPciLog, short rscope) {  
  char cmd[DefaultSize];
  char loclog[MAX_BYTE];
  char *pbuf,*ptmp,*logpos;
  const char *robin_design[] = {"PLX Technology","CERN/ECP/EDU: Unknown device 0324",
				"CERN/ECP/EDU: Unknown device 0144"};
  const char *dolar_design = "CERN/ECP/EDU: Unknown device 0018";
  const char *labstring[] = {"swVersion","designVersion","Robin temperature"};
  const char *scancmd = "robin_firmware_update --mode=scan";
  char *argstline[Max_NumItems];
  unsigned int ides;
  int found,scanfound,iline,largs,icrd,iver;

  strcpy(cmd,"/sbin/lspci");
  if( executedup(cmd,cardPciLog->log,cardPciLog->errlog) < 0 )
    perror("execute");

  found = 0;
  for( ides = 0; ides < (sizeof(robin_design)/sizeof(robin_design[0])); ides++ ) {
    pbuf = cardPciLog->log;
    while( (pbuf = strstr(pbuf,robin_design[ides])) != NULL ) {
      pbuf++;
      found++;
    }
  }
  if( found > 0 ) 
    appendlog(cardPciLog->log,cmd,1);
  else {
    appendlog(cardPciLog->errlog,(char*)"No Robin cards on this system !",1);
    return -1;
  }

  if( runBase.robinBoardsInfo == NULL ) {
    if( (runBase.robinBoardsInfo =  calloc(found,sizeof(robinInfo_t))) == NULL ) {
      appendlog(cardPciLog->errlog,(char*)"ERROR: robinBoardsInfo malloc failed",1);
      return -1;
    }
  }

  /* go through log again, look for the bus:slot */
  if( (logpos = malloc((strlen(cardPciLog->log)+1)*sizeof(char))) == NULL ) {
    appendlog(cardPciLog->errlog,(char*)"ERROR: get_cards: Cannot allocate temporary buffer",1);
    return -1;
  }
  sprintf(logpos,"%s",cardPciLog->log);
  
  largs = splitext(logpos,argstline); icrd = 0;
  for( iline = 0; iline < largs; iline++ ) {
    for( ides = 0; ides < (sizeof(robin_design)/sizeof(robin_design[0])); ides++ ) { 
      if( (pbuf = strstr(argstline[iline],robin_design[ides])) != NULL ) {
	if( (pbuf = index(argstline[iline],'.'))  != NULL ) {
	  strncpy(runBase.robinBoardsInfo[icrd].bus_slot,argstline[iline],(pbuf-argstline[iline]));
	  runBase.robinBoardsInfo[icrd].bus_slot[(pbuf-argstline[iline])] = '\0';
	}
	else
	  sprintf(runBase.robinBoardsInfo[icrd].bus_slot,"bus and slot unknown");
	icrd++;
      }
    }
  }
  free(logpos);
  
  logpos = cardPciLog->errlog + strlen(cardPciLog->errlog);
  if( executedup((char*)scancmd,cardPciLog->errlog,cardPciLog->errlog) < 0 ) 
    appendlog(cardPciLog->errlog,(char*)"ERROR: robin_firmware_update --mode=scan failed\n",1);
  else 
    scanfound = getDeviceInfo(logpos,cardPciLog);

  for( iline = 0; iline < (int)(sizeof(runtimeerr)/sizeof(runtimeerr[0])); iline++ )  {
    if( strstr(logpos,runtimeerr[iline]) != NULL ) {
      notice_win(rbutt,(char*)systemerr[iline],(char*)"WARNING");
      appendlog(cardPciLog->errlog,(char*)systemerr[iline],1);
    }
  }
  
  /* check number of Dolar cards */
  pbuf = cardPciLog->log;
  runBase.Ndolar = 0;
  while( (pbuf = strstr(pbuf,dolar_design)) != NULL ) {
    runBase.Ndolar++;
    pbuf++;
  }

  if( rscope ) {
    if( initRobinMsg(found,cardPciLog->log,cardPciLog->errlog,runBase.cardSelect) ) {
      appendlog(cardPciLog->log,(char*)"ERROR:  initRobinMsg (robinconfig) failed\n",1);
      appendlog(cardPciLog->errlog,(char*)"ERROR:initRobinMsg (robinconfig) failed, see main logfile for details\n",1);
      return -1;
    }
    for( icrd = 0; icrd < found; icrd++ ) {
      runBase.robinBoardsInfo[icrd].boardspec[6] = 0;
      sprintf(cmd,"robinscope -m %d -s",icrd);
      loclog[0] = '\0';
      if( executedup(cmd,loclog,loclog) < 0 )
	perror("execute");
      
      sprintf(cmd,"Card %d:\n",runBase.robinBoardsInfo[icrd].boardspec[1]);
      appendlog(cardPciLog->log,cmd,1);
      iver = 4; 
      for( largs = 0; largs < (int)(sizeof(labstring)/sizeof(labstring[0])); largs++ ) {
	if( (logpos = strstr(loclog,labstring[largs])) != NULL ) {
	  if( (pbuf = strstr(logpos,"\n")) != NULL )
	    *pbuf = '\0';
	  sprintf(cmd,"%s\n",logpos);
	  appendlog(cardPciLog->log,cmd,1);

	  if( (ptmp = index(cmd,' ')) != NULL )
	    sscanf(ptmp,"%x",&(runBase.robinBoardsInfo[icrd].boardspec[iver++]));
	  if( pbuf != NULL ) 
	    *pbuf = '\n';
	}
	else {
	  sprintf(cmd,"Card %d: Cannot read card info. Possible resons: 1) PPC not running: Restart PPC application." \
		  "\n2) Message Passing might not work properly: mark card as faulty, inform the experst\n",icrd);
	  appendlog(cardPciLog->errlog,cmd,1);
	  runBase.robinBoardsInfo[icrd].boardspec[6] = 1;	  
	  found = -1;
	  break;
	}
      }
      setstatus(icrd,runBase.robinBoardsInfo[icrd].boardspec[6],NULL);
    }
    if( chckpci(cardPciLog->errlog,found,runBase.robinBoardsInfo) < 0 )
      return -1;
  }

  /* return scanfound; */
  return found; 
}

int card_write(char *mode,int ncards,cbdata* cardPciLog) {
  int device_nr;
  char *scanlogpos,*logpos;
  char tmpbuf[DefaultSize],tmpstr[DefaultSize];
  char cmd[DefaultSize],promFileName[DefaultSize];
  const char *base_cmd = {"robin_firmware_update --mode=eeprom"};
  const char *scancmd = "robin_firmware_update --mode=scan";
  const char *lookfor[] = {"DeviceId:               144","VendorId:               10dc","ClassCode:              b4000ac"};
  int currentSerial;
  char *argst[Max_NumItems];
  int iargs;
  short crd_err;
  
  iargs = 0;
  if( ncards == 0 ) {
    appendlog(cardPciLog->errlog,(char*)"\nError: unknown number of cards, \ntry 'available cards' option first",1);
    return -1;
  }
  /*if( strstr(mode,"write") != NULL && asm_ser[1] == NULL ) {
    appendlog(cardPciLog->errlog,"Assembly id not set. Close the tests window and press RUN button again.",1);
    return -1;
    }*/

  scanlogpos = cardPciLog->log + strlen(cardPciLog->log)*sizeof(char);
  strcpy(cmd,"robin_firmware_update --mode=scan");
  if( executedup(cmd,cardPciLog->log,cardPciLog->errlog) < 0 )
    perror("execute");
  else
    getDeviceInfo(scanlogpos,cardPciLog);

  /*if( strstr(mode,"write") != NULL )
    iargs = splitarg(asm_ser[1],argst);*/

  cmd[0] = '\0';
  for( device_nr = 0; device_nr < ncards; device_nr++ ) {
    if( runBase.cardSelect[device_nr] == 1 ) {
      logpos = cardPciLog->log + strlen(cardPciLog->log)*sizeof(char);
      sprintf(tmpbuf,"\n\n----- Processing CARD %d board %s %s serial num %d bus:slot %s --------\n",
	      (device_nr+1),runBase.robinBoardsInfo[device_nr].boardType,runBase.robinBoardsInfo[device_nr].origin,
	      runBase.robinBoardsInfo[device_nr].boardspec[1],
	      runBase.robinBoardsInfo[device_nr].bus_slot);
      appendlog(cardPciLog->log,tmpbuf,1);
      crd_err = 0;

      if( strstr(mode,"write") != NULL ) { /* create new eeprom file */
	if( device_nr < iargs )
	  sscanf(argst[device_nr],"%d",&currentSerial);
	else {
	  appendlog(cardPciLog->errlog,(char*)"ERROR: too few serial numbers\n",1);
	  return -1;
	}
	  
	if( create_eeprom(p_prefix,currentSerial,promFileName) == -1 ) {
	  appendlog(cardPciLog->log,(char*)"Creating EEPOROM file failed\n",1);
	  appendlog(cardPciLog->errlog,(char*)"Creating EEPOROM file failed\n",1);
	  crd_err = 1;
	}
	sprintf(cmd,"%s %d %s %s",base_cmd,device_nr,mode,promFileName);
      }
      else
	if( strstr(mode,"read") != NULL ) 
	  sprintf(cmd,"%s %d %s eepromdump.etx",base_cmd,device_nr,mode);
      
      if( strlen(cmd) > 0 ) {
	if( executedup(cmd,cardPciLog->log,cardPciLog->errlog) < 0 ) 
	  appendlog(cardPciLog->errlog,(char*)"execution error",1);
	
	if( checkerr(logpos,cardPciLog->errlog,eprom_flasherr,(sizeof(eprom_flasherr)/sizeof(eprom_flasherr[0])),tmpbuf) )
	  crd_err = 1;
	if( strstr(mode,"read") != NULL ) {
	  sprintf(cmd,"cat eepromdump.etx");
	  executedup(cmd,cardPciLog->log,cardPciLog->errlog);
	  for( iargs = 0; iargs < (int)(sizeof(lookfor)/sizeof(lookfor[0])); iargs++ ) {
	    if( strstr(cardPciLog->log,lookfor[iargs]) == NULL ) {
	      sprintf(cmd,"Missing %s, PLX Eeprom info might be corrupted, card should be checked by manufacturer\n",
		      lookfor[iargs]);
	      appendlog(cardPciLog->errlog,cmd,1);
	      crd_err = 1;
	      break;
	    }
	      
	  }
	}
      }

      /* if PLX eepromwrite: */
      if( strstr(mode,"write") != NULL  && !crd_err ) {
	/* after writing to prom, scan the bus */
	scanlogpos = cardPciLog->log + strlen(cardPciLog->log)*sizeof(char);
	strcpy(cmd,scancmd);
	/* fprintf(stdout,"\ndevice: %d\n",device_nr); fflush(stdout);  */
	if( executedup(cmd,cardPciLog->log,cardPciLog->errlog) < 0 )
	  perror("execute");
	else {
	  getDeviceInfo(scanlogpos,cardPciLog);
	  /* update card info on the x-display */
	  sprintf(tmpstr,"%3d",runBase.robinBoardsInfo[device_nr].boardspec[1]);
	  XtVaSetValues(runBase.CardstatButt[device_nr],XtVaTypedArg, 
			XmNlabelString,XmRString,tmpstr,strlen(tmpstr)+1,NULL);
	}
      }
      setstatus(device_nr,crd_err,cardPciLog->errlog);
    }
  }

  if( strlen(cardPciLog->errlog) > 0 )
    return -1;

  return 0;
}

int create_eeprom(char *prefix,int sernum,char *promFileName) {
  char *file_text,*buf_pos;
  const char *base_file[] = {"eeprom_core.etx","current_eeprom"};
  char tmpbuf[DefaultSize];
  FILE *fp,*fpeeprom;
  struct stat locstat;

  if( (fp = fopen(base_file[0],"r")) == NULL ) {
    fprintf(stderr, "Cannot open %s\n",base_file[0]);
    return -1;
  }
  stat(base_file[0],&locstat);

  if( (file_text = malloc(locstat.st_size)) == NULL ) {
    perror("malloc file_tex");
    return -1;
  }
  fread(file_text,sizeof(char),locstat.st_size,fp);
  fclose(fp);

  sprintf(promFileName,"./prom/%s%06d.etx",base_file[1],sernum);
  if( (fpeeprom = fopen(promFileName,"w")) == NULL ) {
    fprintf(stderr, "Cannot open %s\n",base_file[1]);
    return -1;
  }

  buf_pos = strstr(file_text,"VPDBoardSerial");
  fwrite(file_text,sizeof(char),((buf_pos-file_text)/sizeof(char)),fpeeprom);
  sprintf(tmpbuf,"VPDBoardSerial:         %s%06d\n",prefix,sernum);
  fwrite(tmpbuf,sizeof(char),strlen(tmpbuf),fpeeprom);
  buf_pos = strstr(file_text,"VPDBoardRev");
  fwrite(buf_pos,sizeof(char),strlen(file_text)-((buf_pos-file_text)/sizeof(char)),fpeeprom);

  fclose(fpeeprom);
  free(file_text);

  return 0;
}

int run_ppccmd(int ncards,cbdata* cardPciLog,int *fdtty, char *PPCcmd) {
  int icrd,statapp,cmdDone,nrepeat;
  char *logpos;

  if( fdtty == NULL ) {
    appendlog(cardPciLog->log,(char*)"\nERROR: No tty dev opened, select \"Open tty\" and try again\n",1);
    return -1;
  }

  nrepeat = cmdDone = statapp = 0;
  for( icrd = 0; icrd < ncards; icrd++ ) {
    while( !cmdDone && nrepeat++ < 5 ) { 
      logpos = cardPciLog->log + strlen(cardPciLog->log);

      /* log file for the test begins here */
      runppc_select(fdtty[icrd],PPCcmd,cardPciLog->log);
      if( (strstr(logpos,"Application terminated") != NULL && strstr(logpos,"FATAL ERROR") != NULL) ||
	   strstr(logpos,"Bus Fault") != NULL ) {
	appendlog(cardPciLog->errlog,(char*)"\nApplication terminated, FATAL ERROR\nSee main log for details\n",1);
	statapp = 1;
	cmdDone = 1;
	break;
      }

      if( strstr(logpos,"Unknown command") != NULL ) /*recover from the unknown command */
	runppc_select(fdtty[icrd],(char*)"\r",cardPciLog->log);
      else
	cmdDone = 1;
    }
  }

  return statapp;
}

int loadppcpci(int ncards,char *logapp,char *errlog,short *cardselect,char *ppc_or_fpga) {
  unsigned int imod;
  int icard,crd_err,status;
  char tmpbuf[DefaultSize],cmd[DefaultSize],*logpos;
  const char *cmd_core = "robin_firmware_update --mode=ppcflash";
  const char *modes[] = {"ppc","fpga","uboot","env"};
  const char *flash_write[] = {"ppc write","fpga write","uboot write u-boot.bin","env write robinenv.env"};

  status = 0;
  for( icard = 0; icard < ncards; icard++ ) {
    if( cardselect[icard] == 1 ) {
      logpos = logapp + strlen(logapp);
      sprintf(tmpbuf,"\n\n----- Processing CARD %d %s, serial %d bus:slot %s --------\n",
	      (icard+1),runBase.robinBoardsInfo[icard].origin,runBase.robinBoardsInfo[icard].boardspec[1],
	      runBase.robinBoardsInfo[icard].bus_slot);
      appendlog(logapp,tmpbuf,1);
      logpos = logapp + strlen(logapp);
      crd_err = 0;

      if( !SpecialConf[icard] ) {
	sprintf(cmd,"robin_fpga_load %d %s",
		runBase.robinBoardsInfo[icard].boardspec[0],fpga_elf[2]);
	if( executedup(cmd,logapp,errlog) < 0 )      
	  perror("execute load ppc configure"); 
      }
      if( checkerr(logpos,errlog,eprom_flasherr,(sizeof(fpgaloaderr)/sizeof(fpgaloaderr[0])),tmpbuf) )
	status = crd_err = 1;
      else {
	SpecialConf[icard] = 1;
	cmd[0] = '\0';
	for( imod = 0; imod < (sizeof(modes)/sizeof(modes[0])); imod++ ) 
	  if( !strcmp(ppc_or_fpga,modes[imod]) ) {
	    if( strstr(modes[imod],"uboot") != NULL )
	      sprintf(cmd,"%s %d %s",
		      cmd_core,runBase.robinBoardsInfo[icard].boardspec[0],flash_write[imod]);
	    else
	      sprintf(cmd,"%s %d %s %s",
		      cmd_core,runBase.robinBoardsInfo[icard].boardspec[0],flash_write[imod],fpga_elf[imod]);
	    break;
	  }
	
	if( strlen(cmd) == 0 ) {
	  appendlog(logapp,(char*)"ERROR: loadppcpci - no valid mode found\n",1);
	  appendlog(errlog,(char*)"ERROR: loadppcpci - no valid mode found\n",0);
	  status = crd_err = 1;
	}
	else {
	  if( strstr(flash_write[imod],"env") != NULL ) 
	    status = crd_err = robinsetenv(logapp,errlog,icard);
	  if( !crd_err ) {
	    if( executedup(cmd,logapp,errlog) < 0 || 
		checkerr(logpos,errlog,eprom_flasherr,(sizeof(eprom_flasherr)/sizeof(eprom_flasherr[0])),tmpbuf) ) 
	      status = crd_err = 1; 
	  }
	}
      }	
      setstatus(icard,crd_err,errlog);
    }
  }

  return status;
}

int robinsetenv(char *logapp,char *errlog,int idev) {
  FILE *fenv;
  struct stat dirstat;
  char cmd[DefaultSize];
  const char *defaultenv = "env_core.env";
  const char *envfile = "robinenv.env"; /*should be the same as in the loadppcpci function */
  const char *setrenv = "BaseIPAddress 0\nSubDetectorId 1\nPagesize 512\nNumpages 32768\nHashbits 0x10\nNetworkEnabled 1\n" \
    "DebugEnabled 0\nDumpRolEnabled 0\nInteractive 1\nKeepctlwords 0\nDpmcache 1\nMacflowctl 1\nRolEnabled 0\n" \
    "MaxRxPages 16\nEbistEnabled 0\nContinbist 0\nIgnorebist 0\nMgmtcache 1\nDmafifocheck 1\nUpfdma 0\nHdrdma 0\n" \
    "Prescalefrag 3\nPrescalemsg 1\nPrescalefpf 10\nUDPBasePort 9000\nSecsiemu 0\nMax1618check 1\n" \
    "SkipReload 0\nTempAlarm 103\nbootcmd bootelf 0xff800000\nboard robin2";

  if( stat(defaultenv,&dirstat) == 0 ) {
    sprintf(cmd,"cp %s %s\n",defaultenv,envfile);
    executedup(cmd,logapp,errlog);
  }

  if( !(fenv = fopen(envfile, "w")) ) {
    appendlog(logapp,(char*)"ERROR: robinsetenv - cannot open robinenv.env file\n",1);
    appendlog(errlog,(char*)"ERROR: robinsetenv - cannot open robinenv.env file\n",0);
    return 1;
  }

  if( stat(defaultenv,&dirstat) != 0 ) 
    fprintf(fenv,"%s\nsernum %d\n",setrenv,runBase.robinBoardsInfo[idev].boardspec[1]);
  else
    fprintf(fenv,"sernum %d\n",runBase.robinBoardsInfo[idev].boardspec[1]);
  fclose(fenv);

  return 0;
}

int runBist(int ncards,cbdata *currentLog,int *ttyfd,short int ebistauto,char **fieldLabels) {
  unsigned int icmd;
  int statapp,icard;
  int crd_err;
  short brkapp;
  char tmpbuf[DefaultSize],*logpos,*cardlogpos;
  const char *extloopback = "setenv Rolextloop 1";
  const char *bcmd[] = {"setenv SkipReload 1","setenv EbistEnabled 1","printenv","bootelf 0xff800000","0","0"};
  const char *post_act[] = {"setenv SkipReload 0","setenv bootcmd bootelf 0xff800000","setenv EbistEnabled 0","setenv Rolextloop 0","printenv"};
  int testBuilddate;
  
  if( ncards == 0 ) {
    appendlog(currentLog->errlog,(char*)"\nError: unknown number of cards, \ntry 'available cards' option first",1);
    return -1;
  }

  if( ttyfd == NULL ) {
    appendlog(currentLog->errlog,(char*)"\ntty not opened",1);
    return -1;
  }
  
  statapp = 0; 
  for( icard = 0; icard < ncards; icard++ ) {
    if( runBase.cardSelect[icard] == 1 ) {
      /* opentty(&ttyfd[icard],runBase.robinBoardsInfo[icard].ttyused); */
      if( *(ttyfd+icard) > 0 ) { 
	sprintf(tmpbuf,"\n\n----- Processing CARD %d %s, serial %d bus:slot %s --------\n",
		(icard+1),runBase.robinBoardsInfo[icard].origin,runBase.robinBoardsInfo[icard].boardspec[1],
		runBase.robinBoardsInfo[icard].bus_slot);
	appendlog(currentLog->log,tmpbuf,1);
	cardlogpos = currentLog->log + strlen(currentLog->log);
	crd_err = 0; brkapp = 0;

	stopPpc((ttyfd+icard),currentLog);

	if( !runBase.extLoop ) 
	  statapp-= run_ppccmd(1,currentLog,(ttyfd+icard),(char*)extloopback);

	logpos = currentLog->log + strlen(currentLog->log);
	for( icmd = 0; icmd < XtNumber(bcmd); icmd++ ) {
	  statapp-= run_ppccmd(1,currentLog,(ttyfd+icard),(char*)bcmd[icmd]);

	  if( strstr(logpos,"Bus Fault") != NULL || strstr(logpos,"Application terminated") != NULL  )
	    break;
	}
	if( statapp == 0 ) {
	  for( icmd = 0; icmd < XtNumber(post_act); icmd++ ) 
	    run_ppccmd(1,currentLog,(ttyfd+icard),(char*)post_act[icmd]);
	}

	if( ebistErrCheck(currentLog,logpos) != 0 ) {
	  statapp--;
	  crd_err = 1;
	}
	  
	strcat(tmpbuf,"Warning(s):\n");
	if( checkerr(cardlogpos,currentLog->errlog,ppwar,(sizeof(ppwar)/sizeof(ppwar[0])),tmpbuf) ) 
	  statapp-=crd_err = 1;
	if( ebistauto ) {
	  if( (testBuilddate = checkBuildDate(cardlogpos) - build0 ) < 0 )
	    oldstyle_ebist(currentLog,cardlogpos,fieldLabels);
	  else
	    newstyle_ebist(currentLog,cardlogpos,fieldLabels);
	  /*analyse_ebist(currentLog,cardlogpos,fieldLabels);*/
	}
      }
      else {
	sprintf(tmpbuf,"Error: File descriptor invalid, check tty connection to the card serial %d\n",
		runBase.robinBoardsInfo[icard].boardspec[1]);
	appendlog(currentLog->log,(char*)tmpbuf,1); 
	crd_err = 1;
      }
      setstatus(icard,crd_err,currentLog->errlog);
      /* close(ttyfd[icard]); */
      /*sprintf(tmpbuf,"robinscope -m %d -R",icard);
	executedup(tmpbuf,currentLog->log,currentLog->errlog);*/
    }
  }

  restartPPC(ncards,ttyfd,currentLog);

  return statapp;
}

int load_fpga(int ncards,cbdata* cardPciLog) {
  int icrd,statapp;
  char cmd[DefaultSize],tmpbuf[DefaultSize];
  const char *cmd_core = "robin_fpga_load --reset";
  /*const char *fpga = "proj_1.bit";*/
  char *pbuf;
  short crd_err;

  if( ncards == 0 ) {
    appendlog(cardPciLog->errlog,(char*)"\nError: unknown number of cards, \ntry 'available cards' option first",1);
    return -1;
  }

  statapp = 0;
  for( icrd = 0; icrd < ncards; icrd++ ) {
    if( runBase.cardSelect[icrd] == 1 ) {
      sprintf(tmpbuf,"\n\n----  Configuring FPGA CARD %d, %s %s, serial %d bus:slot %s --------\n",
	      runBase.robinBoardsInfo[icrd].boardspec[0],runBase.robinBoardsInfo[icrd].boardType,
	      runBase.robinBoardsInfo[icrd].origin,
	      runBase.robinBoardsInfo[icrd].boardspec[1],
	      runBase.robinBoardsInfo[icrd].bus_slot);
      SpecialConf[icrd] = 0;
      appendlog(cardPciLog->log,tmpbuf,1);
      crd_err = 0;
      pbuf = cardPciLog->log + strlen(cardPciLog->log)*sizeof(char);
      sprintf(cmd,"%s %d %s",cmd_core,runBase.robinBoardsInfo[icrd].boardspec[0],fpga_elf[1]);
      if( executedup(cmd,cardPciLog->log,cardPciLog->errlog) < 0 )
	perror("execute PPCmd");
      
      if( checkerr(pbuf,cardPciLog->errlog,fpgaloaderr,(sizeof(fpgaloaderr)/sizeof(fpgaloaderr[0])),tmpbuf) ) {
	crd_err = statapp = 1;
      }
      setstatus(icrd,crd_err,cardPciLog->errlog);
      /*readppcout(1,&ttyfd[icrd],runBase.currentLog->log);*/
    }  
  }
  sleep(1);
  return statapp;
}

int getmacnode(char *logapp,char *macadd,char **rolid) {
  const char *searchStr[] = {"RobIn MAC Address: ","Using IP "};
  int count,irol;
  char *pos;

  if( runBase.nettran < 0 || runBase.nettran > 1 ) {
    macadd[0] = '\0';
    return -1;
  }

  pos = logapp; /* maclog and  rollog "walk" along the logapp file */
  if( (pos = strstr(pos,searchStr[runBase.nettran])) != NULL ) {
    pos+= strlen(searchStr[runBase.nettran]);
    if( runBase.nettran ==  0 ) 
      getmac(macadd,pos); 
    else {
      getip(macadd,pos);
    }
  }
  else {
    macadd[0] = '\0';
    return -1;
  }
  
  /* ROL ids */
  pos = logapp;
  for( irol = 0; irol < NRols; irol++ ) {
    count = 0;
    if( (pos = strstr(pos,"Using DC node Id")) != NULL ) {
      pos+= strlen("Using DC node Id")*sizeof(char);
      while( *pos == ' ' && *pos != '\0' && *pos != '\n' ) 
	pos++;
      
      while( *pos != ' ' && *pos != '\0' && *pos != '\n' && count < (IDlen-1) ) {
	if( (*pos >= '0' && *pos <= '9') || (tolower(*pos) >= 'a' && tolower(*pos) <= 'f')  || *pos == 'x' ) 
	  rolid[irol][count++] = *pos;
	pos++;
      }
    }
    rolid[irol][count] = '\0';
  } 
  
  return 0;
}

int getinterface(char *localintf,cbdata *cardLog,short transport) {
      
  if( runBase.Neth == 0 ) {
    appendlog(cardLog->errlog,(char*)"\nERROR: No netaddress address for the interface\n",1);
    return -1;
  }
 
  if( transport ) 
    strcpy(localintf,ifInfo[runBase.ethdev].if_inetadd);
  else
    strcpy(localintf,ifInfo[runBase.ethdev].if_hwadd);
  
  return 0;
}

int run_netreq(int ncards,cbdata *currentLog,int *ttyfd) {
  FILE *fnetlog;
  char localintf[IDlen],macadd[IDlen],**RolnodeId;
  const char *transport[] = {"transport rawsock\n","transport udp\n"};
  const char *confline[] = {"requestsize 512\ndatacheck 1\n",
			    "debugloop 20000000\nmaxruntime 15\nthrottle 10\n",
			    "delgroup 100\nreqrat 100\nlogfile nettest.log\ntstamp 1\n",
			    "hexdump 1\n","datadumpto Roddatainhex.dat\nNumberOfEvents 0\n"};
  char writeLine[DefaultSize10],scratchbuf[DefaultSize10];
  char tmpstr[DefaultSize];
  int fd,irol,statapp,idol;
  const char *cmd[] = {"netrequester netreq.conf"};
  const char *datgen[] = {"setenv RolDataGen 1","setenv TestSize 128",};
  const char *starppc[] = {"setenv  RolEnabled 1","setenv NetworkEnabled 1","setenv EbistEnabled 0",
			   "setenv Rolextloop 0","bootelf 0xff800000"};
  char *logpos;
  unsigned int icmd;
  int icrd,crd_err;

  if( ncards == 0 ) {
    appendlog(currentLog->errlog,(char*)"\nError: unknown number of cards, \ntry 'available cards' option first",1);
    return -1;
  }

  if( getinterface(localintf,currentLog,runBase.nettran) != 0 ) {
    appendlog(currentLog->errlog,(char*)"\nNo net address for the local interface\n",1);
    return -1;
  }

  if( (RolnodeId = malloc(NRols*sizeof(char*))) == NULL )
    return -1;
  else
    for( irol = 0; irol < NRols; irol++ ) 
      if( (RolnodeId[irol] = malloc(IDlen*sizeof(char))) == NULL )
	return -1;

  statapp = 0;
  for( icrd = 0; icrd < ncards; icrd++ ) {
    if( runBase.cardSelect[icrd] == 1 ) {
      /*opentty(&ttyfd[icrd],runBase.robinBoardsInfo[icrd].ttyused);*/

      crd_err = 0;
      sprintf(scratchbuf,"\n-----NETREQUESTER CARD %d %s serial %d bus:slot %s -------\n",
	      (icrd+1),runBase.robinBoardsInfo[icrd].origin,runBase.robinBoardsInfo[icrd].boardspec[1],
	      runBase.robinBoardsInfo[icrd].bus_slot);
      if( appendlog(currentLog->log,scratchbuf,1) != 0 || 
	  appendlog(currentLog->errlog,scratchbuf,0) != 0 )
	return -1;
      
      logpos = currentLog->log + strlen(currentLog->log);

      stopPpc(&ttyfd[icrd], currentLog);

      if( (fnetlog = fopen("nettest.log", "a")) == NULL ) {
	appendlog(currentLog->errlog,(char*)"cannot open nettest.log\n",1);
	return -1;
      }
      fprintf(fnetlog,"%s",scratchbuf);
      fclose(fnetlog);
      
      /* start ppc */
      if( runBase.Ndolar == 0 || !runBase.Dolar ) {
	for( icmd = 0; icmd < (sizeof(datgen)/sizeof(datgen[0])); icmd++ ) 
	  if( run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)datgen[icmd]) )
	   appendlog(currentLog->log,(char*)"ERROR: PPC failed for the current card\n",1);
      }
      sprintf(tmpstr,"setenv DcNodeId %d",(runBase.robinBoardsInfo[icrd].boardspec[1]*3));
      run_ppccmd(1,currentLog,&ttyfd[icrd],tmpstr);
      for( icmd = 0; icmd < (sizeof(starppc)/sizeof(starppc[0])); icmd++ ) 
	 if( run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)starppc[icmd]) )
	   appendlog(currentLog->log,(char*)"ERROR: PPC failed for the current card\n",1);
      
      if( checkerr(logpos,currentLog->errlog,ppcerr,(sizeof(ppcerr)/sizeof(ppcerr[0])),(char*)" ") ) {
	appendlog(currentLog->log,(char*)"ERROR: PPC failed, netrequester will not be tested",1); 
	appendlog(currentLog->errlog,(char*)"ERROR: PPC failed, netrequester will not be tested\n",1); 
	appendlog(currentLog->log,(char*)"\n-----NETREQUESTER END-------\n",1);
	statapp--;
	crd_err = 1;
      }
      else {
	if( getmacnode(logpos,macadd,RolnodeId) != 0 ) {
	  appendlog(currentLog->errlog,(char*)"\nNo MAC address for the ROBin interface\n",1);
	  /* stop ppc */
	  run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"0");
	  run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"0");
	  crd_err = 1;
	}
	 else {
	  if( strlen(macadd) > 0 ) {	    
	    /* write config file */
	    if( (fd = open("./netreq.conf",(O_CREAT|O_RDWR|O_TRUNC))) == -1 ) { 
	      perror("netreq.conf");
	      appendlog(currentLog->errlog,(char*)"Cannot open netreq.conf file, test failed\n",1);
	      for( irol = 0; irol < NRols; free(RolnodeId[irol++]) );
	      free(RolnodeId);
	      return -1;
	    }
	    /*if( executedup((char*)"chmod +rw ./netreq.conf",currentLog->log,currentLog->errlog) < 0 ) {
	      perror(" ");
	      appendlog(currentLog->errlog,(char*)"Warning: cannot change access mode of netreq.conf\n",1);
	      }*/
	    sprintf(writeLine,"localhost %s 0 #local interface\n",localintf);
	    write(fd,writeLine,strlen(writeLine)*sizeof(char));
	    
	    for( irol = 0; irol < NRols; irol++ ) {
	      sprintf(writeLine,"remote %s %s\n",macadd,RolnodeId[irol]);
	      write(fd,writeLine,strlen(writeLine)*sizeof(char));
	    }
	    sprintf(writeLine,"remote %s 2 #delete node\n",macadd);
	    write(fd,writeLine,strlen(writeLine)*sizeof(char));

	    write(fd,transport[runBase.nettran],(strlen(transport[runBase.nettran])*sizeof(char)));
	    for( icmd = 0; icmd < (sizeof(confline)/sizeof(confline[0])); icmd++ )
	      write(fd,confline[icmd],(strlen(confline[icmd])*sizeof(char)));
	    close(fd);

	    sleep(1);
	    sprintf(scratchbuf,"ping -c 2 %s",macadd);
	    executedup(scratchbuf,currentLog->log,currentLog->log);
	    /* run dolarscope and netrequester */
	    if( runBase.Ndolar != 0 && runBase.Dolar ) {
	      for( idol = 0; idol < runBase.Ndolar; idol++ ) {
		sprintf(scratchbuf,"expect start_dolar %d 256",(idol+1));
		if( executedup(scratchbuf,currentLog->log,currentLog->log) < 0 )
		  perror("execute dolarscope start");
	      }
	    }

	    logpos = currentLog->log + strlen(currentLog->log);
	    for( icmd = 0; icmd < (sizeof(cmd)/sizeof(cmd[0])); icmd++ ) {
	      if( executedup((char*)cmd[icmd],currentLog->log,currentLog->log) < 0 ) {
		perror("execute netrequester");
		appendlog(currentLog->log,
			  (char*)"ERROR: cannot run netrequester - check LD_LIBRARY_PATH, libs access or if application exists\n",1);
		appendlog(currentLog->errlog,(char*)"cannot run netrequester - check if not deleted\n",1);
	      }
	    }

	    /*stop dolar cards */
	    if( runBase.Ndolar != 0 && runBase.Dolar ) {
	      for( idol = 0; idol < runBase.Ndolar; idol++ ) {
		sprintf(scratchbuf,"expect stop_dolar %d",(idol+1));
		if( executedup(scratchbuf,currentLog->log,currentLog->log) < 0 )
		  perror("execute dolarscope stop");
	      }
	    }

	    if( checkerr(logpos,currentLog->errlog,commonerr,(sizeof(commonerr)/sizeof(commonerr[0])),(char*)" ") ) {
	      statapp = -1; crd_err = 1;
	    }	    
	    else
	      appendlog(currentLog->errlog,(char*)"\nNo errors, no warnings\n",1);
	  }
	  /* stop ppc */
	  sprintf(scratchbuf,"robinscope -m %d -R",icrd);
	  executedup(scratchbuf,currentLog->log,currentLog->errlog);	  
	  /*run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"0");
	    run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"0");*/
	}
	
	if( strstr(logpos,"Not present-pending	:	0") == NULL ) {
	  appendlog(currentLog->log,(char*)"\nWARNING: \"not perent\" events detected\nCHECK OPTICAL links\n",1);
	}
	appendlog(currentLog->log,(char*)"\n-----NETREQUESTER END-------\n",1);
      }
      run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"setenv RolDataGen 0");
      /*run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"setenv SkipReload 0");*/
      setstatus(icrd,crd_err,currentLog->errlog);
      /*sprintf(tmpstr,"robinscope -m %d -R",icrd);
	executedup(tmpstr,currentLog->log,currentLog->errlog);*/
      /*close(ttyfd[icrd]);*/
    }
  }

  for( irol = 0; irol < NRols; free(RolnodeId[irol++]) ); 
  free(RolnodeId);

  restartPPC(ncards,ttyfd,currentLog);

  return statapp;
}

int run_pcireq(int ncards,cbdata *currentLog, int *ttyfd) {
  const char *labstring[] = {"swVersion","designVersion"};
  const char *datgen[] = {"setenv TestSize 1350"};
  const char *starppc[] = {"setenv EbistEnabled 0","setenv bootcmd bootelf 0xff800000",
			   "setenv RolDataGen 0","bootelf 0xff800000"};
  char *logpos,*pbuf,*ptmp;
  char loclog[MAX_BYTE];
  char scratchbuf[DefaultSize10];
  unsigned int icmd;
  int icrd,statapp,crd_err,idol,largs,iver;

  if( ncards == 0 ) {
    appendlog(currentLog->errlog,(char*)"\nError: unknown number of cards, \ntry 'available cards' option first",1);
    return -1;
  }

  crd_err = 0;
  if( (statapp = initRobinMsg(ncards,currentLog->log,currentLog->errlog,runBase.cardSelect)) > 0  ) {
    appendlog(currentLog->log,(char*)"ERROR:  initRobinMsg (robinconfig) failed\n",1);
    appendlog(currentLog->errlog,(char*)"ERROR:initRobinMsg (robinconfig) failed, see main logfile for details\n",1);
    if( runBase.NCardsSel == 0 ) {
      appendlog(currentLog->log,(char*)"No more error-free cards to test\n",1);
      return statapp;
    }
  }

  statapp = 0;

  resetstatus(); refreshwind(-1);
  for( icrd = 0; icrd < ncards; icrd++ ) {
    if( runBase.cardSelect[icrd] == 1 ) {

      /*opentty(&ttyfd[icrd],runBase.robinBoardsInfo[icrd].ttyused);*/

      crd_err = 0;
      sprintf(scratchbuf,"\n-----PCIREQUESTER CARD %d %s serial %d bus:slot %s -------\n",
	      (icrd+1),runBase.robinBoardsInfo[icrd].origin,runBase.robinBoardsInfo[icrd].boardspec[1],
	      runBase.robinBoardsInfo[icrd].bus_slot);
      if( appendlog(currentLog->log,scratchbuf,1) != 0 || 
	  appendlog(currentLog->errlog,scratchbuf,0) != 0 )
	return -1;

      logpos = currentLog->log + strlen(currentLog->log);
      stopPpc(&ttyfd[icrd],currentLog);
      
      if( runBase.Dolar ) {
	appendlog(currentLog->log,(char*)"Currently cannot run PCI test with Dolar card.\nSwitching to internal generator\n",1);
	runBase.Dolar = 0;
      }

      if( runBase.Ndolar == 0 || !runBase.Dolar ) {
	for( icmd = 0; icmd < (sizeof(datgen)/sizeof(datgen[0])); icmd++ ) 
	  if( run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)datgen[icmd]) ) {
	    appendlog(currentLog->log,(char*)"ERROR: PPC failed for the current card\n",1);
	    appendlog(currentLog->errlog,(char*)"ERROR: PPC failed for the current card\n",1);
	  }
      }
	
      /* start ppc */
      for( icmd = 0; icmd < (sizeof(starppc)/sizeof(starppc[0])); icmd++ ) 
	if( run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)starppc[icmd]) ) {
	  appendlog(currentLog->log,(char*)"ERROR: PPC failed for the current card\n",1);
	  appendlog(currentLog->errlog,(char*)"ERROR: PPC failed for the current card\n",1);
	}
      
	
      if( checkerr(logpos,currentLog->errlog,ppcerr,(sizeof(ppcerr)/sizeof(ppcerr[0])),(char*)" ") ) {
	appendlog(currentLog->log,(char*)"ERROR: PPC failed for the current card\n",1);
	appendlog(currentLog->errlog,(char*)"ERROR: PPC failed, pcirequester will not be tested\n",1); 
	appendlog(currentLog->log,(char*)"-----PCIREQUESTER END-------\n",1);
	statapp--; crd_err = 1;
      }
      else {
	/*if( runBase.Ndolar != 0 && runBase.Dolar ) {
	  for( idol = 0; idol < runBase.Ndolar; idol++ ) {
	    sprintf(scratchbuf,"expect start_dolar %d 256",(idol+1));
	    if( executedup(scratchbuf,currentLog->log,currentLog->errlog) < 0 )
	      perror("execute dolarscope start");
	  }
	  }*/

	/* get the swVersion and designVersion */
	sprintf(scratchbuf,"robinscope -m %d -s",icrd);
	loclog[0] = '\0';
	if( executedup(scratchbuf,loclog,loclog) < 0 )
	  perror("execute");
	
	iver = 4;
	for( largs = 0; largs < (int)(sizeof(labstring)/sizeof(labstring[0])); largs++ ) {
	  if( (logpos = strstr(loclog,labstring[largs])) != NULL ) {
	    if( (pbuf = strstr(logpos,"\n")) != NULL )
	      *pbuf = '\0';
	    sprintf(scratchbuf,"%s\n",logpos);
	    
	    if( (ptmp = strstr(scratchbuf,"0x")) != NULL ) 
	      sscanf(ptmp,"%x",&(runBase.robinBoardsInfo[icrd].boardspec[iver++]));
	    if( pbuf != NULL ) 
	      *pbuf = '\n';
	  }
	}

	/*create testpci script, with swVersion and designVersion as arguments */
	/*if( pciscript((char*)"testpci",currentLog->log,currentLog->log,runBase.Dolar,runBase.robinBoardsInfo[icrd].boardspec) != 0  ) {
	  appendlog(currentLog->errlog,(char*)"WARNING: Cannot create testpci script, trying to run with default\n",1);
	  }*/

	/* run PCI test */
	logpos = currentLog->log + strlen(currentLog->log);
	/*sprintf(scratchbuf,"expect -c expect_before testpci %d 215",icrd);*/
	sprintf(scratchbuf,"test_Robin -P %d -S 0x%x -F 0x%x -p -v -r 100 -n 10000 -i",
		icrd,runBase.robinBoardsInfo[icrd].boardspec[4],runBase.robinBoardsInfo[icrd].boardspec[5]);

	if( executedup(scratchbuf,currentLog->log,currentLog->log) < 0 ) {
	  appendlog(currentLog->log,(char*)"Error: test_Robin failed\n",1);
	  statapp = crd_err = 1;
	}
	
	if( strstr(logpos,"Number of getFragment without data during running = ") == NULL && 
	    strstr(logpos,"Time per event") == NULL ) {
	  appendlog(currentLog->log,(char*)"ERROR: no run statistics - PCI requestre failed\nCheck optical links\n",1);
	  appendlog(currentLog->errlog,(char*)"ERROR: no run statistics - PCI requestre failed\nCheck optical links\n",1);
	  statapp = crd_err = 1;
	}

	/*stop dolar cards */
	if( runBase.Ndolar != 0 && runBase.Dolar ) {
	  for( idol = 0; idol < runBase.Ndolar; idol++ ) {
	    sprintf(scratchbuf,"expect stop_dolar %d",(idol+1));
	    if( executedup(scratchbuf,currentLog->log,currentLog->errlog) < 0 )
	      perror("executedolarscope stop");
	  }
	}
	
	if( checkerr(logpos,currentLog->errlog,ROSerr,(sizeof(ROSerr)/sizeof(ROSerr[0])),scratchbuf) )
	  statapp = crd_err = 1;
	
	/* stop ppc */
	sprintf(scratchbuf,"robinscope -m %d -R",icrd);
	executedup(scratchbuf,currentLog->log,currentLog->errlog);
	
	appendlog(currentLog->log,(char*)"-----PCIREQUESTER END-------\n",1);
      }

      run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"setenv RolDataGen 0");
      /*run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"setenv SkipReload 0");*/
      /*sprintf(scratchbuf,"robinscope -m %d -R",icrd);
	executedup(scratchbuf,currentLog->log,currentLog->errlog);*/
      /* run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"saveenv"); */
      /* close(ttyfd[icrd]); */
    }
    setstatus(icrd,crd_err,currentLog->errlog);
  }

  restartPPC(ncards,ttyfd,currentLog);
  return statapp;
}

int fpga_check(int ncards,cbdata *currentLog,int *ttyfd) {
  unsigned int icmd;
  int statapp,icard;
  int crd_err;
  char tmpbuf[DefaultSize],*logpos;
  const char *bcmd[] = {"imw 30.0 a0 1","imw 30.0 a4 1","reset"};

  if( ncards == 0 ) {
    appendlog(currentLog->errlog,(char*)"\nError: unknown number of cards, \ntry 'available cards' option first",1);
    return -1;
  }

  if( ttyfd == NULL ) {
    appendlog(currentLog->errlog,(char*)"\ntty not opened",1);
    return -1;
  }
  statapp = 0; 
  for( icard = 0; icard < ncards; icard++ ) {
    if( runBase.cardSelect[icard] == 1 ) {
      if( *(ttyfd+icard) > 0 ) { 
	sprintf(tmpbuf,"\n\n----- Processing CARD %d %s, serial %d bus:slot %s --------\n",
		(icard+1),runBase.robinBoardsInfo[icard].origin,runBase.robinBoardsInfo[icard].boardspec[1],
		runBase.robinBoardsInfo[icard].bus_slot);
	appendlog(currentLog->log,tmpbuf,1);
	logpos = currentLog->log + strlen(currentLog->log);
	crd_err = 0; 
	for( icmd = 0; icmd < XtNumber(bcmd); icmd++ )
	  statapp = run_ppccmd(1,currentLog,(ttyfd+icard),(char*)bcmd[icmd]);
	sleep(4);

	if( strstr(logpos,"Application terminated") != NULL || 
	    checkerr(logpos,currentLog->errlog,ppcerr,(sizeof(ppcerr)/sizeof(ppcerr[0])),tmpbuf) != 0 ) {
	  statapp = crd_err = 1;
	}
	else {
	  /*terminate PPC */
	  run_ppccmd(1,currentLog,(ttyfd+icard),(char*)"0");
	  run_ppccmd(1,currentLog,(ttyfd+icard),(char*)"0");
	}
	setstatus(icard,crd_err,currentLog->errlog);
      }
      else {
	appendlog(currentLog->log,(char*)"Error: File descriptor invalid, check tty connection to the card\n",1);
	appendlog(currentLog->errlog,(char*)"Error: File descriptor invalid, check tty connection to the card\n",1);
	statapp = -1;
      }
    }
  }
  return statapp;
}

int passed(Widget w) {
  XtVaSetValues(w,XtVaTypedArg,XmNbackground,XmRString,actgreen,strlen(actgreen)+1,
		XtVaTypedArg,XmNlabelString, XmRString, "Done",strlen("Done")+1,NULL);
  XtVaSetValues(w,XtVaTypedArg,XmNforeground,XmRString,black,strlen(black)+1,NULL);
  XtSetSensitive(w, True);

  return 0;
}

int passedwarrnings(Widget w) {
  XtVaSetValues(w,XtVaTypedArg,XmNbackground,XmRString,actyell,strlen(actyell)+1,
		XtVaTypedArg,XmNlabelString, XmRString, "Passed",strlen("Passed")+1,NULL);
  XtVaSetValues(w,XtVaTypedArg,XmNforeground,XmRString,black,strlen(black)+1,NULL);
  XtSetSensitive(w, True);

  return 0;
}

int failed(int cwid,runshell_t *runBase_loc) {

  XtVaSetValues(runBase_loc->statButtons[cwid],XtVaTypedArg,XmNbackground,XmRString,actwhite,strlen(actwhite)+1,
		XtVaTypedArg,XmNforeground,XmRString,actred,strlen(actred)+1,
		XtVaTypedArg,XmNlabelString, XmRString, "Failed",(strlen("Failed")+1),NULL);
  XtSetSensitive(runBase_loc->statButtons[cwid], True);

  XtVaSetValues(runBase_loc->errButtons[cwid],XtVaTypedArg,XmNbackground,XmRString,actred,strlen(actred)+1,
		XtVaTypedArg,XmNforeground,XmRString,actwhite,(strlen(actwhite)+1),NULL);
  XtSetSensitive(runBase_loc->errButtons[cwid], True);

  return 0;
}

void testEnd(FILE *fplog,FILE *fprep,cbdata *logs, char *fieldLabel,int iwid) {
  const char *sep = "----------";
  char *pbuf;
  char tmpstr[DefaultSize];

  sprintf(tmpstr,"\n\t\t%s%s%s\n",sep,fieldLabel,sep);
  fwrite(tmpstr,sizeof(char),strlen(tmpstr),fplog);
  fwrite(tmpstr,sizeof(char),strlen(tmpstr),fprep);

  if( strlen(logs->log) == 0 && strlen(logs->errlog) == 0 ) {
    fwrite("Not tested\n",sizeof(char),strlen("Not tested\n"),fplog);
    fwrite("Not tested\n",sizeof(char),strlen("Not tested\n"),fprep);
    return;
  }

  if( strlen(logs->log) > 0 ) {
    if( (logs->testlog[iwid] = malloc((strlen(logs->log)+1)*sizeof(char))) == NULL ) {
      perror("testEnd malloc");
      return;
    }
    else {
      strcpy(logs->testlog[iwid],logs->log);

      fwrite(logs->log,sizeof(char),strlen(logs->log),fplog);
      if( strstr(fieldLabel,"available cards") != NULL ) {
	if( (pbuf = strstr(logs->log,"boardType")) != NULL )
	  fwrite(pbuf,sizeof(char),strlen(pbuf),fprep);
      }
      else
	fwrite("Tested, see log file for details\n",sizeof(char),strlen("Tested, see log file for details\n"),fprep);
    }
  }
   
  sprintf(tmpstr,"\n----------Errors:\n");
  fwrite(tmpstr,sizeof(char),strlen(tmpstr),fplog);
  fwrite(tmpstr,sizeof(char),strlen(tmpstr),fprep);
  if( strlen(logs->errlog) > 0 ) {
    fwrite(logs->errlog,sizeof(char),strlen(logs->errlog),fplog);
    fwrite(logs->errlog,sizeof(char),strlen(logs->errlog),fprep);

    if( (logs->testerrlog[iwid] = malloc((strlen(logs->errlog)+1)*sizeof(char))) == NULL ) {
      perror("testEnd malloc");
    }
    else
      strcpy(logs->testerrlog[iwid],logs->errlog);

  }
  else {
    fwrite("No errors\n",sizeof(char),strlen("No errors\n"),fplog);
    fwrite("No errors\n",sizeof(char),strlen("No errors\n"),fprep);
  }

  fflush(fplog); fflush(fprep);
}

int getDeviceInfo(char *scanlogpos,cbdata *cardPciLog) {
  char *scanlog,*pser;
  char *argstline[Max_NumItems],*argstitem[Max_NumItems];
  unsigned int item_nr;
  unsigned int j;
  int device_nr,iarg,jarg,line_nr;
  const char *labels = "BoardId";

  if( (scanlog = malloc((strlen(scanlogpos)+1)*sizeof(char))) == NULL ) {
    appendlog(cardPciLog->errlog,(char*)"ERROR: getDeviceInfo: Cannot allocate temporary buffer",1);
    return -1;
  }
  if( (pser = strstr(scanlogpos,labels)) == NULL )
    return -1;
  sprintf(scanlog,"%s",pser);
  jarg = splitext(scanlog,argstline);
  
  device_nr = 0;
  for( line_nr = 1; line_nr < jarg; line_nr++ ) {
    if( strstr(argstline[line_nr],"ROBIN") != NULL ) {
      iarg = splitarg(argstline[line_nr],argstitem);
      for( item_nr = 0; item_nr < (unsigned int)iarg; item_nr++ ) {
	if( item_nr == 1 )
	  sprintf(runBase.robinBoardsInfo[device_nr].boardType,"%s",argstitem[item_nr]);
	else {
	  if( item_nr == 2 ) {
	    for( j = 0; j < (sizeof(serial_prefix)/sizeof(serial_prefix[0])); j++ ) 
	      if( strncmp(argstitem[item_nr],serial_prefix[j][0],strlen(serial_prefix[j][0])) == 0 ) {
		pser = argstitem[item_nr] + strlen(serial_prefix[j][0]);
		sscanf(pser,"%u",&runBase.robinBoardsInfo[device_nr].boardspec[1]);
		sscanf(serial_prefix[j][1],"%s",runBase.robinBoardsInfo[device_nr].origin);
		sscanf(serial_prefix[j][0],"%s",runBase.robinBoardsInfo[device_nr].prefix);
		break;
	      }
	    if( j == (sizeof(serial_prefix)/sizeof(serial_prefix[0])) ) {
	      sprintf(runBase.robinBoardsInfo[device_nr].origin,"Unknown origin");
	      runBase.robinBoardsInfo[device_nr].prefix[0] = '\0';
	      sscanf(argstitem[item_nr],"%u",&(runBase.robinBoardsInfo[device_nr].boardspec[1]));
	    }
	  }
	  else 
	    sscanf(argstitem[item_nr],"%u",&(runBase.robinBoardsInfo[device_nr].boardspec[item_nr]));
	}
      }
      ++device_nr;
    }
  }
  free(scanlog);

  return (device_nr-1);
}

void info_win(Widget parent,char *message,char *label) {
  Widget noticeshell,base,form,msg_text;
  Arg args[40];
  int nargs,nrows,ncol;
  char *pbuf,*pbufnext;
  const char *empty_message = "No log available";

    noticeshell = XtVaCreatePopupShell(label,xmDialogShellWidgetClass, parent,NULL);

  base = XtVaCreateWidget("base",xmRowColumnWidgetClass,noticeshell,NULL);
  XtVaSetValues(base,XtVaTypedArg,XmNbackground,XmRString,lightgray,strlen(lightgray)+1,NULL); 
  
  form = XtVaCreateWidget("form",xmFormWidgetClass,base, /*XmNfractionBase, 5,*/NULL);
  XtVaSetValues(form,XtVaTypedArg,XmNbackground,XmRString,lightgray,strlen(lightgray)+1,NULL); 

  /* adjust window size accordingly to the msg length */
  nrows = ncol = 0; pbuf = pbufnext = message;
  if( message == NULL )
    message = (char*)empty_message;

  while( (pbufnext = strstr(pbuf,"\n")) != NULL ) {
    nrows++;
    if( ncol < (pbufnext-pbuf) )
      ncol = (pbufnext-pbuf);
    pbuf = pbufnext + 1;
  }

  /* if the last line doesn't end with "\n" */
  if( pbuf < (message+strlen(message)) ) {
    if( ncol < ((message+strlen(message)) - pbuf) )
      ncol = (message+strlen(message)) - pbuf;
    nrows++;
  }

  if( nrows == 0 && ncol == 0 ) {
    nrows = 1; ncol = strlen(message);
  }
  /* nrows++; */

  nargs = 0;
  XtSetArg(args[nargs],XmNeditMode,XmMULTI_LINE_EDIT); nargs++;
  XtSetArg(args[nargs],XmNeditable,False);nargs++;
  XtSetArg(args[nargs],XmNcursorPositionVisible,False); nargs++;
  XtSetArg(args[nargs],XmNwordWrap,False); nargs++;
  XtSetArg(args[nargs],XmNvalue,message); nargs++;
  XtSetArg(args[nargs],XmNrows, nrows); nargs++;
  XtSetArg(args[nargs],XmNcolumns, ncol); nargs++;
  XtSetArg(args[nargs],XmNshadowThickness,1); nargs++;

  /* msg_text = XmCreateScrolledText(form,(String)"msg",args,nargs); */
  msg_text = XmCreateText(form,(String)"msg",args,nargs);
  XtVaSetValues(msg_text,XtVaTypedArg,XmNforeground,XmRString,"black",strlen("black")+1,
		XtVaTypedArg,XmNbackground,XmRString,"khaki",strlen("khaki")+1,NULL);

  XtVaSetValues(XtParent(msg_text),
		XmNleftAttachment, XmATTACH_FORM,XmNrightAttachment, XmATTACH_FORM,
		XmNtopAttachment, XmATTACH_FORM,/* XmNbottomAttachment, XmATTACH_FORM,*/NULL);

  XtManageChild(msg_text);
  XtManageChild(form); 

  XtManageChild(base); 
}

void notice_win(Widget parent,char *message,char *label) {
  Widget noticeshell,base,form,msg_text,okbutt;
  Arg args[40];
  int nargs,nrows,ncol;
  const char *empty_message = "No log available";

  noticeshell = XtVaCreatePopupShell(label,xmDialogShellWidgetClass, parent,NULL);

  base = XtVaCreateWidget("base",xmRowColumnWidgetClass,noticeshell,NULL);
  XtVaSetValues(base,XtVaTypedArg,XmNbackground,XmRString,lightgray,strlen(lightgray)+1,NULL); 
  
  form = XtVaCreateWidget("form",xmFormWidgetClass,base, XmNfractionBase, 5,NULL);
  XtVaSetValues(form,XtVaTypedArg,XmNbackground,XmRString,lightgray,strlen(lightgray)+1,NULL); 

  /* adjust window size accordingly to the msg length */
  if( message == NULL ) 
    message = (char*)empty_message;
  if( (ncol = strlen(message) + 2) > 100  ) 
    ncol = 100;  
  if( (nrows = strlen(message)/100 + 4) > 35 ) 
    nrows = 35;

  nargs = 0;
  XtSetArg(args[nargs],XmNscrollVertical,True); nargs++;
  XtSetArg(args[nargs],XmNscrollHorizontal,True); nargs++;
  XtSetArg(args[nargs],XmNeditMode,XmMULTI_LINE_EDIT); nargs++;
  XtSetArg(args[nargs],XmNeditable,False);nargs++;
  XtSetArg(args[nargs],XmNcursorPositionVisible,False); nargs++;
  XtSetArg(args[nargs],XmNwordWrap,True); nargs++;
  XtSetArg(args[nargs],XmNvalue,message); nargs++;
  XtSetArg(args[nargs],XmNrows, nrows); nargs++;
  XtSetArg(args[nargs],XmNcolumns, ncol); nargs++;
  XtSetArg(args[nargs],XmNshadowThickness,2); nargs++;

  msg_text = XmCreateScrolledText(form,(String)"msg",args,nargs);
  XtVaSetValues(msg_text,XtVaTypedArg,XmNforeground,XmRString,darkOrchid,strlen(darkOrchid)+1,
		XtVaTypedArg,XmNbackground,XmRString,actwhite,strlen(actwhite)+1,NULL);

  XtVaSetValues(XtParent(msg_text),
		XmNleftAttachment, XmATTACH_FORM,XmNrightAttachment, XmATTACH_FORM,
		XmNtopAttachment, XmATTACH_FORM,/* XmNbottomAttachment, XmATTACH_FORM,*/NULL);

  XtManageChild(msg_text);
  XtManageChild(form); 

  form = XtVaCreateWidget("form",xmFormWidgetClass,base, XmNfractionBase, 5,NULL);
  XtVaSetValues(form,XtVaTypedArg,XmNbackground,XmRString,lightgray,strlen(lightgray)+1,NULL); 

  okbutt = XtVaCreateManagedWidget("OK",xmPushButtonWidgetClass, form,
				   XmNtopAttachment, XmATTACH_FORM,XmNbottomAttachment, XmATTACH_FORM,
				   XmNleftAttachment, XmATTACH_POSITION,XmNleftPosition, 3,
				   XmNrightAttachment, XmATTACH_POSITION,XmNrightPosition, 4,
				   XmNshowAsDefault, True, XmNdefaultButtonShadowThickness, 1,
				   NULL);
  XtVaSetValues(okbutt,XtVaTypedArg,XmNforeground,XmRString,darkBlue,strlen(darkBlue)+1,
		XtVaTypedArg,XmNbackground,XmRString,LightYellow1,strlen(LightYellow1)+1,
		NULL);
  XtAddCallback(okbutt,XmNactivateCallback, blowupShell, noticeshell);
  XtManageChild(form); 

  XtManageChild(base); 
}

static void blowupShell(Widget stopbutt, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  XtPopdown((Widget)client_data);
  XtDestroyWidget((Widget)client_data);
  call_data = NULL; /* unused */
}

static void removeShell(Widget stopbutt, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  unsigned int iwid; 

  if( runBase.ttyfd != NULL ) {
    for( iwid = 0; iwid < (unsigned int)runBase.NCards; iwid++ )
      if( runBase.ttyfd[iwid] > 0 ) 
	if( close(runBase.ttyfd[iwid]) )
	  perror("closin serial:");
    free(runBase.ttyfd);
    runBase.ttyfd = NULL;
  }

  free(runBase.select);
  removeAllocWid(runBase.nfields,runBase.statButtons);
  removeAllocWid(runBase.nfields,runBase.errButtons);
  removeAllocWid(runBase.nfields,runBase.TButt);
  removeAllocWid(runBase.NCards,runBase.CardstatButt);
  free(runBase.cardSelect); 
  free(SpecialConf);

  if( runBase.robinBoardsInfo != NULL )
    free(runBase.robinBoardsInfo); 

  for( iwid = 0; iwid < runBase.nfields; iwid++ )
    if( runBase.currentLog->testlog[iwid] != NULL )
      free(runBase.currentLog->testlog[iwid]);
  free(runBase.currentLog->testlog);
  free(runBase.currentLog->testerrlog);

  XtDestroyWidget(logwin);
  logwin = NULL;

  XtSetSensitive(runBase.runButtons[0], True);
  XtSetSensitive(runBase.runButtons[1], True);
  XtSetSensitive(runBase.runButtons[2], True);

  XtPopdown((Widget)client_data);
  XtDestroyWidget((Widget)client_data);
  call_data = NULL; /* unused */
}

static void userSelect(Widget parent, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  unsigned int usel;
  char **plabel;
  short *selvar;

  usel = (unsigned int)client_data;
  if( usel > (sizeof(userSelLabels)/sizeof(userSelLabels[0])) )
    return;

  switch( usel ) {
  case 0: selvar = &runBase.Dolar; break;
  case 1: selvar = &runBase.robintty; 
    closeSerial(&runBase.ttyfd,runBase.NCards);    
    break;
  case 2: selvar = &runBase.nettran; break;
  case 3: selvar = &runBase.extLoop; break; 
  case 4: selvar = &runBase.ethdev; break; 
  default: return;
  }
  if( usel != 4 ) {
    plabel = (char**)userSelLabels[usel];
    if( *selvar )
      *selvar = 0;
    else
      *selvar = 1;

    XtVaSetValues(parent,XtVaTypedArg,XmNlabelString, XmRString,plabel[*selvar],
		strlen(plabel[*selvar])+1,NULL);
  }
  else {
    if( ++runBase.ethdev == (int)runBase.Neth )
      runBase.ethdev = 0;
    XtVaSetValues(parent,XtVaTypedArg,XmNlabelString, XmRString,ifInfo[runBase.ethdev].if_name,
		strlen(ifInfo[runBase.ethdev].if_name)+1,NULL);
  }
  call_data = NULL; /* unused */
}

static void fileSelect(Widget parent, XtPointer client_data,XmAnyCallbackStruct *call_data) {
  Widget file_sel,WorkWid;

  /* usel = (unsigned int)client_data; */

  file_sel = XmCreateFileSelectionDialog(parent,(char*)"conffiles",NULL,0);
  XtSetSensitive(XmFileSelectionBoxGetChild(file_sel,XmDIALOG_HELP_BUTTON), False);
  XtVaSetValues(file_sel,XtVaTypedArg,XmNforeground,XmRString,"White",(strlen("White")+1),
		XtVaTypedArg,XmNbackground,XmRString,"DeepSkyBlue4",(strlen("DeepSkyBlue4")+1),NULL); 

  WorkWid = XmFileSelectionBoxGetChild(file_sel,XmDIALOG_TEXT);
  XtVaSetValues(WorkWid,XtVaTypedArg,XmNbackground, XmRString, LightBlue,strlen(LightBlue)+1 ,NULL);
  WorkWid = XmFileSelectionBoxGetChild(file_sel,XmDIALOG_LIST);
  XtVaSetValues(WorkWid,XtVaTypedArg,XmNbackground, XmRString, LightBlue,strlen(LightBlue)+1 ,NULL);
  WorkWid = XmFileSelectionBoxGetChild(file_sel,XmDIALOG_DIR_LIST);
  XtVaSetValues(WorkWid,XtVaTypedArg,XmNbackground, XmRString, LightBlue,strlen(LightBlue)+1 ,NULL);
  WorkWid = XmFileSelectionBoxGetChild(file_sel,XmDIALOG_FILTER_TEXT);
  XtVaSetValues(WorkWid,XtVaTypedArg,XmNbackground, XmRString, LightBlue,strlen(LightBlue)+1 ,NULL);

  XtAddCallback(file_sel,XmNokCallback,fconf_sel,client_data);
  XtAddCallback(file_sel, XmNcancelCallback, (XtCallbackProc)XtUnmanageChild, NULL);
  XtManageChild(file_sel);
  call_data = NULL; /* unused */
}

static void fconf_sel(Widget parent, XtPointer client_data,XtPointer call_data) {
  char *filename;
  XmFileSelectionBoxCallbackStruct *cbs;
  unsigned int usel;
  char tmpstr[DefaultSize];

  cbs = (XmFileSelectionBoxCallbackStruct*)call_data;
  
  if( !XmStringGetLtoR(cbs->value,XmFONTLIST_DEFAULT_TAG,&filename) )
    return;

  if( !(*filename) ) {
    XtFree(filename);
    return;
  }

  usel = (unsigned int)client_data;
  if( usel < CONF_FILES ) 
    strcpy(fpga_elf[usel],filename);
  
  sprintf(tmpstr,"The following configuration file has been selected:\n%s\n",filename);
  appendlog(NULL,tmpstr,1);
  XtFree(filename);
  /* in the end, commit harakiri */
  XtDestroyWidget(parent);
}

int appendlog(char *currentLog, char *strappend, short screendump) {
  static XmTextPosition log_pos = 0;
  int statapp;
  /*static int minimufree = 0;*/

  /*if( !minimufree ) 
    minimufree = strlen("\n\tLOG FULL !!!\n") + 1;*/

  if( currentLog != NULL ) 
    checRepeat(currentLog, strappend);

  statapp = 0;
    /*if( strstr(strappend,"text reset") != NULL ) {
    log_pos = 0;
    if( logwin != NULL )
      XtVaSetValues(logwin,XmNvalue," ",NULL);
    return 0;
  }

  if( strstr(currentLog,"LOG FULL") == NULL ) {
    if( (space_left = (MAX_BYTE - (strlen(currentLog) + strlen(strappend)))) < minimufree ) {
      strncat(currentLog,strappend,(MAX_BYTE-(minimufree+strlen(currentLog))));
      strcat(currentLog,"\n\tLOG FULL !!!\n");
      return -1;
    }
    else
      strcat(currentLog,strappend);
  }
  else
  statapp = -1;*/

  if( logwin != NULL && screendump ) {
    XmTextInsert(logwin,log_pos,strappend);
    /* log_pos+= strlen(strappend); */
    log_pos = XmTextGetLastPosition(logwin);
    XtVaSetValues(logwin,XmNcursorPosition,log_pos,NULL);
    XmTextShowPosition(logwin,log_pos);    
  }

  return statapp;
}

int checkerr(char *currentLog,char *errlog,const char **errlist, unsigned int elistsize,  char *header) {
  unsigned int ierr;
  int head,err_size,errdet;
  char *pos,*pos_end,*log_pos,cerr[DefaultSize];

  errdet = head = 0;
  for( ierr = 0; ierr < elistsize; ierr++ ) {
    log_pos = currentLog;
    while( (pos = strstr(log_pos,errlist[ierr])) != NULL ) {
      if( !head ) {
	appendlog(errlog,header,0);
	head = 1;
      }

      pos_end = strstr(pos,"\n");
      err_size = (int)(pos_end - pos) < DefaultSize ? (int)(pos_end - pos):DefaultSize;
      strncpy(cerr,pos,err_size); cerr[(err_size/sizeof(char))] = '\0';
      appendlog(errlog,cerr,0);
      appendlog(errlog,(char*)" see main log for details\n",0);
      log_pos = pos + strlen(cerr);
      errdet++;
    }
  }

  return errdet;
}

int checkppcounters(char *currentLog,char *errlog) {
  int hserr;
  unsigned int ierr;
  char *pos,*val;

  /*check counters */
  for( ierr = 0; ierr < (sizeof(rcounters)/sizeof(rcounters[0])); ierr++ )
   if( (pos= strstr(currentLog,rcounters[ierr])) != NULL ) {
     val = pos + strlen(rcounters[ierr]) + 1;
     sscanf(val,"%d",&hserr);
     if( hserr != 0 ) {
       appendlog(errlog,(char*)"Warning:: counters warning ",0); appendlog(errlog,(char*)rcounters[ierr],0);
       appendlog(errlog,(char*)" see main log details\n",0);
       return -1;
     }
   }

  return 0;
}

void selftest(Widget parent) {
  char logapp[MAX_BYTE],errmsg[MAX_BYTE];
  const char *modules[] = {"robin","robintty","io_rcc","cmem_rcc"};
  unsigned int imod;

  logapp[0] = errmsg[0] = '\0';
  if( executedup((char*)"/sbin/lsmod",logapp,logapp) < 0 )
    perror("execute");
  
  for( imod = 0; imod < sizeof(modules)/sizeof(modules[0]); imod++ ) 
    if( strstr(logapp,modules[imod]) == NULL ) {
      appendlog(errmsg,(char*)modules[imod],0);
      appendlog(errmsg,(char*)" ",0);
    }

  if( strlen(errmsg) > 0  ) {
    appendlog(errmsg,(char*)"missing, all/some tests will not run properly\nRecommended: quite the program, fix drivers, " \
	      "\notherwise continue on your own risk. You have been warned.\n\n",0);
    appendlog(errmsg,logapp,0);
    if( parent!= NULL )
      notice_win(parent,errmsg,(char*)"WARNING");
  }
}

void addappwork(XtPointer cdata) {
  char loccmd[DefaultSize];
  char **argpoint;

  argpoint = (char**)cdata;
  strcpy(loccmd,argpoint[0]);

  executedup(loccmd, argpoint[1], argpoint[2]);
}

void updatewind(XtPointer cdata) {
  Display *dp;
  Window shellwin;

  shellwin = XtWindow((Widget)cdata);
  shellwin = XtWindowOfObject((Widget)cdata);
  dp = XtDisplay((Widget)cdata);
  while(1) {
    XmUpdateDisplay((Widget)cdata);
  }
}

void refreshwind(int pid) {
  Display *dp;
  XEvent pending_event;
  char cmd[100];
  int status;

  if( runBase.shell != NULL ) {
    dp = XtDisplay(runBase.shell);

    XFlush(dp);
    while( XCheckMaskEvent(dp,ButtonPressMask | ButtonReleaseMask /*| FocusIn*/,&pending_event) ) {
      if( pending_event.xany.window ==  XtWindow(runBase.stopButton) ) {
	StopAll = 1;
	XtDispatchEvent(&pending_event);
	if( waitpid(pid,&status,WNOHANG) != pid && pending_event.xbutton.type == ButtonRelease ) {
	  sprintf(cmd,"kill -9 %d",pid);
	  execute(cmd);
	  fprintf(stderr,"terminating pid %d, cmd: %s\n",pid,cmd); 
	  return;
	}
      }	
    }
    XmUpdateDisplay(runBase.shell);
  }
}

void setstatus(int iw,short fubar, char *errpoint) {
  errpoint = NULL; /*dummy*/
  if( runBase.cardSelect[iw] == 1 ) {
    if( !fubar ) {
      XtVaSetValues(runBase.CardstatButt[iw] ,XtVaTypedArg,XmNbackground, XmRString, actgreen,
		    strlen(actgreen)+1, NULL);
    }
    else {
      XtVaSetValues(runBase.CardstatButt[iw] ,XtVaTypedArg,XmNbackground, XmRString, actred,
		    strlen(actred)+1, NULL);
      /* runBase.robinBoardsInfo[iw].teststat = errpoint; */
      runBase.cardSelect[iw] = -1;
      
      runBase.NCardsSel--;
      XmToggleButtonSetState(runBase.CardstatButt[iw],False,False);     
    }
  }
}

void resetstatus() {
  int iw;

  for( iw = 0; iw < runBase.NCards; iw++ )
    if( runBase.cardSelect[iw] == 1 && runBase.CardstatButt[iw] != NULL )
      XtVaSetValues(runBase.CardstatButt[iw] ,XtVaTypedArg,XmNbackground, XmRString, actwhite,
		    strlen(actwhite)+1, NULL);
}

int check_active() {
  int iw;

  for( iw = 0; iw < runBase.NCards; iw++ )
    if( runBase.cardSelect[iw] == 1 ) 
      return 0;
  return -1;
}

int removeAllocWid(int items,Widget *widpoint) {
  int iwid;

  if( widpoint != NULL ) {
    for( iwid = 0; iwid < items; iwid++ ) {
      if( widpoint[iwid] != NULL )
	XtDestroyWidget(widpoint[iwid]);
    }
    free(widpoint);
  }
  return 0;
}

int checkBuildDate(char *date0) {
  int day_yer[2];
  unsigned int imon;
  int baseDate;
  char *mpos,*dpos,*tmpstr;

  if( (dpos = strstr(date0,buildstr)) == NULL ) 
    return -1;

  dpos+= strlen(buildstr);
  imon = strstr(dpos,",") - dpos;
  tmpstr = malloc(imon*sizeof(char)+1);
  strncpy(tmpstr,dpos,imon);
  tmpstr[imon] = '\0';
  dpos = tmpstr;

  for( imon = 0; imon < sizeof(monthList)/sizeof(monthList[0]); imon++ ) {
    if(  (mpos =  strstr(dpos,monthList[imon])) != NULL )
      break;
  }

  if( imon >= 12 ) {
    free(tmpstr);
    return -1;
  }
  dpos = mpos + strlen(monthList[imon]);
  
  baseDate = 30*imon;
  sscanf(dpos,"%d%d",&day_yer[0],&day_yer[1]);
  baseDate = 30*imon + day_yer[0] + 365*day_yer[1];

  free(tmpstr);

  return baseDate;
}

int oldstyle_ebist(cbdata *logs,char *logpos,char **labels) {
  unsigned int ilab,start_label,irol,icheck;
  const char *global_check[] = {"SecSi MAGIC: 0x12344321","FPGA already programmed","RCLK is within tolerance",
			  "Net test returns 0x0"};
  const char *roltest[][2] = {{"ROL","test returns 0x0"},{"Buffer ",": Totally 0 errors"}};
  char tmpstr[DefaultSize];

  for( start_label = ilab = 0; ilab < runBase.nfields; ilab++ ) {
    if( strstr(labels[ilab],"Open tty:") != NULL || strstr(labels[ilab],"Run eBIST:") != NULL )
      start_label++; 
  }
  
  ilab = start_label;
  for( icheck = 0; icheck < (sizeof(global_check)/sizeof(global_check[0])); icheck++,ilab++ ) {
    if( strstr(logpos,global_check[icheck]) == NULL ) {
      sprintf(tmpstr,"Invalid/failed test %s\n",labels[ilab]);
      appendlog(logs->errlog,tmpstr,1);
      failed(ilab,&runBase);
    }
    else {
      passed(runBase.statButtons[ilab]);
      sprintf(tmpstr,"%s OK, examine the log for more details\n",labels[ilab]);
      appendlog(logs->log,tmpstr,0);
    }
  }
  
  ilab+= runBase.extLoop; /* make an adjustment, depending if ROL will be tested with ext loopback */
  for( icheck = runBase.extLoop; icheck < (sizeof(roltest)/sizeof(roltest[0])); icheck++,ilab++ ) {
    for( irol = 0; irol < NRols; irol++ ) {
      sprintf(tmpstr,"%s%d%s",roltest[icheck][0],irol,roltest[icheck][1]);
      if( strstr(logpos,tmpstr) == NULL ) {
	failed(ilab,&runBase);
	sprintf(tmpstr,"Test %s for ROL %d failed\n",labels[ilab],irol);
	appendlog(logs->errlog,tmpstr,1);
      }
      else {
	passed(runBase.statButtons[ilab]);
	sprintf(tmpstr,"%s OK, examine the log for more details\n",labels[ilab]);
	appendlog(logs->log,tmpstr,0);
      }
    }
  }
  XmUpdateDisplay(runBase.shell);

  return 0;
}

int newstyle_ebist(cbdata *logs,char *logpos,char **labels) {
  int ilab;
  unsigned int errcode,irol,icheck,irolfound;
  const char *serchstr = "BIST result ";
  char *pos,*tmppos;
  char tmpstr[DefaultSize];

  irolfound = 0;
  for( irol = 0; irol < NRols; irol++ ) {
    ilab = 2;
    sprintf(tmpstr,"%s",serchstr);
    if( (pos = strstr(logpos,tmpstr)) != NULL ) {
      irolfound++;
      pos+= strlen(tmpstr);
      tmppos = strstr(pos,"\n");
      if( (tmppos - pos) < DefaultSize ) {
	strncpy(tmpstr,pos,(size_t)(tmppos - pos));
	tmpstr[(unsigned int)(tmppos - pos)] = '\0';
	sscanf(tmpstr,"%xd",&errcode);
      }
      
      for( icheck = 0; icheck < 6; icheck++ ) {
	if( errcode & ebisterr[icheck].errshift ) {
	  sprintf(tmpstr,"ROL %d: Invalid/failed test %s %s\n",irol,labels[ilab],ebisterr[icheck].errstring);
	  appendlog(logs->errlog,tmpstr,1);
	  failed(ilab,&runBase);
	}
	else {
	  passed(runBase.statButtons[ilab]);
	  sprintf(tmpstr,"%s OK, examine the log for more details\n",labels[ilab]);
	  appendlog(logs->log,tmpstr,0);
	}
	ilab++;
      }   
    } 
  }
  if( irolfound < NRols ) {
    appendlog(logs->errlog,(char*)"Not all ROL statistic is available\n",1);
    return -1;
  }

  return 0;
}

int ebistErrCheck(cbdata *logs,char *logpos) {
  int ebistStat;
  unsigned int errcode,irol,icheck;
  const char *serchstr = "BIST result ";
  char *pos,*tmppos;
  char tmpstr[DefaultSize];

  ebistStat = 0;
  for( irol = 0; irol < NRols; irol++ ) {
    if( (pos = strstr(logpos,serchstr)) != NULL ) {
      pos+= strlen(serchstr);
      tmppos = strstr(pos,"\n");
      if( (tmppos - pos) < DefaultSize ) {
	strncpy(tmpstr,pos,(size_t)(tmppos - pos));
	tmpstr[(unsigned int)(tmppos - pos)] = '\0';
	sscanf(tmpstr,"%xd",&errcode);
      }
      
      for( icheck = 0; icheck < sizeof(ebisterr)/sizeof(ebisterr[0]); icheck++ ) {
	if( errcode & ebisterr[icheck].errshift ) {
	  sprintf(tmpstr,"ERROR: ROL %d: failed test %s\n",irol,ebisterr[icheck].errstring);
	  appendlog(logs->errlog,tmpstr,1);
	  ebistStat--;
	}
      }
      logpos = pos;
    }
    else {
      sprintf(tmpstr,"ERROR: no statistic available for the ROL %d\n",irol);
      appendlog(logs->errlog,tmpstr,1);
      return -1;
    }
  }

  return ebistStat;
}

int runRobins(char *log) {
  int idol;
  char scratchbuf[DefaultSize10];
  char tmpbuf[MAX_BYTE];
  int j,nfdesc,nfds,timeout,in_bytes;
  
  if( runBase.Ndolar != 0 && runBase.Dolar ) {
    for( idol = 0; idol < runBase.Ndolar; idol++ ) {
      sprintf(scratchbuf,"expect start_dolar %d 1024",(idol+1));
      system(scratchbuf);

      /* check main program pipes */
      nfdesc = 2; timeout = 2000;
      while( (nfds = poll(ufdsloc,nfdesc,timeout)) > 0 ) {
	for( j = 0; j < nfdesc; j++ ) {
	  if( ufdsloc[j].revents != 0 ) {
	    if( (in_bytes = read(ufdsloc[j].fd,tmpbuf,DefaultSize)) > 0 ) {
	      tmpbuf[in_bytes] = '\0';
	      appendlog(log,tmpbuf,1);
	    }
	    if( --nfds == 0 ) 
	      break;
	  }
	}
      }
    }
  }
  
  return 0;
}

int restartPPC(int ncards, int *ttyfd,cbdata *currentLog) {
  int icrd,crd_err;
  char *logpos;

  crd_err = 0;
  for( icrd = 0; icrd < ncards; icrd++ ) {
    if( runBase.cardSelect[icrd] == 1 ) {
      appendlog(currentLog->log,(char*)"restartPPC\nDevice: ",1);
      appendlog(currentLog->log,runBase.robinBoardsInfo[icrd].ttyused,1);
      appendlog(currentLog->log,(char*)"\n",1);
      /*if( opentty(&ttyfd[icrd],runBase.robinBoardsInfo[icrd].ttyused) != 0 ) {
	appendlog(currentLog->log,"restartPPC: opentty failed\n",1);
	appendlog(currentLog->log,runBase.robinBoardsInfo[icrd].ttyused,1);
	return -1;
	}*/

      crd_err = 0;
      logpos = currentLog->log + strlen(currentLog->log);

      stopPpc(&ttyfd[icrd],currentLog);
      if( strstr(logpos,"=>") != NULL ) {
	if( run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"bootelf 0xff800000") )
	  crd_err = 1;
      }
      else {
	if( run_ppccmd(1,currentLog,&ttyfd[icrd],(char*)"x") )
	  crd_err = 1;
      }

      setstatus(icrd,crd_err,currentLog->errlog);
      /* close(ttyfd[icrd]); */
    }
    else
      appendlog(currentLog->log,(char*)"card disabled\n",1);
  }

  return crd_err;
}

void stopPpc(int *ttyfd, cbdata *currentLog) {
  char *logpos;
  
  logpos = currentLog->log + strlen(currentLog->log);
  run_ppccmd(1,currentLog,ttyfd,(char*)"\r");
  if( strstr(logpos,"0 : Terminate") != NULL ) {
    run_ppccmd(1,currentLog,ttyfd,(char*)"0");
    run_ppccmd(1,currentLog,ttyfd,(char*)"\r");
  }
}

int dolar_scripts(char *logapp,char *errlog) {
   FILE *fdolar;
   const char *start_dolar0 = "send_user \"[lrange $argv 0 1]\\n\"\nspawn dolarscope [lrange $argv 0 1]\n" \
     "expect \"Your choice\"\nexpect \":\"\nsend \"7\\n\"\nexpect \"Reset which channel (1-4, 5:selected, 0:cancel)\"\n" \
     "send \"5\\n\"\nexpect \"Your choice\"\nsend \"8\\n\"\nexpect \"Enter the channel number (1-4, 5:selected, 0:cancel)\"\n" \
     "send \"5\\n\"\nexpect \"Your choice\"\nsend \"1\\n\"\nexpect \"Enter the fragment frequency (in Hz) \"\n" \
     "send \"250000\\n\"\nexpect \"Select the pattern:\"\nexpect \"Your choice\"\nsend \"6\\n\"\n";
   const char *start_dolar1 = "expect \"Enter the fragment length (14-8191 words)\"\nexpect \" :\"\n" \
     "send \"[lrange $argv 1 2]\\n\"\n" \
     "expect \"Enter the level 1 ID \"\nsend \"0\\n\"\n\nexpect \"Your choice\"\nsend \"9\\n\"\n" \
     "expect \"Start/stop which channel (1-4, 5:selected, 0:cancel)\"\nsend \"5\\n\"\n\nexpect \"Your choice\"\n" \
     "send \"0\\n\"\nexit\n";
   const char *stop_dolar = "spawn dolarscope [lrange $argv 0 1]\nexpect \"Your choice\"\nsend \"9\\n\"\n" \
     "expect \"Start/stop which channel (1-4, 5:selected, 0:cancel)\"\nsend \"5\\n\"\nexpect \"Your choice\"\nsend \"0\\n\"\n";

  if( !(fdolar = fopen("start_dolar", "w")) ) {
    appendlog(logapp,(char*)"ERROR: Cannot open start_dolar file\n",1);
    appendlog(errlog,(char*)"ERROR: Cannot open start_dolar file\n",0);
    return -1;
  }
  fprintf(fdolar,"%s%s\n",start_dolar0,start_dolar1);
  fclose(fdolar);

  if( !(fdolar = fopen("stop_dolar", "w")) ) {
    appendlog(logapp,(char*)"ERROR: Cannot open stop_dolar file\n",1);
    appendlog(errlog,(char*)"ERROR: Cannot open stop_dolar file\n",0);
    return -1;
  }
  fprintf(fdolar,"%s\n",stop_dolar);
  fclose(fdolar);

  return 0;
}
