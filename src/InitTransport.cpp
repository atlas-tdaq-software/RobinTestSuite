#include <iostream>

#include "RobinTestSuite/InitTransport.h"

#include "msg/Provider.h"
#include "msg/BufferManager.h"
#include "msg/DefaultBufferManager.h"
#include "msg/Port.h"
#include "msgtcp/TCPProvider.h"
#include "msgudp/UDPProvider.h"
// #include "msgrawsock/rawsockprov.h"

// create the Node structure for a TCP node

using namespace std;

std::string d_transport;

static MessagePassing::Node make_node(MessagePassing::NodeID id, string host)
{
  using namespace MessagePassing;
  
  std::string s = d_transport;
  s+= ":";
  s+= host;
  
  Address addr(s);
  AddressList addr_list;
  addr_list.push_back(addr);
  
  Node  node(id, "default", addr_list);
  
  return node;
}

// initialize nodes for TCP/UDP/RAWSock transport

//bool init_tran(string& c_tran,MessagePassing::NodeID id, MessagePassing::NodeID id_step, vector<string>& hosts)
bool init_tran(string& c_tran, multimap<string,int>& hosts,unsigned int *dfpoint)
{
  using namespace MessagePassing;
  using namespace std;
  BufferManager *tmpBfM = new DefaultBufferManager;
  
  dfpoint = NULL; // empty/not used
  d_transport = c_tran;
  // initialize buffer manager
  BufferManager::set_manager(tmpBfM);
  
  int cpos;
  if( (cpos = c_tran.find("tcp")) >= 0 ) {
    // initialize TCP provider and register it
    TCPProvider *provider = new TCPProvider();
    //Provider::register_provider("tcp", provider);
    Provider::register_provider(provider);
  }
  else {
    if( (cpos = c_tran.find("udp")) >= 0 ) {
      // initialize UDP provider and regiser it
      UDPProvider *provider = new UDPProvider();
      //Provider::register_provider("udp", provider);
      Provider::register_provider(provider);
    }
    /*else {
      if( (cpos = c_tran.find("rawsock")) >= 0 ) {
      // initialize RAWSOCKET provider and register it
	RawSockProv *provider = new RawSockProv();
	Provider::register_provider("rawsock", provider);
	// delete provider;
      }
      else {
	std::cerr<<"\nError:Unknown transport, program terminated !\n\n";
	exit(1);
      }
      }*/
  }
  
  // initialize Nodes
  multimap<std::string,int>:: iterator irhn = hosts.begin();
  MessagePassing::NodeID id = irhn->second;
  MessagePassing::NodeID local_id = id & (1<<ROS_SHIFT); //local_id should be ROS

  /* Find a local node and put it first on the list */
  MessagePassing::NodeList nodes; Node node;
  for( irhn = hosts.begin(); irhn != hosts.end(); irhn++ ) {
    if( irhn->second == (1<<ROS_SHIFT) ) {
      std::cout<<"\nFound local node: "<<irhn->first<<"\n";
      node = make_node(local_id,irhn->first);
    }
    else {
      std::cout<<"Adding host "<<irhn->first<<"\n";
      node = make_node(irhn->second, irhn->first);
    }
    nodes.push_back(node);
  }

  // configure message passing - null group list
  if(!Provider::configure_all(local_id, nodes, GroupList())) {
    return false;
  }
    
  for(NodeList::iterator it = nodes.begin(); it != nodes.end(); ++it) {
    // create ports to all remote nodes
    if((*it).id() != local_id) {
      cout << "Creating Port " << (*it).id() << std::endl;
      Port *port = Port::create(*it);
      if(port == 0) {
	std::cerr << "Error:Could not create Port for node " 
		  << (*it).id() << std::endl;
	exit(EXIT_FAILURE);
      }
    }
  }
  std::cout<<"transport initialised\n";

  return true;
}

