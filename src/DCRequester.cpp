#include <stdio.h>
#include <fstream>
#if (__GNUC__ > 2) || (__GNUC_MINOR__ > 95)
#include <sstream>
#else
#include <strstream>
#define istringstream istrstream
#define ostringstream ostrstream
#endif

#include <iostream>
#include <string.h>
#include <errno.h>

#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <list>
#include <vector>
#include <string>
#include <iomanip>

#include "msg/BufferManager.h"
#include "RobinTestSuite/DCRequester.h"
#include "RobinTestSuite/DCMsgHeaders.h"

template <class T> void getval(string key, T& conval ) {
#if (__GNUC__ > 2) || (__GNUC_MINOR__ > 95)
  istringstream ival(key);
#else
  istringstream ival(key.c_str());
#endif
  if( key.find("0x") == 0 || key.find("x") == 0 ) 
    ival.setf(ios::hex,ios::basefield);
  ival>>conval;
}

using namespace std;

int timeoutflag = 0;
long int fullom = 0;
unsigned int tdiv;
unsigned int Lastrequested;
short thrend = 0;
FILE *fdataout;
unsigned int NRols;
u_int max_ROLcount;
int DelNode;
int ExpectedNIds,RcvNIds;

typedef struct {
  unsigned int req_ids;
  int count;
} rqsids_t;

rqsids_t *rqrcv_ids;

vector<unsigned int> not_found_ids;

unsigned int* GenRequest(unsigned int csize) {
  static unsigned int msg[Max_msgsize];

  //std::cout<<"\nGenerating new message";
   
  //initialise message header. It stays the same for each sent message
  trg_msg  gen_req;
  gen_req.msg_header.magic_word = MESSAGE_MAGIC_WORD;
  gen_req.msg_header.mes_type_id = L2PU_Data_Request;
  gen_req.msg_header.sender_id = (1<<15) + 1;
  gen_req.msg_header.transaction_id = 1;
  gen_req.msg_header.timestemp = TimeStamp;

  gen_req.msg_header.mlength =  sizeof(g_header) + csize;
  std::cout<<"\nmlength = "<<gen_req.msg_header.mlength<<"\n";
  
  unsigned int *dp = new unsigned int[Max_msgsize];
  if( csize > 0 ) {
    //create message proper
    if( csize > (Max_msgsize-sizeof(g_header)/sizeof(int)) )
      csize = Max_msgsize - sizeof(g_header)/sizeof(int);
    dp[0] = firstId;
    int idp = 1;
    for( u_int inode = 0; inode < NROLS; inode++ )
      if( selectedNodes[inode] > 0 )
	dp[idp++] = inode;
    
    unsigned int *pmsg = msg;
    memcpy(pmsg,&gen_req.msg_header,sizeof(g_header));
    pmsg+= sizeof(g_header)/sizeof(int);
    memcpy(pmsg,dp,csize*sizeof(int));
  }

  delete [] dp;
  return(msg); 
}

int main(int argc, char *argv[])
{
  cout<<Ver<<"\n";
  string conf_name;
  if( argc > 1 )
    conf_name = argv[1];
  else
    conf_name = "dcrequester.conf";
  in_par(conf_name); 
      
  unsigned int dfpoint[5];
  connect(dfpoint);

  cout<<"Header size: "<<sizeof(FragHdr)<<"\n";
  /* signals to catch */
  void (*fp)(int) = &catcher;
  signal(SIGINT,fp);
  signal(SIGBUS,fp);
  signal(SIGSEGV,fp);

  crc_init();

  min_size = 9999999; max_size = 0;
  max_ev_id = empty_msg = not_found = 0;

  unsigned int mes_size = (1 + NumOfSelNodes)*sizeof(int);
  req_add = GenRequest(mes_size); 

  m_msg_snd_size = sizeof(g_header) + mes_size;
  pg_header = (g_header*)req_add;
  std::cout<<"\n\nHeader size: "<<sizeof(g_header)<<"\nRequested length: "<<req_length<<"\n";
  rodErrData = NULL;
  
  //prepare DFM_Clear header and empty delete list
  /* del add: general headre at the beginning, followed by the event list */
  del_add = new unsigned int[1+del_group+sizeof(g_header)/sizeof(int)];
  memcpy(del_add,req_add,sizeof(g_header));
  ((g_header*)del_add)->mes_type_id = DFM_Clear;
  ((g_header*)del_add)->mlength =  sizeof(g_header) + (del_group + 1)*sizeof(int);
  cerr<<"Delete message length: "<<((g_header*)del_add)->mlength<<"\n";
  ((g_header*)del_add)->sender_id = DFM; /*<<15;*/ /*ROSapp;*/
  del_add[sizeof(g_header)/sizeof(int)] = 0xffffffff; //del_group;

  total_received = nloop = 0;
  total_sent = 0;
  m_invalidmsg = m_rosmark = m_robmark = m_rodmark = m_invalidCRC = 0;
  msg_sent = msg_rcv = del_sent = 0;

  pg_header->req_length = req_length;

  // initialise event id: first event Id:
  req_add[(sizeof(g_header)/sizeof(int))] = firstId;

  startOfRun = dcclock.time();

  rcv_cancel_count = snd_cancel_count = 0;
  //start "receive" thread:
  if( pthread_create(&rcv_thr,NULL,rcv_dcmsg,NULL) ) {
    perror("rcv thread: ");
    return -1;
  }
  if( pthread_detach(rcv_thr) != 0 ) {
     perror("detach: ");
  }
  /* wait for the rcv thread to start */
  pthread_mutex_lock(&rcv_lock);
  pthread_cond_wait(&rcv_started, &rcv_lock);
  pthread_mutex_unlock(&rcv_lock);
  
  //start "send" thread:
  if( pthread_create(&snd_thr,NULL,snd_dcreq,NULL) ) {
    perror("rcv thread: ");
    return -1;
  }
  if( pthread_detach(snd_thr) != 0 ) {
    perror("detach: ");
  }
  
  int fracold = -1; short leftw = 0;
  m_timeElapsed_main = dcclock.time() - startOfRun;
  while( m_timeElapsed_main.as_seconds() < maxRunTime && !endoftime ) {

    if( NumberOfEvents != 0 && msg_rcv >= NumberOfEvents ) {
      endoftime = 1;
      break;
    }

    int fracdone = static_cast<int>(100*m_timeElapsed_main.as_seconds()/maxRunTime);
    if( fracdone != fracold && !(fracdone%tdiv) ) {
      fracold = fracdone;
      if( leftw ) {
	fprintf(stderr,"  . ");
	leftw = 0;
      }
      else {
	fprintf(stderr,"   .");
	leftw = 1;
      }
      fprintf(stderr,"%d\r",fracdone);
      //std::cout<<fracdone<<" \r"<<flush;
    }

    m_timeElapsed_main = dcclock.time() - startOfRun;
    sched_yield();
  }
  endoftime = 1;

  fprintf(stderr,"stopping all threads\n");
  //cancel snd  and rcv threads
  fprintf(stderr,"INFO:waiting for \"Send\" \n");

  pthread_mutex_lock(&snd_lock);
  fprintf(stderr,"snd lock acquired \n");
  /* wait until thread has quitted */
  while( snd_cancel_count == 0 ) {
    sched_yield();
  }
  pthread_mutex_unlock(&snd_lock);
  fprintf(stderr,"INFO: \"Send\" thread finished\n");
    
  if( pthread_cancel(rcv_thr) )
     perror("rcv cancel error ");
  
  while( rcv_cancel_count == 0 ) {
    sched_yield();
  }
  
  fprintf(stderr,"INFO: \"Receive\" thread finished\n");

  fprintf(stderr,"INFO: end of program\n");

  m_timeElapsed_main = dcclock.time() - startOfRun;
  get_info();

  delete_globals();

  return 0;
}

void in_par(string conFile) {

  ifstream configFile(conFile.c_str());
  if( !configFile ) {
    std::cerr<<"Error: cannot open "<<twoseven<<"[7m"<<conFile<<twoseven<<"[0m"<<" exit now\n";
    exit(-1);
  }
  std::cout<<"\nUsing "<<twoseven<<"[7m"<<conFile<<twoseven<<"[0m";
  std::cout<<" to read config parameters\n"; 

  const int lineSize = 1024;
  char buf[lineSize];
  for( u_int irol = 0; irol < NROLS; selectedNodes[irol++] = 0 ){};

  map<string,string> local_config;
  int rnode;
  int rols = 0;
  while(configFile.getline(buf, lineSize)) {
    if (buf[0] == '#') { // skip comment lines
      continue;
    }
    
    string line(buf);
    int equalsPosition = line.find_first_not_of(' ');
    line.erase(0, equalsPosition);

    int shiftval;
    if( (equalsPosition = line.find("localhost")) >= 0 ) 
      shiftval = strlen("localhost");
    else {
      equalsPosition = line.find("remote");
      shiftval = strlen("remote");
    }

    if( equalsPosition >= 0 ) {  
      string host(line, equalsPosition+shiftval);
      equalsPosition = host.find_first_not_of(' ');
      host.erase(0, equalsPosition);
      equalsPosition = host.find(' ');

      string key(host,0,equalsPosition);
      host.erase(0,equalsPosition+1);
      local_config[key] = host;
      getval(local_config[key],rnode);

      //int rawbcast = key.find("01:"); int udpbcast = key.find("255");
      //if( ((rawbcast != 0 && udpbcast < 0) ) { 
      if( rnode == 0 /*|| rnode > 10*/ ) { //here rnode == 0 denotes a "local" node, node 1-10 reserved for "delete" ports	
	rnode+= (1<<ROS_SHIFT);  // add ROS node id to the remote node
      }
      else
	rols++;

      DelNode = rnode;
      RhostsNode.insert(make_pair(key,rnode));
    }
    else {
      equalsPosition = line.find(' ');
      string key(line, 0, equalsPosition);
      line.erase(0,equalsPosition+1);
      local_config[key] = line;
    }
  }

  multimap<string,int>:: iterator irhn;
  std::cout<<"\nFrom conf file: \n";
  for( irhn = RhostsNode.begin(); irhn != RhostsNode.end(); irhn++ )
    std::cout<<irhn->first<<" "<<irhn->second<<"\n";

  //transport type
  getval(local_config["transport"],trafic_type);
  //cerr<<"transport: "<<trafic_type<<"\n";

  //requested size
  getval(local_config["requestsize"],req_length); 
  //cerr<<"requestsize: "<<req_length<<"\n";
  
  //ROL will be derived from remote_node
  getval(local_config["remotenode"],remote_node);

  //datacheck
  getval(local_config["datacheck"],datacheckloop);

  //debugloop
  getval(local_config["debugloop"],debugloop);

  firstId = 1;
  getval(local_config["firstId"],firstId);

  //maxruntime
  getval(local_config["maxruntime"],maxRunTime);
  if( maxRunTime >= 300 ) 
    tdiv = 2;
  else
    tdiv = 10;
  
  //throttle
  getval(local_config["throttle"],throttle);

  //group delete
  getval(local_config["delgroup"],del_group);
  rqrcv_ids = new rqsids_t[del_group];

  //ROD data dumped in hex or dec format
  getval(local_config["hexdump"],hexordec);

  getval(local_config["datadumpto"],filedata);

  getval(local_config["NumberOfEvents"],NumberOfEvents);

  //requested ratio of events
  getval(local_config["reqrat"],reqrat);
  if( reqrat < 1 || reqrat > 100  )
    reqrat = 100;

  //performance run
  getval(local_config["perf"],perf);

  //if resetid == 1, event id will be reset to 1 after sending del msg. 
  //On the Robin side, KeepFrags must be set to 1
  getval(local_config["resetid"],resetid);

  getval(local_config["logfile"],logname);
  time_t newmesDue = 600;
  getval(local_config["tstamp"],newmesDue);

  getval(local_config["single"],single);
  getval(local_config["eventloop"],MAX_EV_ID);

  // Check if there are specific Rols selected
  getval(local_config["NodeROL0"],selectedNodes[0]);
  getval(local_config["NodeROL1"],selectedNodes[1]);
  getval(local_config["NodeROL2"],selectedNodes[2]);
  NumOfSelNodes = 0;
  for( u_int inode = 0; inode < NROLS; inode++ )
    if( selectedNodes[inode] > 0 ) 
      NumOfSelNodes++;

  if( NumOfSelNodes == 0 ) 
    NRols = max_ROLcount = 3;
  else
    NRols = max_ROLcount = NumOfSelNodes;
  cerr<<"Max Rol count: "<<max_ROLcount<<"\n";

  getval(local_config["timeout"],rqtimeout);
  if( rqtimeout < 1 ) 
    rqtimeout = 2; //default value

  time_t ltime;
  if( logname.size() > 0 ) {
    std::cout<<"logfile: "<<logname<<"\n";
    ofstream log(logname.c_str(), ios:: app );
    if( !log )
      std::cerr<<"Error: \nCannot open logfile, statistics only on the screen\n";
    else {
      short tstamp = 1;
      time(&ltime);
      struct stat  logstat;
      if( stat(logname.c_str(),&logstat) > -1 ) {
	time_t dtime = ltime - logstat.st_mtime;
	if( !newmesDue && dtime > 0 ) 
	  tstamp = 0;
      }

      std::cout<<"tstamp: "<<tstamp<<" new measurement"<<newmesDue<<"\n";
      if( tstamp ) {
	log<<Ver<<"\n\n";
	log<<"\n"<<ctime(&ltime)<<"\n"<<NRols<<" ROL(s)\n";
	log<<"OM\tEB\tData\tLvL1\treqrate\ttranrate\tidle\tdata\n";
	log<<" \t[%]\t[bytes]\t[kHz]\t[kHz]\t[bytes/s]\t[%]\t[words]\n";
	log<<"-------------------------------------------------------------------------\n";
      }
      log.close();
    }
  }

  rodDataDump = NULL;
  if( filedata.size() > 0 ) {
    if( (fdataout = fopen(filedata.c_str(),"w")) == NULL ) {
      perror("fdataout: ");
    }
    /*if( hexordec ) 
      rodDataDump = new ofstream(filedata.c_str(),ios::app);
    else 
      rodDataDump = new ofstream(filedata.c_str(), ios::binary|ios::app);
 
    if( !*rodDataDump ) {
      std::cerr<<"Error: \nCannot open Rod Data dump file, Rod data only on the screen\n";
      std::cerr.setf(ios::hex,ios::basefield);
      }*/
    else {
      if( hexordec ) {
	/*(*rodDataDump)<<Ver<<"\n\n";
	(*rodDataDump)<<"\n"<<ctime(&ltime)<<"\n"<<(rols-1)<<" ROL(s)\n";
	rodDataDump->setf(ios::hex,ios::basefield);*/
	fprintf(fdataout,"%s\n\n%s\n%d ROL(s)\n",Ver,ctime(&ltime),(rols-1));
      }
      /*else {
	for( unsigned int it = 0; it < 10000; it++ ) 
	  rodDataDump->write(reinterpret_cast<char *>(&it),sizeof(int));
	rodDataDump->close();
	}*/
    }
  }
  ExpectedNIds = ((reqrat*del_group)/100)*max_ROLcount;
  RcvNIds = 0;
}

void connect(unsigned int *dfpoint) {
  using namespace MessagePassing;

  std::cout<<"Traffic type: "<<trafic_type<<"\n";
  init_tran(trafic_type,RhostsNode,dfpoint);

  u_int NremHosts = RhostsNode.size() - 1;
  std::cout<<"\nNumber of all remote nodes "<<NremHosts<<":\n";

  
  multimap<string,int>:: iterator irhn; 
  std::cerr.setf(ios::hex,ios::basefield);
  std::cout<<"del node: "<<DelNode<<"\n";
  for( irhn = RhostsNode.begin(); irhn != RhostsNode.end(); irhn++ ) {
    if( irhn->second != (1<<ROS_SHIFT) ) {
      Port *port = Port::find(irhn->second);
      if( port == 0 ) 
	std::cerr<<"Error: Couldn't find port for host "<<irhn->first<<" node 0x"<<irhn->second<<"\n";
      else {
	int rawbcast = irhn->first.find("01:"); int udpbcast = irhn->first.find("255");
	if( rawbcast == 0 || udpbcast >= 0 ||  irhn->second == DelNode /*irhn->second < (1<<ROS_SHIFT)*/ ){
	  deletehosts.push_back(port);
	  std::cout<<"Delete node "<<irhn->first<<" port "<<port<<"\n";
	  port_del = port; //Delete node 
	}
	else {
	  std::cout<<"host "<<irhn->first<<" node 0x"<<irhn->second<<" port 0x"<<port<<"\n";
	  remotehosts.push_back(port);
	}
      }
    }
  }
  std::cerr.setf(ios::dec,ios::basefield);

  if( (m_NumDelHosts = deletehosts.size()) == 0  ) {
    std::cout<<"\n\t"<<twoseven<<"[103m"<<"Warning ! No multicast delete host found, ";
    std::cout<<" using remote node to send delete"<<twoseven<<"[0m"<<"\n";
  }

  if( (m_NumRemHosts = remotehosts.size()) == 0 ) {
    std::cerr<<"Error: \n\t\t"<<twoseven<<"[103m"<<"------->Warning ! No remote hosts found<---------"<<twoseven<<"[0m"<<"\t\t\n";
    if( port_del == 0 ) {
      std::cerr<<"Error: \n\t\t"<<twoseven<<"[101m"<<"------->Error ! No delete port, abord the program"<<twoseven<<"[0m"<<"\t\t\n";
      exit(1);
    }      
  }
  cerr<<"m_NumRemHost: "<<m_NumRemHosts<<"\n";
}

void *snd_dcreq(void *pparam) {
  using namespace MessagePassing;

  unsigned int del_id = 0;
  unsigned int id_shift = sizeof(g_header)/sizeof(int) + 1;
  unsigned int del_buf_size = (((g_header*)del_add)->mlength);
  unsigned int req_dat_size = (((g_header*)req_add)->mlength);
  unsigned int max_del_index = del_group + sizeof(g_header)/sizeof(int) + 1;
  unsigned int max_snd = (reqrat*del_group)/100; 
  Time start_wait,delmsg_start,elapsed_Wait,elapsed_yield;
  struct timeval ltstart,ltstop;
  struct timezone ltz;

  int last_state,last_type;
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &last_state);
  pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED /*PTHREAD_CANCEL_ASYNCHRONOUS*/, &last_type);
  pthread_cleanup_push(snd_cleanup,NULL);

  u_int evIdpos = sizeof(g_header)/sizeof(int);
  u_int saved_id;
  /*int dmsg;*/
  u_int NmissingIds = 0;
  ((g_header*)del_add)->transaction_id = 1;
  while( !endoftime ) {
    //Prepare delete message, otherwise L2 request
    
    if( del_id == max_snd ) {
      //((g_header*)del_add)->transaction_id = ((g_header*)req_add)->transaction_id;
      
      for( ; del_id < del_group; del_id++ ) {
	if( (del_id+id_shift) < max_del_index ) 
	  del_add[del_id+id_shift] = req_add[evIdpos]++; 
	else
	  std::cerr<<"del index too big: "<<(del_id+id_shift)<<" max : "<<max_del_index<<"\n";
      }
      saved_id = req_add[evIdpos];
      del_add[id_shift-1] = 0xffffffff; /*req_add[evIdpos]-1;*/
      
      start_wait = dcclock.time();
      
      NmissingIds = 0;
      /*for( u_int ircv = 0; ircv < ((reqrat*del_group)/100); ircv++ ) {
	if( rqrcv_ids[ircv].count < (int)max_ROLcount && rqrcv_ids[ircv].req_ids != 0 ) {
	  NmissingIds+= max_ROLcount - rqrcv_ids[ircv].count;
	}
	}*/
      pthread_mutex_lock(&rcvIds);
      if( RcvNIds < ExpectedNIds ) 
	NmissingIds = ExpectedNIds - RcvNIds;
      pthread_mutex_unlock(&rcvIds);

      u_int to_reRequest = 0;
      while( NmissingIds > 0 /*(dmsg = static_cast<unsigned int>(msg_sent - msg_rcv)) > 0*/ && !endoftime ) {
	gettimeofday(&ltstart,&ltz);
	delmsg_start = dcclock.time();

	sched_yield();
	elapsed_yield = dcclock.time() - start_wait;

	/* re-check */
	NmissingIds = 0;	
	pthread_mutex_lock(&rcvIds);
	if( RcvNIds < ExpectedNIds ) 
	  NmissingIds = ExpectedNIds - RcvNIds;
	pthread_mutex_unlock(&rcvIds);

	if( NmissingIds == 0 ) 
	  break;

	if( /*elapsed_yield.as_seconds() > 1.0*/ elapsed_yield.as_microseconds() > 200000 ) {
	  fprintf(stderr,"\nWarning: \n\t\t%c%sRe-requesting %d%c%s ids\n",twoseven,"[101m",NmissingIds/*dmsg*/,twoseven,"[0m");
	  
	  /*fprintf(stderr,"missing ids: ");*/
	  for( u_int ircv = 0; ircv < ((reqrat*del_group)/100); ircv++ ) {
	    if( rqrcv_ids[ircv].count < (int)max_ROLcount && rqrcv_ids[ircv].req_ids != 0 ) {
	      fprintf(stderr," %d: %d,",rqrcv_ids[ircv].req_ids,rqrcv_ids[ircv].count );
	      to_reRequest = 1;
	      for( u_int ireq = 0; ireq < (max_ROLcount - rqrcv_ids[ircv].count); ireq++ ) {
		if( ireq < m_NumRemHosts ) {
		  req_add[(sizeof(g_header)/sizeof(int))] = rqrcv_ids[ircv].req_ids;
		
		  Buffer *buf = new Buffer(req_dat_size);
		  Buffer::iterator ins = buf->begin();
		  ins.insert(req_add,req_dat_size);
		  buf->size(req_dat_size);
		  if( !(remotehosts[ireq]->send(buf)) ) {
		    std::cerr<<"Error sending data !!\n";
		    endoftime = 1;
		  }
		  nre_requested++;
		}
	      }
	    }
	  }
	  sched_yield();
	  start_wait = dcclock.time();
	}
	
	gettimeofday(&ltstop,&ltz);
	del_idle+= 1.e+6*(ltstop.tv_sec - ltstart.tv_sec) + (ltstop.tv_usec - ltstart.tv_usec);

	/*if( nre_requested >= (unsigned int)dmsg ) 
	  break;*/
      }
      if( to_reRequest == 1 )
	req_add[evIdpos] = saved_id;
      
      if( port_del != NULL ) { //multicast delete
	//std::cerr<<" del id:"<<((g_header*)del_add)->transaction_id<<"\n";
	for( unsigned int iport = 0; iport < m_NumDelHosts; iport++ ) {
	  Buffer *buf = new Buffer(del_buf_size);
	  Buffer::iterator ins = buf->begin();
	  ins.insert(del_add,del_buf_size);
	  buf->size(del_buf_size);
	  if( !(deletehosts[iport]->send(buf)) ) {
	    std::cerr<<"Error sending delete request !!\n";
	    exit(1);
	  }
	  del_sent++; 
	  ((g_header*)del_add)->transaction_id++;
	}
      }
      else { //unicast delete
	for( unsigned int iport = 0; iport < m_NumRemHosts; iport++ ) {
	  Buffer *buf = new Buffer(del_buf_size);
	  Buffer::iterator ins = buf->begin();
	  ins.insert(del_add,del_buf_size);
	  buf->size(del_buf_size);
	  if( !(remotehosts[iport]->send(buf)) ) {
	    std::cerr<<"Error sending delete request !!\n";
	    exit(1);
	    //return;
	  }
	  else {
	    del_sent++; 
	    ((g_header*)del_add)->transaction_id++;
	  }
	}
      }
      //cerr<<"deleted from "<<del_add[id_shift]<<" to "<<del_add[id_shift+del_group-1]<<"\n";
      
      unsigned int last_deleted;
      if( (last_deleted = del_add[id_shift+del_group-1]) >= MAX_EV_ID )
	last_deleted = 1;
      del_add[sizeof(g_header)/sizeof(int)] = last_deleted+1; // oldest, still valid LvL1 id
      // cerr<<"Last deleted: "<<last_deleted<<"\n";

      //resets event ID: valid only if on the Robin side KeepFrag=1
      if( resetid ) {
	req_add[evIdpos] = 1;
	last_deleted = 0;
      }

      del_id = 0;
      RcvNIds = 0;
      /* end of delete processing */
    }
    else { //Data request
      
      Lastrequested = req_add[evIdpos];
      unsigned int it = Lastrequested - del_group*((Lastrequested-1)/del_group) -1;
      if( it < del_group ) {
	rqrcv_ids[it].req_ids = Lastrequested;
	rqrcv_ids[it].count = 0;
      }
      else 
	std::cerr<<"rev_id out of bound: "<<it<<", maximum is "<<del_group<<"\n";

      for( unsigned int iport = 0; iport < m_NumRemHosts; iport++ ) {
	//Wait if there are two many outstanding messages
	if( (msg_sent - msg_rcv) >= throttle ) {
	  fullom++;
	  Time elapsed_yield;
	  
	  start_wait = dcclock.time();
	  gettimeofday(&ltstart,&ltz);
	  while( (msg_sent - msg_rcv) > throttle && !timeoutflag ) {
	    elapsed_yield = dcclock.time() - start_wait;
	    if( elapsed_yield.as_seconds() > rqtimeout /*&& elapsed_yield.as_seconds() < 10*/ ) {
	      std::cerr<<"Error: \n\t\t"<<twoseven<<"[101m"<<"---> Time out, program aborted<----"<<twoseven<<"[0m"<<"\t\t\n";
	      for( u_int iev = 0; iev < del_group; iev++ ) {
		if( rqrcv_ids[it].req_ids > 0 && rqrcv_ids[it].count < (int)max_ROLcount ) 
		  std::cerr<<"Not received "<<rqrcv_ids[iev].req_ids<<" count "<<rqrcv_ids[iev].count<<"\n";
	      }
	      std::cerr<<"\nwait count = "<<elapsed_yield.as_seconds()<<" seconds\n";
	      timeoutflag = 1;
	      //get_info();
	      endoftime = 1;
	      break;
	      //return NULL;
	    }
	    sched_yield();
	  }
	  gettimeofday(&ltstop,&ltz);
	  idle+= 1.e+6*(ltstop.tv_sec - ltstart.tv_sec) + (ltstop.tv_usec - ltstart.tv_usec);
	}
	if( timeoutflag || endoftime ) 
	  //return NULL;
	  break;
	
	// send message 
	Buffer *buf = new Buffer(req_dat_size);
	Buffer::iterator ins = buf->begin();
	ins.insert(req_add,req_dat_size);
	//std::cerr<<"tran id:"<<((g_header*)req_add)->transaction_id<<"\n";
	buf->size(req_dat_size);
	if( !(remotehosts[iport]->send(buf)) ) {
	  std::cerr<<"Error sending data !!\n";
	  endoftime = 1;
	  //exit(1);
	  //return;
	}
	msg_sent++;
      }
      
      //prepare next data request message
      //put an event id on the delete list, increment event id
      if( req_add[(sizeof(g_header)/sizeof(int))] >=  MAX_EV_ID ) {

	del_add[del_id+id_shift] = req_add[evIdpos];
	req_add[evIdpos] = 1;	
      }
      else {
	del_add[del_id+id_shift] = req_add[evIdpos]++;
      }
      
      del_id++;
      total_sent+= (m_NumRemHosts*req_dat_size);
    }
    
    //inrement transaction id 
    if( pg_header->transaction_id < MAX_TRAN_ID )
      ((g_header*)req_add)->transaction_id++;
      // pg_header->transaction_id++;
    else
      ((g_header*)req_add)->transaction_id = 1;
      // pg_header->transaction_id = 1;
    
    
    ++nloop;
    
    if( single == 1 ) {
      endoftime = 1; 
      std::cerr<<"single shot\n";
    }
    pthread_testcancel();
  }
  
  /*pthread_mutex_lock(&snd_lock);*/
  snd_cancel_count++;
  /*pthread_cond_signal(&snd_done);
    pthread_mutex_unlock(&snd_lock);*/

  pthread_cleanup_pop(0);

  pparam = pparam; //unused
  return NULL;
}

void *rcv_dcmsg(void *pparam) {
  using namespace MessagePassing;
  using namespace MessageInput;
  short checkActive;

  int last_state,last_type;
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &last_state);
  pthread_setcanceltype(/*PTHREAD_CANCEL_DEFERRED*/ PTHREAD_CANCEL_ASYNCHRONOUS, &last_type);

  pthread_cleanup_push(rcv_cleanup,&checkActive);

  pthread_mutex_lock(&rcv_lock);
  pthread_cond_signal(&rcv_started);
  pthread_mutex_unlock(&rcv_lock);
  short not_avail = 0;

  unsigned int rcv_packs = 0;
  int stat;
  Buffer *rcv_buf;  
  u_int rcv_pack_size,processed_size,currentEvSize,ev_pos;
  RobinRodHdr_t evfrag_hdr; 

  while( !endoftime ) {
    rcv_buf = Port::receive();

    if( rcv_buf == NULL ) {
      std::cerr<<"\nError: receive() failed";
    }
    else {
      msg_rcv++; rcv_packs++;
      total_received+= rcv_pack_size = rcv_buf->size();

      if( rcv_pack_size < min_size ) 
	min_size = rcv_pack_size;
      if( rcv_pack_size > max_size )
	max_size = rcv_pack_size;

      if( rcv_packs > 0 && !(rcv_packs%datacheckloop)  ) {
	checked_pac++;
	//check message header(s)
	if( (stat = check_gh(rcv_buf) == 1 ) || RodDataCheck(rcv_buf) > 0 ) {
	  m_invalidmsg++;
	  if( stat == 1 ) //entire message is invalid, don't bother with headers
	    continue; 
	}
      }

      //cout<<nloop<<" "<<debugloop<<"\n";
      if( rcv_packs > 0 && !(rcv_packs%debugloop) )  //dump received packet
	dump_fullevent(rcv_buf,0);
      
      Buffer::iterator ins = rcv_buf->begin();
      
      g_header gen_hdr; ins>>gen_hdr;
      processed_size = sizeof(g_header);

      unsigned iev_frag = 0;
      while( processed_size < rcv_pack_size ) {
	ins>>evfrag_hdr;
	iev_frag++;

	processed_size+= currentEvSize = 4*evfrag_hdr.RobHrd.totalSize;
	if( currentEvSize > (rcv_pack_size - sizeof(g_header)) ) {
	  if( rcv_packs > 0 && !(rcv_packs%datacheckloop) ) {
	    fprintf(stderr,"Wrong event fragment size (read from ROB hdr): 0x%x, event fragment %d\n",evfrag_hdr.RobHrd.totalSize,iev_frag);
	    dump_fullevent(rcv_buf,3);
	  }
	  break;
	}
	    
	if( currentEvSize == (sizeof(RobinRodHdr_t) + TrailesSize) ) {
	  empty_msg++;
	  //std::cerr<<"Not available "<<FragHdr.evId<<" Last requested: "<<Lastrequested<<"\n";
	  fprintf(stderr,"Not available %d Last requested: %d\n",evfrag_hdr.evId,Lastrequested);
	  dump_fullevent(rcv_buf,1);
	  not_avail = 1;
	}
	else
	  not_avail = 0;

	if( evfrag_hdr.RobHrd.fragStat == 0x90000004 ) {
	  if( not_found++ < 500 ) {
	    // not_found_ids.push_back(evfrag_hdr.RobHrd.evId);
	    //not_found_ids.push_back(evfrag_hdr.RobHrd.RosHrd.l1Id);
	  }
	}

	//ids start from 1 not 0, -1 shift     
	ev_pos =  evfrag_hdr.evId - del_group*((evfrag_hdr.evId-1)/del_group) -1;
	if( ev_pos >= del_group )
	  std::cerr<<"Event positionout out of bound: "<<ev_pos<<" event Id: "<<evfrag_hdr.evId<<"\n"; 
	if( !perf && evfrag_hdr.RobHrd.fragStat != 0x90000004 && !not_avail ) {
	  //cout<<"Current event ID "<<FragHdr.evId<<" position "<<ev_pos;
	  if( ev_pos < del_group ) {
	    
	    if( evfrag_hdr.evId == rqrcv_ids[ev_pos].req_ids ) {
	      pthread_mutex_lock(&rcvIds); 
	      RcvNIds++;
	      pthread_mutex_unlock(&rcvIds); 
	      rqrcv_ids[ev_pos].count++;
	    }
	    else {
	      cerr<<"\nWarning: Event id mismatch, expected "<<rqrcv_ids[ev_pos].req_ids<<" got "<<evfrag_hdr.evId<<" it: "<<ev_pos<<"\n";
	      if( ev_pos < (del_group-1) )
		cerr<<"Next event expected "<<rqrcv_ids[ev_pos+1].req_ids<<"\n";
	      else
		cerr<<"First from the group "<<rqrcv_ids[0].req_ids<<"\n";
	      cerr<<"Last requested "<<Lastrequested<<"\n";
	    }
	  }
	  else {
	    cerr<<"\nWarning: Invalid current event Id\n";
	  }
	}
	
	// more events in the packet. Read till the end of the current event    
	// u_int dummy[2];
	//u_int ev_size = sizeof(RobinRodHdr_t);
	  
	// read the rest of the event - cannot jump, it is DC buffer, not simple memory
	u_int remaining_size = currentEvSize - sizeof(evfrag_hdr);
	if( remaining_size  > 0 )
	  ins+= remaining_size;
	  /*while(ev_size < currentEvSize ){
	  ins>>dummy;
	  ev_size+=2*sizeof(u_int);
	  }*/
      }

      /*pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &last_state);*/      
      /* pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &last_state);*/
            
      delete rcv_buf;
    }
    //pthread_testcancel();
  }

  /*pthread_mutex_lock(&rcv_lock);*/
  rcv_cancel_count++;
  fprintf(stderr,"End of rcv\n");
  /*pthread_cond_signal(&rcv_done);
    pthread_mutex_unlock(&rcv_lock);*/

  pthread_cleanup_pop(0);

  pparam = pparam; //unused
  return NULL;
}

int check_gh(Buffer* rcv_buf) {
  using namespace MessageInput;

  MessageHeader hdr(rcv_buf);
  if( !hdr.valid() ) {
    std::cerr<<"Error: \nMessage header: invalid MagicNumber\n";
    return 1;
  }
  
  if( hdr.timestamp()!= TimeStamp ) {
    std::cerr<<"Error: \nMessage header: invalid TimeStamp \n";
    return 1;
  }
  
  if( hdr.xid() > pg_header->transaction_id ) {
    std::cerr<<"Error: \nMessage header: xid in the future \n";
    return 1;
  }  

  return 0;
}

int check_robh(Buffer* rcv_buf) {
  MessagePassing::Buffer::iterator idc = rcv_buf->begin();
  unsigned int tot_extr = sizeof(RobMsgHdr) + sizeof(g_header);
  unsigned int *hdr = new unsigned int[tot_extr];
  idc.extract(hdr,tot_extr);

  //jump to the Rob header
  hdr = hdr + sizeof(g_header)/sizeof(int);
  
  RobMsgHdr *rob_hdr = (RobMsgHdr*)(hdr);
  if( rob_hdr->version != s_formatVersionNumber ) {
    std::cerr<<"Error: ROB header: wrong version number\n";
    delete [] hdr;
    return 1;
  }
  
  delete [] hdr;
  return 0;
}

void add2str(EthSoc* ethinf) {
  char tmp_dump[5];
  unsigned char *addr;
  int i;

  addr = (unsigned char*)&(ethinf->loc_eth);
  if( *addr < 10 )
    sprintf(ethinf->str_add,"0%X",(int)*addr++);
  else
    sprintf(ethinf->str_add,"%X",(int)*addr++);
  for( i = 1; i < 6; i++ ) {
    if( *addr < 10 ) 
      sprintf(tmp_dump,":0%X",(int)*addr++); 
    else 
      sprintf(tmp_dump,":%X",(int)*addr++); 
    strcat(ethinf->str_add,tmp_dump);
  }
}

void get_info() {

  std::cout.setf(ios::dec,ios::basefield);

  std::cout<<twoseven<<"[5m";
  std::cout<<"\n-------------info------------\n";
  std::cout<<twoseven<<"[0m";
  std::cout<<"The run was done for: \nTraffic\t\t:\t"<<trafic_type<<"\n";
  std::cout<<"requestsize\t:\t"<<req_length<<"\n";
  std::cout<<"datacheck\t:\t"<<datacheckloop<<"\n";
  std::cout<<"debugloop\t:\t"<<debugloop<<"\n";
  std::cout<<"delgrouping\t:\t"<<del_group<<"\n";
  if( maxRunTime > 0 ) 
    std::cout<<"maxruntime\t:\t"<<maxRunTime<<" seconds\n";
  std::cout<<"throttle\t:\t"<<throttle<<"\n";
  std::cout<<"delgroup\t:\t"<<del_group<<"\n";
  std::cout<<"reqrat\t\t:\t"<<reqrat<<"\n";
  std::cout<<"perf\t\t:\t"<<perf<<"\n";
  std::cout<<"NumberOfEvents\t:\t"<<NumberOfEvents<<"\n";

  std::cout<<twoseven<<"[5m";
  std::cout<<"\n-------------Run statistics------------\n";
  std::cout<<twoseven<<"[0m";
  
  std::cout<<"Eff. run time\t:\t"<<m_timeElapsed_main.as_seconds()<<" seconds\n";
  idle/=1.0e+6; //idle is in us
  std::cout<<"send idle\t:\t"<<idle<<" seconds\n";
  std::cout<<"send idle\t:\t"<<twoseven<<"[7m";
  std::cout<<(100*idle/m_timeElapsed_main.as_seconds())<<twoseven<<"[0m"<<" % of run time\n";
  del_idle/=1e+6; //del_idle is in us
  std::cout<<"delete idle\t:\t"<<del_idle<<" seconds\n";
  std::cout<<"delete idle\t:\t"<<twoseven<<"[7m";
  std::cout<<(100*del_idle/m_timeElapsed_main.as_seconds())<<twoseven<<"[0m"<<" % of run time\n";

  std::cout.precision(10);
  std::cout<<"Sent\t\t:\t"<<msg_sent<<" messages\n";
  std::cout<<"Sent\t\t:\t"<<total_sent<<" bytes\n";
  std::cout<<"Sent\t\t:\t"<<total_sent/msg_sent<<" bytes per message\n";
  std::cout<<"Req. rate\t:\t"<<twoseven<<"[7m"; std::cout.precision(5);
  std::cout<<(msg_sent/(1000.0*m_timeElapsed_main.as_seconds()))<<" kHz\n"<<twoseven<<"[0m"; 
  std::cout<<"Del.req.\t:\t"<<del_sent<<" messages, "<<del_sent/(1000.0*m_timeElapsed_main.as_seconds())<<" kHz\n";
  std::cout.precision(10);
  std::cout<<"Received\t:\t"<<msg_rcv<<" messages\n";
    
  if( m_invalidmsg ) {
    std::cout<<"Warning: Invalid package data:\t"<<twoseven<<"[101m"<<m_invalidmsg<<twoseven<<"[0m"<<"\n";
    std::cout<<"Warning: Invalid RosHrd\t:\t"<<twoseven<<"[101m"<<m_rosmark<<twoseven<<"[0m"<<" \n";
    std::cout<<"Warning: Invalid RobHrd\t:\t"<<twoseven<<"[101m"<<m_robmark<<twoseven<<"[0m"<<"\n";
    std::cout<<"Warning: Invalid RodHrd\t:\t"<<twoseven<<"[101m"<<m_rodmark<<twoseven<<"[0m"<<"\n";
    std::cout<<"Warning: Invalid CRC\t:\t"<<twoseven<<"[101m"<<m_invalidCRC<<twoseven<<"[0m"<<"\n";
    /*if( wrongDataRcv > 0 ) 
      std::cout<<"Rod data check failed for "<<wrongDataRcv<<" packages, "<<100*wrongDataRcv/msg_rcv<<" % of all received event fragments\n";*/
  }
  std::cout<<"Received\t:\t"<<total_received<<" bytes\n";
  if( msg_rcv > 0 ) 
    std::cout<<"Received\t:\t"<<total_received/msg_rcv<<" bytes per message\n";
  else
    std::cout<<"Received\t:\t0 bytes per message\n";
  std::cout<<"Received\t:\t"<<twoseven<<"[106m";
  std::cout.precision(5);
  std::cout<<(total_received/m_timeElapsed_main.as_seconds())<<" bytes/s"<<twoseven<<"[0m"<<"\n\n";
  std::cout<<"pending \t:\t"<<empty_msg<<"\n";
  std::cout<<"Not found\t:\t"<<not_found<<"\n";
  std::cout<<"min/max size\t:\t"<<min_size<<"/"<<max_size<<"\n";
  std::cout<<"last requested: "<<req_add[(sizeof(g_header)/sizeof(int))]<<"\n";
  std::cout<<"re-requested: "<<nre_requested<<"\n";

  std::cout<<"not received "<<(msg_sent-msg_rcv)<<" messages\n";
  std::cout<<"Packages checked: "<<checked_pac<<" messages\n";
  std::cout<<"Full OM queue: "<<fullom<<"\n";
  
  if( logname.size() > 0  )
    write_log();

  std::cout<<"\n\n";
}

void write_log() {

  ofstream log(logname.c_str(), ios:: app );
  if( !log )
    std::cerr<<"Error: \nCannot open logfile, statistics only on the screen\n";
  else {
    std::cout<<"\nWriting log file\n";

    // EB faction, ROD data size, LvL1 rate, req_rate, transfer rate
    double Lvl1 = del_group*del_sent/(1000.0*m_timeElapsed_main.as_seconds());
    double ROD_size = (total_received/msg_rcv) - (sizeof(g_header) + sizeof(RobHdr));
    double reqRate = (msg_sent/(1000.0*m_timeElapsed_main.as_seconds()));
    double tot_idle = 100*(idle+del_idle)/m_timeElapsed_main.as_seconds();
  
    log<<throttle<<"\t"<<reqrat<<"\t"<<ROD_size<<"\t"<<Lvl1<<"\t"<<reqRate<<"\t";
    log<<(total_received/m_timeElapsed_main.as_seconds())<<"\t"<<tot_idle;
    if( nre_requested > 0 ) 
      log<<"\tre-requested: "<<nre_requested;
    if( timeoutflag )
      log<<"\ttimeout";
    log<<"\n";    
    log.close();
  }
}

void catcher(int dummy)
{
  std::cerr<<"Error: catcher entered\n\n";

  endoftime = 1;
  dummy = 0;
  m_timeElapsed_main = dcclock.time() - startOfRun;

  signal(SIGINT,&catcher);
  signal(SIGBUS,&catcher);
  signal(SIGSEGV,&catcher);

  //cancel snd  and rcv threads
  /*if( pthread_cancel(snd_thr) )
    perror("snd cancel error ");
  if( pthread_cancel(rcv_thr) )
  perror("rcv cancel error ");*/

  fprintf(stderr,"Get Info: Catcher end of program\n");
  get_info();
  if( logname.size() > 0  )
    write_log();

  delete_globals();  

  //std::cerr<<"Error: catcher end of program\n\n";
  fprintf(stderr,"Error: catcher end of program\n\n");
 
  exit(1);
}

void dump_fullevent(Buffer* rcv_buf,u_short err_index) {
  using namespace MessagePassing;
  const char *err_msg[] = {"No data errors","Event not available","CRC check mismatch",
			   "Invalid Rod marker","Invalid ROB marker","Data error","Wrong error index"};

  if( err_index >= (sizeof(err_msg)/sizeof(err_msg[0])) )
    err_index = sizeof(err_msg)/sizeof(err_msg[0]) -1;
  
  Buffer::iterator ins_rcv = rcv_buf->begin();
  Buffer::iterator ins = ins_rcv;
  FragHdr FragHdr;
  ins_rcv>>FragHdr;
  /*FragHdr *pFragHdr = (FragHdr*)ins_rcv.address();*/

  std::cerr.setf(ios::hex,ios::basefield);

  unsigned int buf_size = rcv_buf->size();
  int *bp = new int[buf_size];
  ins_rcv = rcv_buf->begin();
  //ins_rcv>>bp;

  //std::cerr<<"\n---->Dumping full event<---------------\n";
  /*if( pFragHdr->RosHrd.marker != ROSMarker ) {
    std::cerr<<"Error: Invalid RosHrd\t:\t"<<twoseven<<"[101m"<<pFragHdr->RosHrd.marker<<twoseven<<"[0m";
    std::cerr<<" expected "<<ROSMarker<<"\n";
    }*/

  if( buf_size >= sizeof(FragHdr) ) {
    if( FragHdr.RobHrd.marker != ROBMarker ) {
      std::cerr<<"Error: Invalid RobHrd\t:\t"<<twoseven<<"[101m"<<FragHdr.RobHrd.marker<<twoseven<<"[0m";
      std::cerr<<" expected "<<ROBMarker<<"\n";
    }
    
    unsigned int *Rodpdata = (unsigned int*)(FragHdr.rod_words);
    //0xb0f or 0xb0f supressed
    if( *Rodpdata == 0xb0f00000 || *Rodpdata == 0x0 )
      Rodpdata = Rodpdata + 1;
    if( (*Rodpdata != RODMarker) && (*(Rodpdata + 1) != RODMarker) ) {
      std::cerr<<"Warning: Invalid RodHrd\t:\t"<<twoseven<<"[101m"<<*Rodpdata<<twoseven<<"[0m";
      std::cerr<<" expected "<<RODMarker<<"\n";
    }
    
    if( buf_size == sizeof(FragHdr) ){
      if (empty_msg < 10 )   {
	std::cerr<<"Error: <empty>\r";
      }
    }
    else {

      unsigned int vdata;
      unsigned int j = 0;
      
      if( /*rodDataDump*/fdataout == NULL ) 
	std::cerr<<"\nBuffer size: "<<buf_size<<" "<<err_msg[err_index]<<"\nDumping event fragment:\n";	
      else
	/**rodDataDump<<"\nBuffer size: "<<buf_size<<"\nDumping event fragment:\n";*/
	fprintf(fdataout,"\nBuffer size: %d\n%s. Dumping event fragment:\n",buf_size,err_msg[err_index]);

      /*Buffer::iterator ins_end = rcv_buf->end();*/
       unsigned int dumped_size = 0;
       while( dumped_size < buf_size ) {
	ins>>vdata;
	dumped_size+=4;
	if( /*rodDataDump*/fdataout  == NULL ) {
	  if( !j ) 
	    std::cerr<<"\n";
	  std::cerr<<vdata<<" "; 
	}
	else {
	  if( !j )
	    fprintf(fdataout,"\n");
	    /*(*rodDataDump)<<"\n";
	      (*rodDataDump)<<setw(8); (*rodDataDump)<<vdata<<" ";*/
	    fprintf(fdataout,"%8x ",vdata);
	} 
	if( ++j >= 8 ) 
	  j = 0;
       }

       if( /*rodDataDump*/ fdataout == NULL ) 
	std::cerr<<"\n------->End of event<--------\n";
       else
	 fprintf(fdataout,"\n------->End of event<--------\n");
       /* *rodDataDump<<"\n------->End of event<--------\n"<<flush;*/
    }
  }
  else
    std::cerr<<"Packet length < DC header size\n";
  std::cerr.setf(ios::dec,ios::basefield);
  delete [] bp;
}

void dumpErrEvent(Buffer* rcv_buf) {
  using namespace MessageInput;
  
  if( rodErrData == NULL ) {
    rodErrData = new ofstream("invalidEvFrag.dat",ios::trunc);
    if( *rodErrData ) 
      rodErrData->setf(ios::hex,ios::basefield);
  }

  if( rodErrData != NULL ) {
    //unsigned int j = 0;
    Buffer::iterator ins = rcv_buf->begin();

    dump_fullevent(rcv_buf,3);
    
    /*unsigned int dval;
    while( ins != rcv_buf->end() ) {
      ins >>(dval);
      if( !(j%8) )
	(*rodErrData)<<"\n"; 
      (*rodErrData)<<dval;
      }*/
  }
}

void RodDataDump(Buffer* rcv_buf) {
  using namespace MessageInput;
  
  Buffer::iterator ins = rcv_buf->begin();
  if( rcv_buf->size() == sizeof(FragHdr) ) {
    if (empty_msg < 10 )   {
      std::cout<<"Error: <empty>\r";
      return;
    }
  }

  unsigned int vdata,j =0;
  while( ins != rcv_buf->end() ) {
    ins >>(vdata);
    if( vdata == RODMarker ) {
      std::cout<<"\n\nRod data dump:\n";
      std::cout.setf(ios::hex,ios::basefield);
    }
     if( j && !(j%8) )
       std::cout<<"\n";
    std::cout<<" x"<<vdata;
    j++;
  }
  std::cout<<"\n"<<flush;
  std::cout.setf(ios::dec,ios::basefield);
}

int RodDataCheck(Buffer* rcv_buf) {
  using namespace MessageInput;
  
  unsigned int errdat = 0; 
  int data_error = 0;
  u_int rcv_pack_size;
  rcv_pack_size = rcv_buf->size();
 
  Buffer::iterator ins = rcv_buf->begin();

  // For the CRC check position ins iterator at ROD header
  g_header gen_hdr; ins>>gen_hdr;    
  u_int processed_size = sizeof(g_header);

  while( (rcv_pack_size - processed_size) > 0  ) {
    Buffer::iterator ins_Data = ins;
    RobHdr rob_hdr; ins>>rob_hdr;

    RobinRodHdr_t rodHdrData;
    ins_Data>>rodHdrData;
    
    if( rob_hdr.totalSize == sizeof(RobHdr) + TrailesSize ) {
      if (empty_msg < 10 )
	std::cerr<<"\nError: <empty>";
    }

    if( rob_hdr.marker != ROBMarker ) {
      dump_fullevent(rcv_buf,5);
      m_robmark++;
      return 1; // No ROB marker, data invalid
    }

    u_int ROD_marker = 0;
    if( rodHdrData.rod_words[ROD_marker] == 0xb0f00000 || rodHdrData.rod_words[ROD_marker] == 0x0 ) // move one more
      ROD_marker++;
    if( rodHdrData.rod_words[ROD_marker] != RODMarker ) {
      dump_fullevent(rcv_buf,4);
      m_rodmark++;
      return 1; // No Rod Marker
    }
      
    unsigned int rcv_dataSize = rob_hdr.totalSize - rob_hdr.hdrSize - 1; // -1 - trailerc - contains CRC
    if( rcv_dataSize > (rcv_pack_size - sizeof(g_header)) ) {
      fprintf(stderr,"Wrong event fragment (read from ROB hdr)\n");
      return 1;
    }
    // now, "ins" should be position at the ROD header*/

    u_int rcv_crc;
    u_int calc_crc = crctablefast16(ins,rcv_dataSize,&rcv_crc);
    if( calc_crc != rcv_crc ) {
      std::cout.setf(ios::hex,ios::basefield);
      std::cout<<"ERROR: CRC mismatch for event Id 0x"<<rodHdrData.evId<<"\nreceived CRC: 0x"<<rcv_crc<<" calculated CRC: 0x"<<calc_crc<<"\n";
      std::cout.setf(ios::dec,ios::basefield);
      dump_fullevent(rcv_buf,2);
      m_invalidCRC++;
      data_error = errdat = 1;
    }

    processed_size+= 4*rob_hdr.totalSize;
  }
    
    /*unsigned int max_len = rcv_buf->size();
    unsigned int lsize = 0;
    unsigned int cur_data; unsigned int old_data; 
    short rod_detect = 0;
    std::cerr.setf(ios::hex,ios::basefield);
    old_data = 0;
  
    ins = rcv_buf->begin();
    while( lsize < (max_len-24) ){  //&& ins != rcv_buf->end()*
      lsize+=4;
      ins>>cur_data; 
      if( rod_detect == 10 ) {
	if( (cur_data - old_data) != 1 || (cur_data - old_data) == 0 ) {
	  data_error = 1;
	  std::cout.setf(ios::hex,ios::basefield);
	  //std::cout<<"\n\nEvent id: x"<<pFragHdr->RosHrd.l1Id<<" srcId: x"<<pFragHdr->RosHrd.srcId;
	  std::cout<<"\nWarning: data discontinuity for event id: x:"<<pFragHdr->evId<<": old: x"<<old_data<<" new: x"<<cur_data;
	  if( fdataout != NULL ) {
	    fprintf(fdataout,"\nWarning: data discontinuity for event id: 0x%x : old: 0x%x",
		    old_data,cur_data);
	  }
	  if( rodDataDump != NULL && hexordec ) {
	    // *rodDataDump<<"\n\nEvent id: x"<<pFragHdr->RosHrd.l1Id<<" srcId: x"<<pFragHdr->RosHrd.srcId;
	    // *rodDataDump<<"\n\nEvent id: 0x"<<pFragHdr->evId<<"\n"<<" old: x"<<old_data<<" new: x"<<cur_data;
	  }

	  std::cout.setf(ios::dec,ios::basefield);
	  std::cout<<"\nPosition in the data stream: ";
	  std::cout<<"entry nr: "<<(lsize/4)<<" raw (8 words in a raw): "<<(lsize/32)<<", size "<<lsize<<"\n"<<flush;
	  // *rodDataDump<<"entry nr "<<(lsize/4)<<" raw (8 words in a raw) "<<(lsize/32)<<"\n";
	  if( fdataout != NULL )
	    fprintf(fdataout,"\nPosition in the data stream: entry nr: %d  raw (8 words in a raw): %d\n",
		   (lsize/4),(lsize/32));
	  
	  errdat++;	  
	}
	old_data = cur_data;
      }
      else {
	if( cur_data == RODMarker || rod_detect > 0 ) {
	  rod_detect++;
	  old_data = cur_data;
	}
      }
    }
    if( lsize < (max_len-24) ) {
      cout<<"Error: Buffer size < Size reported in the header\n";
      errdat = 1;
    }*/
    
  if( data_error ) {
    if( fdataout != NULL ) {
      fprintf(fdataout,"\nEvent with the data error:\n");
    }
    dump_fullevent(rcv_buf,3);
  }

  std::cerr.setf(ios::dec,ios::basefield);
 
  if( errdat != 0 )
    wrongDataRcv++;
  return errdat;
}

void dec2bin(unsigned long v2con,ofstream *dataDump, short bino) {
  
  int binval[LSIZE];
  int cval;

  using namespace std;
  std::cout.setf(ios::dec,ios::basefield);

  if( bino ) {
    (*dataDump).write(reinterpret_cast<char *>(&v2con),sizeof(int));
    return;
  }

  for( cval = 0; cval < (int)LSIZE; cval++ ) {
    binval[cval] = (v2con>>cval)&1;
    if( (v2con>>cval) == 0 ) {
      break;
    }
  }
  if( cval == (int)LSIZE )
    cval--;

  while( cval >= 0 ) {
    if( dataDump == NULL ) 
      std::cerr<<binval[cval];
    else {
      if( bino ) {
	
	// (*dataDump).write(reinterpret_cast<char *>(&binval[cval]),sizeof(int));
      }
      else
	*dataDump<<binval[cval];
    }
    --cval;
  }
}

void rcv_cleanup(void *pstruc) {
  /*short *wait_rcv;*/

  //wait_rcv = (short*)pstruc;
  std::cerr<<"\nRcv cleanup... ";
  //std::cerr<<"rcv active flag: "<<*wait_rcv<<"\n";
  /*while( *wait_rcv != 0 ) {
    sched_yield();
    }*/

  pthread_mutex_lock(&rcv_lock);
  rcv_cancel_count++;
  pthread_cond_signal(&rcv_done);
  pthread_mutex_unlock(&rcv_lock);
  
  std::cerr<<"done\n";
  pstruc = pstruc; //unused
}

void snd_cleanup(void *pstruc) {

  std::cerr<<"\nSnd cleanup... ";

  /*pthread_mutex_lock(&snd_lock);
  snd_cancel_count++;
  pthread_cond_signal(&snd_done);
  pthread_mutex_unlock(&snd_lock);*/
  
  std::cerr<<"done\n";
  pstruc = pstruc; //unused
}

void delete_globals() {
  delete [] rqrcv_ids; 
  delete [] del_add; 

  if( rodDataDump != NULL ) { 
    rodDataDump->close();
    delete rodDataDump;
  }

  if( rodErrData!= NULL ) {
    rodErrData->close();
    delete rodErrData;
  }
}

void crc_init() {
  //compute CRC table
  const u_long crchighbit = (u_long) (1 << 15);
  const u_long polynom =  0x1021;

  for (int i = 0; i < 65536; i++) {
    u_long crc = i;
    
    for (int j = 0; j < 16; j++)  {
      u_long bit = crc & crchighbit;
      crc <<= 1;
      if (bit) 
	crc ^= polynom;
    }			
    crctab16[i] = crc & 0xffff; 
  }
}

u_int crctablefast16(Buffer::iterator& ins, u_int len, u_int *crc_data) {

  u_long crc1 = 0xffff;
  u_long crc2 =0xffff;
  u_long crc;
  u_short data16;
  while( len-- ) {
    ins>>data16;
    crc1 = (crc1 << 16) ^ crctab16[(crc1&0xffff) ^data16];
    ins>>data16; 
    crc2 = (crc2 << 16) ^ crctab16[(crc2&0xffff) ^data16];
  }
  ins>>(*crc_data);

  crc1 &= 0xffff;
  crc2 &= 0xffff;
  
  crc = crc1 + (crc2<<16);
  return((u_int)crc);
}

