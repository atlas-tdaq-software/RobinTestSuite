#include <stdio.h>
#include <fstream>
#if (__GNUC__ > 2) || (__GNUC_MINOR__ > 95)
#include <sstream>
#else
#include <strstream>
#define istringstream istrstream
#define ostringstream ostrstream
#endif

#include <iostream>
#include <string.h>
#include <errno.h>

#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <list>
#include <vector>
#include <string>

#include "msg/BufferManager.h"
#include "RobinTestSuite/DCRequester.h"
#include "RobinTestSuite/DCMsgHeaders.h"
#include "RobinTestSuite/RobEm.h"

template <class T> void getval(string key, T& conval ) {
#if (__GNUC__ > 2) || (__GNUC_MINOR__ > 95)
  istringstream ival(key);
#else
  istringstream ival(key.c_str());
#endif
  if( key.find("0x") == 0 || key.find("x") == 0 ) 
    ival.setf(ios::hex,ios::basefield);
  ival>>conval;
  //std::cout<<"\n key "<<key<<" yields "<<conval<<"\n";
}

using namespace std;

int fracdone;
int timeoutflag = 0;
int ROLstat[3];
unsigned int *Ev_idPos[3];
int DelNode;

unsigned int* GenRequest(unsigned int csize) {
  static unsigned int msg[Max_msgsize];  
  u_int RodSeed[3] = {0x110000,0x220000, 0x330000};
  u_int trailer[24];

  //std::cout<<"\nGenerating new message";
  csize+=8;
  u_int *rodData = new u_int[csize];
   
  //initialise message header
  trg_msg  gen_req;
  gen_req.msg_header.magic_word = MESSAGE_MAGIC_WORD;
  gen_req.msg_header.mes_type_id = L2PU_Data_Request;
  gen_req.msg_header.sender_id = 1<<15;
  gen_req.msg_header.transaction_id = 1;
  gen_req.msg_header.timestemp = TimeStamp;

  gen_req.msg_header.mlength =  sizeof(g_header) + 3*(sizeof(int)*csize + sizeof(RobinRodHdr_t));
  std::cout<<"\nmlength = "<<gen_req.msg_header.mlength<<"\n";

  RobinRodHdr_t evfrag_hdr;
  evfrag_hdr.RobHrd.marker = ROBMarker;
  evfrag_hdr.RobHrd.totalSize = csize + sizeof(RobinRodHdr_t)/sizeof(int);
  evfrag_hdr.rod_words[0] = RODMarker;
  
  if( csize > 0 ) {
    //create message proper
    if( csize > (Max_msgsize-sizeof(g_header)/sizeof(int)) )
      csize = Max_msgsize - sizeof(g_header)/sizeof(int);
    
    unsigned int *pmsg = msg;
    memcpy(pmsg,&gen_req.msg_header,sizeof(g_header));
    pmsg+= sizeof(g_header)/sizeof(int);
    
    /* copy data for all three ROLs: Rob header, ROD words, followed by the data */
    for( u_int idata = 0; idata < 3; idata++ ) {
      memcpy(pmsg,&evfrag_hdr,sizeof(RobinRodHdr_t));
      pmsg+= sizeof(RobinRodHdr_t)/sizeof(int);
      Ev_idPos[idata] = pmsg - 1;
      *Ev_idPos[idata] = 0xfab000;
      
      for( u_int irod = 0; irod < csize; rodData[irod++] = RodSeed[idata] + irod );
      memcpy(pmsg,rodData,csize*sizeof(int));
      pmsg+= csize;
    }
    memcpy(pmsg,trailer,sizeof(trailer));
  }
  //dump_sndData(msg,(gen_req.msg_header.mlength)/sizeof(int));

  delete [] rodData;
  return(msg);
 
}

int main(int argc, char *argv[])
{
  string conf_name;
  if( argc > 1 )
    conf_name = argv[1];
  else
    conf_name = "dcrequester.conf";
  in_par(conf_name); 
      
  unsigned int dfpoint[5];
  connect(dfpoint);

  cout<<"Header size: "<<sizeof(FragHdr)<<"\n";
  /* signals to catch */
  void (*fp)(int) = &catcher;
  signal(SIGINT,fp);
  signal(SIGBUS,fp);
  signal(SIGSEGV,fp);

  min_size = 9999999; max_size = 0;
  max_ev_id = empty_msg = 0;
  ROLstat[0] = ROLstat[1] = ROLstat[2] = 0;

  unsigned int mes_size = req_length;
  req_add = GenRequest(mes_size); 

  m_msg_snd_size = sizeof(g_header) + mes_size*sizeof(int);
  pg_header = (g_header*)req_add;
  std::cout<<"\n\nHeader size: "<<sizeof(g_header)<<"\nRequested length: "<<req_length<<"\n";
  
  //prepare DFM_Clear header and empty delete list

  total_sent = total_received = nloop = 0;
  m_invalidmsg = m_rosmark = m_robmark = m_rodmark = 0;
  msg_sent = msg_rcv = del_sent = 0;

  pg_header->req_length = req_length;

  startOfRun = dcclock.time();
  m_timeElapsed_main = dcclock.time();
  rcv_dcmsg_EM();


  get_info();
  return 0;
}

void in_par(string conFile) {

  ifstream configFile(conFile.c_str());
  if( !configFile ) {
    std::cerr<<"Error: cannot open "<<twoseven<<"[7m"<<conFile<<twoseven<<"[0m"<<" exit now\n";
    exit(-1);
  }
  std::cout<<"\nUsing "<<twoseven<<"[7m"<<conFile<<twoseven<<"[0m";
  std::cout<<" to read config parameters\n"; 

  const int lineSize = 1024;
  char buf[lineSize];
  for( u_int irol = 0; irol < NROLS; selectedNodes[irol++] = 0 ){};

  map<string,string> local_config;
  int rnode;
  int rols = 0;
  while(configFile.getline(buf, lineSize)) {
    if (buf[0] == '#') { // skip comment lines
      continue;
    }
    
    string line(buf);
    int equalsPosition = line.find_first_not_of(' ');
    line.erase(0, equalsPosition);

    int shiftval;
    if( (equalsPosition = line.find("localhost")) >= 0 ) 
      shiftval = strlen("localhost");
    else {
      equalsPosition = line.find("remote");
      shiftval = strlen("remote");
    }

    if( equalsPosition >= 0 ) {  
      string host(line, equalsPosition+shiftval);
      equalsPosition = host.find_first_not_of(' ');
      host.erase(0, equalsPosition);
      equalsPosition = host.find(' ');

      string key(host,0,equalsPosition);
      host.erase(0,equalsPosition+1);
      local_config[key] = host;
      getval(local_config[key],rnode);

      //int rawbcast = key.find("01:"); int udpbcast = key.find("255");
      //if( ((rawbcast != 0 && udpbcast < 0) ) { 
      if( rnode == 0 /*|| rnode > 10*/ ) { //here rnode == 0 denotes a "local" node, node 1-10 reserved for "delete" ports	
	rnode+= (1<<ROS_SHIFT);  // add ROS node id to the remote node
      }
      else
	rols++;

      DelNode = rnode;
      RhostsNode.insert(make_pair(key,rnode));
    }
    else {
      equalsPosition = line.find(' ');
      string key(line, 0, equalsPosition);
      line.erase(0,equalsPosition+1);
      local_config[key] = line;
    }
  }

  multimap<string,int>:: iterator irhn;
  std::cout<<"\nFrom conf file: \n";
  for( irhn = RhostsNode.begin(); irhn != RhostsNode.end(); irhn++ )
    std::cout<<irhn->first<<" "<<irhn->second<<"\n";

  //transport type
  getval(local_config["transport"],trafic_type);
  //cerr<<"transport: "<<trafic_type<<"\n";

  //requested size
  getval(local_config["requestsize"],req_length); 
  //cerr<<"requestsize: "<<req_length<<"\n";
  
  //ROL will be derived from remote_node
  getval(local_config["remotenode"],remote_node);

  //datacheck
  getval(local_config["datacheck"],datacheckloop);

  //debugloop
  getval(local_config["debugloop"],debugloop);

  firstId = 1;
  getval(local_config["firstId"],firstId);

  //maxruntime
  getval(local_config["maxruntime"],maxRunTime);  
  
  //throttle
  getval(local_config["throttle"],throttle);

  //group delete
  getval(local_config["delgroup"],del_group);

  //ROD data dumped in hex or dec format
  getval(local_config["hexdump"],hexordec);

  getval(local_config["datadumpto"],filedata);

  getval(local_config["NumberOfEvents"],NumberOfEvents);

  //requested ratio of events
  getval(local_config["reqrat"],reqrat);
  if( reqrat < 1 || reqrat > 100  )
    reqrat = 100;

  //performance run
  getval(local_config["perf"],perf);

  //if resetid == 1, event id will be reset to 1 after sending del msg. 
  //On the Robin side, KeepFrags must be set to 1
  getval(local_config["resetid"],resetid);

  getval(local_config["logfile"],logname);
  time_t newmesDue = 600;
  getval(local_config["tstamp"],newmesDue);

  getval(local_config["single"],single);
  getval(local_config["eventloop"],MAX_EV_ID);

  // Check if there are specific Rols selected
  getval(local_config["NodeROL0"],selectedNodes[0]);
  getval(local_config["NodeROL1"],selectedNodes[1]);
  getval(local_config["NodeROL2"],selectedNodes[2]);
  NumOfSelNodes = 0;
  for( u_int inode = 0; inode < NROLS; inode++ )
    if( selectedNodes[inode] > 0 ) 
      NumOfSelNodes++;

  getval(local_config["timeout"],rqtimeout);
  if( rqtimeout < 1 ) 
    rqtimeout = 2; //default value

  time_t ltime;
  if( logname.size() > 0 ) {
    std::cout<<"logfile: "<<logname<<"\n";
    ofstream log(logname.c_str(), ios:: app );
    if( !log )
      std::cerr<<"Error: \nCannot open logfile, statistics only on the screen\n";
    else {
      short tstamp = 1;
      time(&ltime);
      struct stat  logstat;
      if( stat(logname.c_str(),&logstat) > -1 ) {
	time_t dtime = ltime - logstat.st_mtime;
	if( !newmesDue && dtime > 0 ) 
	  tstamp = 0;
      }

      std::cout<<"tstamp: "<<tstamp<<" new measurement"<<newmesDue<<"\n";
      if( tstamp ) {
	log<<Ver<<"\n\n";
	log<<"\n"<<ctime(&ltime)<<"\n";
	log<<"OM\tEB\tData\tLvL1\treqrate\ttranrate\tidle\tdata\n";
	log<<" \t[%]\t[bytes]\t[kHz]\t[kHz]\t[bytes/s]\t[%]\t[words]\n";
	log<<"-------------------------------------------------------------------------\n";
      }
      log.close();
    }
  }
}

void connect(unsigned int *dfpoint) {
  using namespace MessagePassing;

  std::cout<<"Traffic type: "<<trafic_type<<"\n";
  init_tran(trafic_type,RhostsNode,dfpoint);

  u_int NremHosts = RhostsNode.size() - 1;
  std::cout<<"\nNumber of all remote nodes "<<NremHosts<<":\n";

  
  multimap<string,int>:: iterator irhn; 
  std::cerr.setf(ios::hex,ios::basefield);
  std::cout<<"del node: "<<DelNode<<"\n";
  for( irhn = RhostsNode.begin(); irhn != RhostsNode.end(); irhn++ ) {
    if( irhn->second != (1<<ROS_SHIFT) ) {
      Port *port = Port::find(irhn->second);
      if( port == 0 ) 
	std::cerr<<"Error: Couldn't find port for host "<<irhn->first<<" node 0x"<<irhn->second<<"\n";
      else {
	int rawbcast = irhn->first.find("01:"); int udpbcast = irhn->first.find("255");
	if( rawbcast == 0 || udpbcast >= 0  ||  irhn->second == DelNode ){
	  deletehosts.push_back(port);
	  std::cout<<"Delete node "<<irhn->first<<" port "<<port<<"\n";
	  port_del = port; //Delete node 
	}
	else {
	  std::cout<<"host "<<irhn->first<<" node 0x"<<irhn->second<<" port 0x"<<port<<"\n";
	  remotehosts.push_back(port);
	}
      }
    }
  }
  std::cerr.setf(ios::dec,ios::basefield);

  if( (m_NumDelHosts = deletehosts.size()) == 0  ) {
    std::cout<<"\n\t"<<twoseven<<"[103m"<<"Warning ! No multicast delete host found, ";
    std::cout<<" using remote node to send delete"<<twoseven<<"[0m"<<"\n";
  }

  if( (m_NumRemHosts = remotehosts.size()) == 0 ) {
    std::cerr<<"Error: \n\t\t"<<twoseven<<"[103m"<<"------->Warning ! No remote hosts found<---------"<<twoseven<<"[0m"<<"\t\t\n";
    if( port_del == 0 ) {
      std::cerr<<"Error: \n\t\t"<<twoseven<<"[101m"<<"------->Error ! No delete port, abord the program"<<twoseven<<"[0m"<<"\t\t\n";
      exit(1);
    }      
  }
  cerr<<"m_NumRemHost: "<<m_NumRemHosts<<"\n";
}


void snd_dcreq_EM(u_int ev_id) {
  using namespace MessagePassing;

  static short leftw = 0;
  unsigned int req_dat_size = ((g_header*)req_add)->mlength;
  
  (*Ev_idPos[0]) = (*Ev_idPos[1]) = (*Ev_idPos[2]) = ev_id;
    
  // send message 
  for( unsigned int iport = 0; iport < m_NumRemHosts; iport++ ) {
    Buffer *buf = new Buffer(req_dat_size);
    Buffer::iterator ins = buf->begin();
    ins.insert(req_add,req_dat_size);
    buf->size(req_dat_size);
    if( !(remotehosts[iport]->send(buf)) ) {
      std::cerr<<"Error sending data !!\n";
      //exit(1);
    }
    msg_sent++;
    total_sent+=req_dat_size;

    //dump_sndData(req_add,(req_dat_size/sizeof(int)));
  }

  //inrement transaction id 
  if( pg_header->transaction_id < MAX_TRAN_ID )
    pg_header->transaction_id++;
  else
    pg_header->transaction_id = 1;

  
}

int rcv_dcmsg_EM() {
  using namespace MessagePassing;
  using namespace MessageInput;

  int stat = 0;
  Buffer *rcv_buf;
  g_header gen_hdr;
  u_int ev_id;

  Time waitTime,waitTime_stop;

  struct timeval waitStart,waitStop;
  struct timezone ltz;
  while( 1 ) {
    if( (rcv_buf = Port::receive()) == NULL ) {
      std::cerr<<"\nError: receive() failed";
      stat = 1;
    }
    else {
      if( rcv_buf->size() < min_size ) 
	min_size = rcv_buf->size();
      if( rcv_buf->size() > max_size )
	max_size = rcv_buf->size();
      
      total_received+= rcv_buf->size();
      
      if( rcv_buf->size() == (sizeof(FragHdr)+4) )
	empty_msg++;
      msg_rcv++;
      
      Buffer::iterator ins_rcv = rcv_buf->begin();
      ins_rcv>>gen_hdr;
      
      switch(gen_hdr.mes_type_id) {
      case L2PU_Data_Request:
	//std::cerr<<"Received L2PU_Data_Request\n";
	
	// get requested event id
	ins_rcv>>ev_id;
	snd_dcreq_EM(ev_id);
	
	break;

      case DFM_Clear: /*gettimeofday(&waitStart,&ltz); 
	do {	  
	  gettimeofday(&waitStop,&ltz);
	  } while( ((1.0e+6*waitStop.tv_sec + waitStop.tv_usec) - (1.0e+6*waitStart.tv_sec + waitStart.tv_usec)) < 5 ); */// emulate Robin delete, sleep for aprox. 7 us
	//usleep(2);        // emulate Robin delete, sleep for aprox. 2 us
	//std::cerr<<"Received DFM_Clear\n"; 
	break;  
	

      default: std::cerr<<"Unknown message, ignored\n";
      }

      delete rcv_buf;      
    }
  }

  return stat;
}

int check_gh(Buffer* rcv_buf) {
  using namespace MessageInput;

  MessageHeader hdr(rcv_buf);
  if( !hdr.valid() ) {
    std::cerr<<"Error: \nMessage header: invalid MagicNumber\n";
    return 1;
  }
  
  if( hdr.timestamp()!= TimeStamp ) {
    std::cerr<<"Error: \nMessage header: invalid TimeStamp \n";
    return 1;
  }
  
  if( hdr.xid() > pg_header->transaction_id ) {
    std::cerr<<"Error: \nMessage header: xid in the future \n";
    return 1;
  }

  //check ROS and ROB headers
  Buffer::iterator ins = rcv_buf->begin();
  FragHdr *pFragHdr = (FragHdr*)ins.address();

  if( pFragHdr->RobHrd.marker != ROBMarker ) {
    m_robmark++;
    return 1;
  }

  return 0;
}

int check_robh(Buffer* rcv_buf) {
  MessagePassing::Buffer::iterator idc = rcv_buf->begin();
  unsigned int tot_extr = sizeof(RobMsgHdr) + sizeof(g_header);
  unsigned int *hdr = new unsigned int[tot_extr];
  idc.extract(hdr,tot_extr);

  //jump to the Rob header
  hdr = hdr + sizeof(g_header)/sizeof(int);
  
  RobMsgHdr *rob_hdr = (RobMsgHdr*)(hdr);
  if( rob_hdr->version != s_formatVersionNumber ) {
    std::cerr<<"Error: ROB header: wrong version number\n";
    delete [] hdr;
    return 1;
  }
  
  delete [] hdr;
  return 0;
}

void change_msgtype(int nloop,g_header* pg_header) {
  if( !(nloop%19) ) 
    pg_header->mes_type_id = SFI;
  else {
    if( !(nloop%17) ) 
      pg_header->mes_type_id = DFM;
    else
      if( !(nloop%16) ) 
	pg_header->mes_type_id = L2SV;
      else
	if( !(nloop%13) ) 
	  pg_header->mes_type_id = 0;
	else
	  if( !(nloop%11) )  
	    pg_header->mes_type_id = L2PU;
	  else
	    pg_header->mes_type_id = DFM;
  }
}

void add2str(EthSoc* ethinf) {
  char tmp_dump[5];
  unsigned char *addr;
  int i;

  addr = (unsigned char*)&(ethinf->loc_eth);
  if( *addr < 10 )
    sprintf(ethinf->str_add,"0%X",(int)*addr++);
  else
    sprintf(ethinf->str_add,"%X",(int)*addr++);
  for( i = 1; i < 6; i++ ) {
    if( *addr < 10 ) 
      sprintf(tmp_dump,":0%X",(int)*addr++); 
    else 
      sprintf(tmp_dump,":%X",(int)*addr++); 
    strcat(ethinf->str_add,tmp_dump);
  }
}

void get_info() {
  std::cout.setf(ios::dec,ios::basefield);

  std::cout<<twoseven<<"[5m";
  std::cout<<"\n\n-------------info------------\n";
  std::cout<<twoseven<<"[0m";
  std::cout<<"The run was done for: \nTraffic\t\t:\t"<<trafic_type<<"\n";
  std::cout<<"requestsize\t:\t"<<req_length<<"\n";
  std::cout<<"datacheck\t:\t"<<datacheckloop<<"\n";
  std::cout<<"debugloop\t:\t"<<debugloop<<"\n";
  std::cout<<"delgrouping\t:\t"<<del_group<<"\n";
  if( maxRunTime > 0 ) 
    std::cout<<"maxruntime\t:\t"<<maxRunTime<<" seconds\n";
  std::cout<<"throttle\t:\t"<<throttle<<"\n";
  std::cout<<"delgroup\t:\t"<<del_group<<"\n";
  std::cout<<"reqrat\t:\t"<<reqrat<<"\n";

  std::cout<<twoseven<<"[5m";
  std::cout<<"\n\n-------------Run statistics------------\n";
  std::cout<<twoseven<<"[0m";

  std::cout<<"Eff. run time\t:\t"<<m_timeElapsed_main.as_seconds()<<" seconds\n";
  std::cout<<"send idle\t:\t"<<idle<<" seconds\n";
  std::cout<<"send idle\t:\t"<<twoseven<<"[7m";
  std::cout<<(100*idle/m_timeElapsed_main.as_seconds())<<twoseven<<"[0m"<<" % of run time\n";
  std::cout.precision(10);
  std::cout<<"Sent\t\t:\t"<<msg_sent<<" messages\n";
  std::cout<<"Sent\t\t:\t"<<total_sent<<" bytes\n";
  std::cout<<"Sent\t\t:\t"<<total_sent/msg_sent<<" bytes per message\n";
  std::cout<<"Req. rate\t:\t"<<twoseven<<"[7m"; std::cout.precision(5);
  std::cout<<(msg_sent/(1000.0*m_timeElapsed_main.as_seconds()))<<" kHz\n"<<twoseven<<"[0m"; 
  std::cout<<"Del.req.\t:\t"<<del_sent<<" messages, "<<del_sent/(1000.0*m_timeElapsed_main.as_seconds())<<" kHz\n";
  std::cout.precision(10);
  std::cout<<"Received\t:\t"<<msg_rcv<<" messages\n";
  std::cout<<"Warning: Invalid header\t:\t"<<m_invalidmsg<<"\n";
  if( m_invalidmsg ) {
    std::cerr<<"Warning: Invalid RobHrd\t:\t"<<twoseven<<"[101m"<<m_robmark<<twoseven<<"[0m"<<"\n";
    std::cerr<<"Warnin: Invalid RodHrd\t:\t"<<twoseven<<"[101m"<<m_rodmark<<twoseven<<"[0m"<<"\n";
  }
  std::cout<<"Received\t:\t"<<total_received<<" bytes\n";
  std::cout<<"Received\t:\t"<<total_received/msg_rcv<<" bytes per message\n";
  std::cout<<"Received\t:\t"<<twoseven<<"[106m";
  std::cout.precision(5);
  std::cout<<(total_received/m_timeElapsed_main.as_seconds())<<" bytes/s"<<twoseven<<"[0m"<<"\n\n";
  std::cout<<"Lost:\t\t:\t"<<lost_msg<<"\n";
  std::cout<<"Empty payload\t:\t"<<empty_msg<<"\n";
  std::cout<<"min/max size\t:\t"<<min_size<<"/"<<max_size<<"\n";
  std::cout<<"maximum event id: "<<max_ev_id<<"\n";
  std::cout<<"last requested: "<<req_add[(sizeof(g_header)/sizeof(int))]<<"\n";

  std::cout<<"not received "<<(msg_sent-msg_rcv)<<" messages\n";
  std::cout<<"Rod data check failed for "<<wrongDataRcv<<" packages\n";
  std::cout<<"ROLstat ROL0 "<<ROLstat[0]<<" ROL1: "<<ROLstat[1]<<" ROL2: "<<ROLstat[2]<<"\n";

  std::cout<<"\n\n";
}

void write_log() {
  /* EB faction, ROD data size, LvL1 rate, req_rate, transfer rate */
  /*double Lvl1 = del_group*del_sent/(1000.0*m_timeElapsed_main.as_seconds());
  double ROD_size = (total_received/msg_rcv) - (sizeof(g_header) + sizeof(RobHdr));
  double reqRate = (msg_sent/(1000.0*m_timeElapsed_main.as_seconds()));

  idle = 100*idle/m_timeElapsed_main.as_seconds();
  *rlog<<throttle<<"\t"<<reqrat<<"\t"<<ROD_size<<"\t"<<Lvl1<<"\t"<<reqRate<<"\t";
  *rlog<<(total_received/m_timeElapsed_main.as_seconds())<<"\t"<<idle;
  if( timeoutflag )
    *rlog<<"\ttimeout";
  *rlog<<"\n";

  rlog->close();*/
}

void catcher(int dummy)
{
  m_timeElapsed_main = dcclock.time() - startOfRun;

  signal(SIGINT,&catcher);
  signal(SIGBUS,&catcher);
  signal(SIGSEGV,&catcher);

  get_info();
  std::cerr<<"Error: catcher end of program\n\n";
 
  exit(1);
}

void dump_fullevent(Buffer* rcv_buf) {
  using namespace MessageInput;
  
  Buffer::iterator ins = rcv_buf->begin();
  FragHdr *pFragHdr = (FragHdr*)ins.address();

  unsigned int max_size; 
  if( rcv_buf->size() > sizeof(FragHdr) )
    max_size = pFragHdr->GenHdr.mlength;
  else
    max_size = rcv_buf->size()/sizeof(int);
  std::cerr<<"\nBuffer size: "<<(rcv_buf->size()/sizeof(int))<<" packet size: "<<max_size<<"\n";

  std::cerr.setf(ios::hex,ios::basefield);
  std::cerr<<"\n---->Dumping full event<---------------\n";

  if( pFragHdr->RobHrd.marker != ROBMarker ) {
    std::cerr<<"Error: Invalid RobHrd\t:\t"<<twoseven<<"[101m"<<pFragHdr->RobHrd.marker<<twoseven<<"[0m";
    std::cerr<<" expected "<<ROBMarker<<"\n";
  }
  
  unsigned int *Rodpdata = (unsigned int*)((unsigned int)ins.address() + sizeof(FragHdr));
  //0xb0f or 0xb0f supressed
  if( *Rodpdata == 0xb0f00000 || *Rodpdata == 0x0 )
    Rodpdata = Rodpdata + 1;
  if( (*Rodpdata != RODMarker) && (*(Rodpdata + 1) != RODMarker) ) {
    std::cerr<<"Warning: Invalid RodHrd\t:\t"<<twoseven<<"[101m"<<*Rodpdata<<twoseven<<"[0m";
    std::cerr<<" expected "<<RODMarker<<"\n";
  }

  if( rcv_buf->size() == sizeof(FragHdr) )
    std::cerr<<"Error: <empty>\r";
  else {
    std::cerr<<"\n";
    unsigned int j = 0;
    unsigned int lsize = 0;
    unsigned int vdata;
    while( lsize++ < max_size /* && */ /*ins != rcv_buf->end()*/ ) {
      ins >>(vdata);
      if( j && !(j%8) )
	std::cerr<<"\n";
      if( vdata == ROSMarker || vdata ==  ROBMarker || vdata ==  RODMarker ) 
	std::cerr<<twoseven<<"[7m"<<" x"<<vdata<<twoseven<<"[0m";
      else
	std::cerr<<" x"<<vdata;
      j++;
    }
  }
  std::cerr.setf(ios::dec,ios::basefield);
}

void RodDataDump(Buffer* rcv_buf) {
  using namespace MessageInput;
  
  Buffer::iterator ins = rcv_buf->begin();
  if( rcv_buf->size() == sizeof(FragHdr) ) {
    std::cerr<<"Error: <empty>\r";
    return;
  }

  unsigned int vdata,j =0;
  while( ins != rcv_buf->end() ) {
    ins >>(vdata);
    if( vdata == RODMarker ) {
      std::cout<<"\n\nRod data dump:\n";
      std::cout.setf(ios::hex,ios::basefield);
    }
     if( j && !(j%8) )
       std::cout<<"\n";
    std::cout<<" x"<<vdata;
    j++;
  }
  std::cout<<"\n";
  std::cout.setf(ios::dec,ios::basefield);

}

void dump_sndData(u_int *pdata,u_int dsize)
{
  std::cout<<"\nSendig: \n";
  std::cout.setf(ios::hex,ios::basefield);
  for( u_int idata = 0; idata < dsize; idata++ ) {
    if( idata && !(idata%8) ) 
      std::cout<<"\n";
    std::cout<<pdata[idata]<<"\t";
  }
  std::cout.setf(ios::dec,ios::basefield);
}

int RodDataCheck(Buffer* rcv_buf) {
  using namespace MessageInput;
  
  unsigned int errdat = 0;
  if( rcv_buf->size() == sizeof(FragHdr) ) 
    std::cerr<<"Error: <empty>\r";
  else {
    Buffer::iterator ins = rcv_buf->begin();
    //FragHdr *pFragHdr = (FragHdr*)ins.address();
    unsigned int max_len = (sizeof(g_header)/sizeof(int));

    unsigned int lsize = 0;
    unsigned int cur_data; unsigned int old_data; 
    short rod_detect = 0;
    std::cerr.setf(ios::hex,ios::basefield);
    while( lsize++ < (max_len-5) ){
      ins>>cur_data; 
      if( rod_detect == 9 ) {
	if( (cur_data - old_data) != 1 && (cur_data - old_data) != 0 ) {
	  std::cerr<<"\n\nold: x"<<old_data<<" new: x"<<cur_data<<" at ";
	  std::cerr.setf(ios::dec,ios::basefield);
	  std::cerr<<lsize<<"\n";
	  std::cerr.setf(ios::hex,ios::basefield);
	  errdat++;
	}
	old_data = cur_data;
      }
      else {
	if( cur_data == RODMarker || rod_detect > 0 ) {
	  rod_detect++;
	  old_data = cur_data;
	}
      }
    }
  }
  std::cerr.setf(ios::dec,ios::basefield);
  if( errdat != 0 )
    wrongDataRcv++;
  return errdat;
}
