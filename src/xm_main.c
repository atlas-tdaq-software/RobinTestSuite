#include <Xm/Xm.h>
#include <Xm/RowColumn.h>
#include <Xm/ScrolledW.h>
#include <Xm/Form.h>

extern const char *Ver;
extern Widget CreateConsole(Widget,Widget);
XtAppContext app;
Widget top_level;

int main(int argc, char **argv)
{
  Widget       /* top_level, */main_widget,mainForm;
  char tmpstr[100];
  
  sprintf(tmpstr,"cts %s",Ver);
  /* Create and initialize the top-level widget */
  top_level = XtVaAppInitialize(&app, (String)"XM_main", NULL, 0, &argc, argv, NULL,XmNtitle,tmpstr,
				/*XmNminWidth, 190, XmNminHeight, 189,
				  XmNmaxWidth, 190, XmNmaxHeight, 189,*/				
				/*XmNbaseWidth, 1, XmNbaseHeight, 1,*/
				NULL,0);

  main_widget = XtVaCreateWidget((String)"main_widget",xmRowColumnWidgetClass,top_level,
				 XmNspacing, 5,NULL );
  
  /* console main frame */
  mainForm = XtVaCreateManagedWidget((String)"Main form",xmFormWidgetClass,main_widget,
				     XmNwidth,190,XmNheight,190, NULL);

  XtVaSetValues(mainForm,XtVaTypedArg,XmNbackground,XmRString,"LightBlue",
		strlen("LightBlue")+1,NULL); 

  /* create the widgets */
  CreateConsole(mainForm,main_widget);
  
  /* Realize the widgets and start processing events */
  XtManageChild(main_widget);
  
  XtRealizeWidget(top_level);
  XtAppMainLoop(app);

  return 0;
}

