#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dir.h>
#include <unistd.h>
#include <sys/poll.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <termios.h>
#include <fcntl.h>
#include <ctype.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>

#include "RobinTestSuite/pdef.h"
#include "RobinTestSuite/util.h"

extern int checkerr(char*,char*,char**,int,short);
extern int appendlog(char*,char*,short);
extern char *kermerr[];
extern void refreshwind(int);
extern void setstatus(int,short,char*);
extern int runRobins(char*);

int checkNlines(char* test_str) {
  int nlines;
  char *eol;

  nlines = 0;
  while( *test_str != '\0' ) {
    if( (eol = index(test_str,'\n')) !=NULL ) {
      nlines++;
      test_str = eol + 1;
    }
    else {
      if( strlen(test_str) > 0 ) /* a line without '\n'*/
	return(++nlines);
    }
  }

  return nlines;
}

int split2lines(char* test_str,char** argline) {
  int narg;
  char *eol;

  if( test_str == NULL || strlen(test_str) == 0 ) 
    return 0;

  narg = 0;
  while( *test_str != '\0' ) {

    if( (eol = index(test_str,'\n')) !=NULL ) {
      *argline++ = test_str;
      narg++;
      test_str = eol + 1;
    }
    else { /* no more lines to read */
      if( test_str != NULL && strlen(test_str) > 0 ) {
	*argline++ = test_str;
	return(++narg);
      }
    }
  }

  return (narg);
}

int splitext(char* test_str,char** argline) {
  int narg;
  char *eol;

  if( test_str == NULL || strlen(test_str) == 0 ) 
    return 0;

  narg = 0;
  while( *test_str != '\0' ) {

    if( (eol = index(test_str,'\n')) !=NULL ) {
      *eol = '\0';
      *argline++ = test_str;
      narg++;
      test_str = eol + 1;
    }
    else { /* no more lines to read */
      if( test_str != NULL && strlen(test_str) > 0 ) {
	*argline++ = test_str;
	return(++narg);
      }
    }
  }

  return (narg);
}

int splitarg(char* test_str,char** argline) {
  int narg;

  if( test_str == NULL ) 
    return 0;

  narg = 0;
  while( *test_str != '\0' ) {
    while( (*test_str == ' ') || (*test_str == '\t') || (*test_str == '\n') ) 
      *test_str++ = '\0';

    if( *test_str != '\0' ) {
      *argline++ = test_str; 
      narg++;
    
      while( (*test_str != '\0') && (*test_str != ' ') && 
	     (*test_str != '\t') && (*test_str != '\n') )
	test_str++;
    }
  }
  *argline = '\0';

  return (narg);
}

int execute(char *cmd)
{
  int pid,status;
  char loccmd[MAX_CmdLen];
  char *args[Max_NumItems];

  /*Get a child process.*/
  if( (pid = fork()) < 0 ) {
    perror("fork");
    exit(1);
  }

  if( pid == 0 ) {
    strncpy(loccmd,cmd,MAX_CmdLen-1);
    splitarg(loccmd,args);
    execvp(*args, args);
    /* execlp(*args,args[0],args[1]); */
    perror(*args);
    exit(1);
  }
  while( wait(&status) != pid );

  return(pid);
}

int executedup(char *cmd, char *buf, char *buferr) {
  int  pid,retpid,status;
  ssize_t  in_bytes;
  int pipefd[2],pipefderr[2];
  char tmpbuf[MAX_BYTE],loccmd[MAX_CmdLen];
  char *args[Max_NumItems];
  long fdflags;
  struct pollfd ufds[2];
  int j,nfdesc,timeout,waittime,nfdescmain,nfds;
  char *pstatistict,*pteststat;
  static char *pteststat_old;
  char *emptystat,*teststat;
  const char *statstr = "Number of complete events          = 0";
  int nemptystat;

  /* stdout pipe */
  if( pipe(pipefd) < 0 ) {
    perror("pipe");
    return -1;
  }

  /* stderr pipe */
  if( pipe(pipefderr) < 0 ) {
    perror("pipeerr");
    return -1;
  }

  /*Get a child process.*/
  if( (pid = fork()) < 0 ) {
    perror("fork");
    exit(1);
  }

  appendlog(buf,cmd,0);
  appendlog(buf,(char*)"::\n",0);
  /*child redirects its stdout and stderr to the pipes */
  if( pid == 0 ) {
    /* dup both: stdout and stderr */
    dup2(pipefd[1],1);
    dup2(pipefderr[1],2);
    close(pipefd[1]);
    close(pipefderr[1]);

    strncpy(loccmd,cmd,MAX_CmdLen-1);
    splitarg(loccmd,args);
    execvp(*args, args);

    perror(*args);
    exit(1);
  }

  if( (fdflags = fcntl(pipefd[0],F_GETFL)) == -1 ) {
    perror("fcntl getflag stdout::");
    fprintf(stdout,"errrr"); fflush(stdout);
  }
  fdflags |= O_NONBLOCK;
  if( fcntl(pipefd[0],F_SETFL,fdflags)  == -1 ) {
    perror("fcntl setflag stdout::");
  }

  if( (fdflags = fcntl(pipefderr[0],F_GETFL)) == -1 ) {
    perror("fcntl getflag stderr::");
  }
  fdflags |= O_NONBLOCK;
  if( fcntl(pipefderr[0],F_SETFL,fdflags)  == -1 ) {
    perror("fcntl setflag stderr::");
  }

  nfds = nemptystat = 0; teststat = pstatistict = NULL; 
  waittime = 0;
  timeout = 2*TimeOut;
  nfdescmain = nfdesc = 2; /*stdout and stderr */ 
  ufds[0].fd = pipefd[0];  ufds[1].fd = pipefderr[0];
  ufds[0].events = ufds[1].events = (POLLIN | POLLPRI); pteststat = buf + strlen(buf);
  while( (retpid = waitpid(pid,&status,WNOHANG)) != pid && retpid != -1 ) {
    if( (nfds = poll(ufds,nfdesc,timeout)) > 0  ) {
      for( j = 0; j < nfdesc; j++ ) {
	if( ufds[j].revents != 0 ) {
	  if( (in_bytes = read(ufds[j].fd,tmpbuf,(DefaultSize-1))) > 0 ) {
	    tmpbuf[(in_bytes/sizeof(char))] = '\0';
	    appendlog(buf,tmpbuf,1);
	    waittime = 0;	    
	  }
	  if( (pstatistict = strstr(pteststat,"Running. Press ctrl")) != NULL && pstatistict != pteststat_old) {
	    pteststat_old = pstatistict;
	    pteststat+= strlen("Running. Press ctrl");
	    runRobins(buf);
	    teststat = pstatistict;
	  }
	  
	  if( pstatistict != NULL ) {
	    if( (emptystat = strstr(teststat,statstr)) != NULL ) {
	      teststat = emptystat + strlen(statstr);
	      if( ++nemptystat > 10 ) {
		appendlog(buf,(char*)"WARNING: ONE OF THE LINKS MIGHT BE DOWN\nKilling PCI script...\n",1);
		sprintf(tmpbuf,"kill -9 %d\n",pid);
		system(tmpbuf);
	      }
	    }
	  }
	  

	  if( --nfds == 0 )
	    break;
	}
	
      }
      refreshwind(pid); 
      
    }
    else {
      
      if( (waittime+= timeout) > 180000 ) {
	sprintf(tmpbuf,"ERROR: process %s not responding\n",cmd);
	sprintf(tmpbuf,"kill -9 %d\n",pid);
	system(tmpbuf);
	return -1;
      }
      /*sprintf(tmpbuf,"wait time %d\n",waittime);
	appendlog(buf,tmpbuf,1);*/
      refreshwind(pid);
    }
  
    /* check main program pipes */
    /*while( (nfdsmain = poll(ufdsloc,nfdescmain,timeout)) > 0 ) {
      for( j = 0; j < nfdescmain; j++ ) {
	if( ufdsloc[j].revents != 0 ) {
	  if( (in_bytes = read(ufdsloc[j].fd,tmpbuf,DefaultSize)) > 0 ) {
	    tmpbuf[in_bytes] = '\0';
	    appendlog(buf,tmpbuf,1);
	  }
	  if( --nfdsmain == 0 )                                                                   
	    break; 
	}
      }
      }*/
    refreshwind(pid);
  }

  /* left overs from the child process (if any) */
  /*while( (nfds = poll(ufds,nfdesc,timeout)) > 0  ) {*/
    for( j = 0; j < nfdesc; j++ ) {
      if( ufds[j].revents != 0 ) {
	if( (in_bytes = read(ufds[j].fd,tmpbuf,(DefaultSize-1))) > 0 ) {
	  tmpbuf[(in_bytes/sizeof(char))] = '\0';
	  appendlog(buf,tmpbuf,1);
	}
      }
      if( --nfds == 0 )
	break;
    }
  /* } */

  /*while( (nfdsmain = poll(ufdsloc,nfdescmain,TimeOut)) > 0 ) {
    for( j = 0; j < nfdescmain; j++ ) {
      if( ufdsloc[j].revents != 0 ) {
	if( (in_bytes = read(ufdsloc[j].fd,tmpbuf,DefaultSize)) > 0 ) {
	  tmpbuf[in_bytes] = '\0';
	  appendlog(buf,tmpbuf,1);
	}
	  if( --nfdsmain == 0 )                                                                   
	    break; 
      }
    }
    }*/

  /* stdout */
  while( (in_bytes = read(pipefd[0],tmpbuf,(DefaultSize -1))) > 0 ) {
    tmpbuf[(in_bytes/sizeof(char))] = '\0';
    appendlog(buf,tmpbuf,1);
  }
    
  /*stderr */
  while( (in_bytes = read(pipefderr[0],tmpbuf,(DefaultSize -1))) > 0 ) {
    tmpbuf[(in_bytes/sizeof(char))] = '\0';
    appendlog(buferr,tmpbuf,1);
  }

  if( close(pipefd[0]) || close(pipefd[1]) || close(pipefderr[0])  )
    appendlog(buferr,(char*)"closing executedup pipes:",1);

  
  if( strstr(buf,"No such file or directory") != NULL || strstr(buf,"Command not found") != NULL ) {
    sprintf(tmpbuf,"ERROR: no such file or directory when executing: %s\n",cmd);
    appendlog(buferr,tmpbuf,1);
    return -1;
  }

  if( nemptystat >= 10 ) {
    appendlog(buf,(char*)"WARNING: PCI script has been killed - to many pending events.\nCheck optical links\n\n",1);
  }

  if( WIFEXITED(status) ) 
    return( WEXITSTATUS(status) );
  else      
    return(pid);
}

int opentty(int *tty_fd, char *device) {
  struct termios ttyter;

  if( (*tty_fd = open(device,(O_RDWR|O_NONBLOCK|O_NOCTTY))) == -1 ) { 
    perror(device);
    return -1;
  }

  /* modify parameters of tty device */  
  tcgetattr(*tty_fd,&ttyter);

  ttyter.c_iflag =  (IGNCR | ICRNL); 
  ttyter.c_oflag = ( OCRNL );

  ttyter.c_cflag = (ttyter.c_cflag);
  /* allow single character input */
  ttyter.c_lflag = ((ttyter.c_lflag & ~ICANON) & ~ECHO);

  cfsetispeed(&ttyter,B115200);
  cfsetospeed(&ttyter,B115200);
  tcsetattr(*tty_fd, TCSANOW, &ttyter);

  return 0;
}

int openallttyPci(int ncards,int **ttyfd) {
  char ttydev[DefaultSize];
  const char *ttyd = "/dev/robin_tty";
  int icrd,openok,nusb;
  
  if( *ttyfd != NULL ) 
    return -1;

  if( ((*ttyfd) = malloc(ncards*sizeof(int)) ) == NULL )
    return -1;

  openok = 0; nusb = 0;
  for( icrd = 0; icrd < ncards; icrd++ ) {
    sprintf(ttydev,"%s%d",ttyd,icrd);
    if( opentty(&((*ttyfd)[icrd]),ttydev) == 0 )
      openok++;
  }  
  return openok;
}

int runppc_select(int tty_fd,char *command,char *buflog) {
  struct pollfd ufds;
  short brkapp;
  unsigned int istr;
  int nfds,count,timeout;
  ssize_t  in_byte;
  char io_buf[DefaultSize],loclog[MAX_BYTE];
  char *logpos,*pcmd,*brk_pos,*except_pos;
  /*size_t shift_pos;*/
  const char *act_str[] = {"=>","Entering main loop\n","x : Continue",
			   "Entering main loop. Press any key to enter user interface",
			   "Application terminated"};
  int appstat;

  if( tty_fd <= 0 ) {
    fprintf(stderr,"\nrunppc: tty device not open, or open failed\n");
    return -2;
  }  
  tcflush(tty_fd,TCIOFLUSH);

  strcpy(io_buf,command);
  if( strlen(io_buf) > 1  && strstr(io_buf,"\r") == NULL )
    strcat(io_buf,"\r");
  write(tty_fd,io_buf,(strlen(io_buf)*sizeof(char)));
  tcdrain(tty_fd);

  count = -40;
  /*if( strstr(command,"saveenv") != NULL )
    count = 4;
  if( strstr(command,"bootelf") != NULL )
    count = 20;
  if( strstr(command,"reset") != NULL )
  count = 20;*/
    /*count = 20;*/
  if( strcmp(command,"\r") == 0 ) 
    timeout = 500;
  else
    timeout = count*TimeOut;

  ufds.fd = tty_fd;
  ufds.events = (POLLIN | POLLPRI);
  loclog[0] = '\0'; logpos = loclog; brkapp = 0; in_byte = 10; appstat = 0;
  while( (nfds = poll(&ufds,1,timeout)) > 0  ) {
    if( StopAll ) {
      StopAll = 0;
      break;
    }

    if( (in_byte = read(ufds.fd,io_buf,(DefaultSize-1))) > 0 ) {
      io_buf[(in_byte/sizeof(char))] = '\0';
      appendlog(loclog,io_buf,1);
    }
    refreshwind(-1);
   
    if( (strstr(loclog,"stop autoboot") != NULL && (pcmd = strstr(command,"reset")) == NULL) 
	|| strstr(loclog,"Bus Fault") != NULL ) {
      write(tty_fd,"\r\r",sizeof(char));
      tcdrain(tty_fd);
      logpos = logpos + strlen(loclog);
      break;
    }
    for( istr = 0; istr < (sizeof(act_str)/sizeof(act_str[0])); istr++ )
      if( (brk_pos = strstr(logpos,act_str[istr])) != NULL  ) {
	except_pos = strstr(logpos,"=> data");
	if( brk_pos > except_pos )
	  brkapp = 1;
	if( strstr(logpos,"Application terminated") != NULL )
	  appstat = 1;
      }

    /*appendlog(buflog,logpos,0);*/
    if( brkapp == 1 ) {
      break;    
    }
    /* logpos = logpos + shift_pos; */
  }
  appendlog(buflog,loclog,0);

  tcflush(tty_fd,TCIFLUSH);
  
  return appstat;
}

int readppcout(int ncards,int *ttyfd,char *logapp) {
  struct pollfd ufds;
  short brkapp;
  unsigned int istr;
  int nfds,icrd,ttyinp;
  ssize_t  in_byte;
  char io_buf[DefaultSize];
  char *logpos;
  const char *act_str[] = {"=>","Entering main loop\n","x : Continue",
			   "Entering main loop. Press any key to enter user interface"};

  if( ttyfd == NULL ) {
    appendlog(logapp,(char*)"Error: tty not open\n",1);
    return -1;
  }

  logpos = logapp + strlen(logapp);
  ufds.events = (POLLIN | POLLPRI);
  if( ttyfd != NULL ) {
    for( icrd = 0; icrd < ncards; icrd++ ) {
      appendlog(logapp,(char*)"\n",1);
      
      logpos = logpos + strlen(logapp);
      ufds.fd = ttyfd[icrd]; ttyinp = 0; brkapp = 0;
      while( (nfds = poll(&ufds,1,10*TimeOut)) > 0 ) {
	if( StopAll ) {
	  StopAll = 0;
	  break;
	}
	ttyinp++;
	if( (in_byte = read(ttyfd[icrd],io_buf,DefaultSize)) > 0 ) {
	  io_buf[(in_byte/sizeof(char))] = '\0';
	  appendlog(logapp,io_buf,1);
	}
	else
	  break;
	if( strstr(logpos,"to stop autoboot") != NULL || strstr(logpos,"Unknown command") != NULL ) {
	  strcpy(io_buf,"\r");
	  write(ttyfd[icrd],io_buf,(strlen(io_buf)*sizeof(char)));
	  tcdrain(ttyfd[icrd]);
	  io_buf[0] = '\0';
	  logpos = logapp + strlen(logapp);
	}

	for( istr = 0; istr < (sizeof(act_str)/sizeof(act_str[0])); istr++ )
	  if( strstr(logpos,act_str[istr]) != NULL ) 
	    brkapp = 1;
	if( brkapp )
	  break;	
      }
      if( ttyinp == 0 && nfds == 0 ) 
	appendlog(logapp,(char*)"No more tty input\n",0);
    }
  }
  else
    appendlog(logapp,(char*)"Warning:: tty not opened",1);

  return 0;
}

int loadppc(int ncards,int *ttyfd,char *logapp, char *errlog, short *cardSelect) {
  int icrd,statapp;
  char cmd[DefaultSize],loclog[MAX_BYTE],tmpbuf[DefaultSize];
  short crd_err;
  char *logpos;

  if( ttyfd ==  NULL ) {
    appendlog(errlog,(char*)"No tty open, cannot talk to the cards\n",1);
    return -1;
  }

  statapp = 0;
  for( icrd = 0; icrd < ncards; icrd++ ) {
    if( cardSelect[icrd] ) {
      sprintf(tmpbuf,"\n\n\t--------  Downloading elf file to CARD %d --------\n",(icrd+1));
      appendlog(logapp,tmpbuf,1);
      loclog[0] = '\0';
      crd_err = 0;

      /* check connection to the card and run kermit*/
      if( !pingrobin(ttyfd[icrd],loclog) ) {
	logpos = logapp + strlen(logapp);
	runppc_select(ttyfd[icrd],(char*)"loadb",logapp);
	
	sprintf(cmd,"kermit .snd_kermrc%d",icrd);
	executedup(cmd,logapp,errlog);
	if( pingrobin(ttyfd[icrd],loclog) ) 
	  crd_err = 1;
	else {
	  /* check the output from the cards */
	  readppcout(1,&ttyfd[icrd],logapp); 
	}

	if( checkerr(logpos,errlog,kermerr,1,1) != 0 ) {
	  appendlog(errlog,logapp,0);
	  statapp--;
	  crd_err = 1;
	}

      }
      else {
	sprintf(cmd,"\nWarning:: Cannot astablish connection to the card %d ppc not loaded\n",(icrd+1));
	appendlog(errlog,cmd,1);
	crd_err = 1;
      }
      setstatus(icrd,crd_err,logapp);
    }
  }

  return 0;
}

int findStr(char *teststring,char **fieldLabels, int nfields) {
  int iwid;

  for( iwid = 0; iwid < nfields; iwid++ )
    if( strstr(teststring,fieldLabels[iwid]) != NULL )
      break;

  return iwid;
}

void getmac(char *mac, char *pbuf) {
  int tesv,count,dcount;
  
  while( *pbuf == ' ' ) 
    pbuf++;

  dcount = count = 0;
  while( (*pbuf != ' ' && *pbuf != '\0' && *pbuf != '\n') && count < (IDlen-1) ) {
    if( (*pbuf >= '0' && *pbuf <= '9') || (tolower(*pbuf) >= 'a' && tolower(*pbuf) <= 'f') ) {
      mac[count] = *pbuf;
      if( (tesv = (dcount%2)) && count != 0 )
	mac[++count] = ':';
      count++;
      dcount++;
    }
    pbuf++;
  }
  mac[count-1] = '\0';
}

void getip(char *mac, char *pbuf) {
  char cline[DefaultSize];
  char *endline;
  char *args[Max_NumItems];

  endline = strstr(pbuf,"\n");
  strncpy(cline,pbuf,((endline - pbuf)*sizeof(char)));
  cline[((endline - pbuf)/sizeof(char))] = '\0';
  splitarg(cline,args);
  sprintf(mac,"%s",args[0]);
}

void writepipelog(struct pollfd *ufds,int fd,char* logapp) {
  int in_bytes;
  int nfds;
  char tmpbuf[DefaultSize];

  while( (nfds = poll(ufds,1,TimeOut)) > 0 ) {
    if( (in_bytes = read(fd,tmpbuf,DefaultSize)) > 0 ) {
      tmpbuf[in_bytes] = '\0';
      appendlog(logapp,tmpbuf,0);
    }
  }
}

int pingrobin(int ttyfd,char *logapp) {
  struct pollfd ufds;
  int nfds,ttyinp,repeat;
  ssize_t  in_byte;
  char io_buf[DefaultSize],loclog[MAX_BYTE];
  char *logpos,*tmpbuf;

  if( ttyfd <= 0 ) {
    appendlog(logapp,(char*)"Error: File descriptor invalid, check tty connection to the card\n",1);
    return -1;
  }

  ufds.events = (POLLIN | POLLPRI);
  ufds.fd = ttyfd; ttyinp = 0;

  loclog[0] = '\0'; repeat = 0; logpos = loclog;
  while( strstr(loclog,"=>") == NULL && strstr(loclog,"Unknown") == NULL && repeat++ < 10 ) {
    write(ttyfd,"\r",sizeof(char));
    tcdrain(ttyfd);
      
    while( (nfds = poll(&ufds,1,500)) > 0 ) {
      if( (in_byte = read(ufds.fd,io_buf,DefaultSize)) > 0 ) {
	io_buf[(in_byte/sizeof(char))] = '\0';
	appendlog(loclog,io_buf,1);
	ttyinp++;
      }
      else 
	break;

     if( (strstr(logpos,"Unknown command")) != NULL ) {
       runppc_select(ttyfd,(char*)"\r",loclog);
       logpos = loclog + strlen(loclog);
     }
    }
    if( ttyinp == 0 && repeat > 5 ) {
      appendlog(logapp,(char*)"No tty input\n",1);
      return -1;
    } 
 
    if( (tmpbuf = strstr(loclog,"Select user command by number")) != NULL ) {
      loclog[0] = '\0';
      runppc_select(ttyfd,(char*)"0",logapp);
      appendlog(logapp,(char*)"ping is terminating ppc application\n",1);
    }
  }
  /*appendlog(logapp,"ping: ",0);
    appendlog(logapp,loclog,0);*/
  tcflush(ttyfd,TCIFLUSH);

  if( repeat >= 10 )
    return -1;
  return 0;
}

int kermitconfig(int icon,char *dev) {
  FILE *fdcon;
  const char *conline[] = {
    "set speed 115200","set carrier-watch off","set handshake none",
    "set flow-control none","robust","\n","set file type bin","set file name lit",
    "set rec pack 1000","set send pack 1000","set window 5","\n","send ./robin.elf","quit"};
  char tmpbuf[DefaultSize];
  unsigned int iline;

  sprintf(tmpbuf,"./.snd_kermrc%d",icon);
  if( (fdcon = fopen(tmpbuf,"w")) == NULL ) { 
    perror(tmpbuf);
    return -1;
  }

  sprintf(tmpbuf,"set line %s\n",dev);
  fprintf(fdcon,"%s",tmpbuf);
  for( iline = 0; iline < (sizeof(conline)/sizeof(conline[0])); iline++ ) 
    fprintf(fdcon,"%s\n",conline[iline]);

  if( fclose(fdcon) ) {
    perror("kermit config:");
    return -1;
  }

  return 0;
}

int findactivetty(int ncards, int nsertty,int **actty,char *currentLog, char *errlog,
		  robinInfo_t *cardorder,short ttyselect,short *cardSelect) {
  int fd,itty,tty_type,icrd,serNum,iser;
  char ttydev[DefaultSize],loclog[MAX_BYTE];
  const char *ttyd_def1[] = {"/dev/usb/ttyUSB","/dev/ttyS","/dev/ttyUSB"};
  const char *ttyd_def2[] = {"/dev/robintty_","/dev/robin_tty","/dev/tty_robin"};
  char **ttyd;
  int max_tty,crd_status,config_status;
  struct stat dirstat;

  config_status = crd_status = 0;
  if( ttyselect ) {
    ttyd = (char**)ttyd_def2;
    max_tty = sizeof(ttyd_def2)/sizeof(ttyd_def2[0]);
    if( (config_status = initRobinMsg(ncards,currentLog,errlog,cardSelect)) ) {
      appendlog(currentLog,(char*)"ERROR: configuration failed\n",1);
    }
  }
  else {
    ttyd = (char**)ttyd_def1;
    max_tty = (sizeof(ttyd_def1)/sizeof(ttyd_def1[0]));
  }

  loclog[0] = '\0';
  if( (*actty) != NULL ) {
    appendlog(currentLog,(char*)"tty already opened\n\n",1);
    return 0;
  }
  else {
    if( ((*actty) = calloc(ncards,sizeof(int))) == NULL ) {
      perror("ttylist:");
      return -1;
    }
  }

  icrd = 0; 
  for( tty_type = 0; tty_type < max_tty; tty_type++ ) {
    nsertty = 10;
    for( itty = 0; itty < nsertty; itty++ ) {
      sprintf(ttydev,"%s%d",ttyd[tty_type],itty);
      /* don't bother if the device does not exists */
      if( stat(ttydev,&dirstat) == 0 ) {
	if( opentty(&fd,ttydev) == 0 ) {
	  if( !pingrobin(fd,currentLog) ) { 
	    appendlog(currentLog,(char*)"Found active serial tty ",1); appendlog(currentLog,ttydev,1);
	    serNum = getserial(fd,currentLog);
	    appendlog(currentLog,(char*)"\n",1);
	    
	    for( iser = 0; iser < ncards; iser++ ) {
	      /*if( (*actty)[iser] != 0 ) 
		close((*actty)[iser]);*/
	      if( cardorder[iser].boardspec[1] != 0 ) {
		if( cardorder[iser].boardspec[1] == (unsigned int)serNum ) {
		  strcpy(cardorder[iser].ttyused,ttydev);
		  (*actty)[iser] = fd;
		  setstatus(iser,0,currentLog);
		  /* close(fd); */
		  break;
		}
	      }
	      else {
		cardorder[iser].boardspec[1] = serNum;
		strcpy(cardorder[iser].ttyused,ttydev);
		(*actty)[iser] = fd;
		setstatus(iser,0,currentLog);
		/* close(fd); */
		break;
	      }
	    }
	    if( kermitconfig(iser,ttydev) != 0 ) 
	      appendlog(currentLog,(char*)"Cannot write kermit config file",1);
	    if( ++icrd == ncards ) 
	      return 0;
	  }
	  else {
	    appendlog(currentLog,ttydev,1);
	    appendlog(currentLog,(char*)": no input from this device, closing\n",1);
	    appendlog(loclog,ttydev,0);
	    appendlog(loclog,(char*)": no input from this device, closing\n",0);
	    if( close(fd) != 0 ) 
	      perror("close serial:");
	  }
	}
	else {
	  appendlog(currentLog,(char*)"Error: open failed for ",1);
	  appendlog(currentLog,ttydev,1); 
	  appendlog(currentLog,(char*)" check ownershit/group and permissions for the device\n",1);
	}
	
      }
    }
  }

  if( (ncards-icrd) == 0 ) 
    appendlog(currentLog,(char*)"All ttydev open\n",1); 
  else {
    for( iser = 0; iser < ncards; iser++ ) {
      if( (*actty)[iser] == 0 ) {
	sprintf(ttydev,"Error: card serial %d: no serial connection\n Check the cable (if connected via the cable)\n",
		cardorder[iser].boardspec[1]);
	appendlog(currentLog,ttydev,1);
	setstatus(iser,1,currentLog);
	cardSelect[iser] = 0;
	crd_status++;
      }
    }
    appendlog(errlog,loclog,1);
    sprintf(ttydev,"Error: %d ttydev was/were not opened.\n",(ncards-icrd));
    appendlog(currentLog,ttydev,1);
    appendlog(currentLog,(char*)"If the serial port (USB/tty serial) is used, check the cable connection\n",1);
    appendlog(currentLog,(char*)"If the ttydriver is selected, check if the driver is loaded.\n",1);
    appendlog(currentLog,(char*)"If the driver is loaded, and connection cannot be established,\n this might indicate problem with Message Passing\n",1);
    appendlog(errlog,ttydev,0);
  }

  if( (crd_status+config_status) != 0 ) 
    for( iser = 0; iser < ncards; iser++ )
      if( cardSelect[iser] )
	return 0; /* there are cards active, reset tty error */

  return (crd_status+config_status); 
}

int getserial(int ttyfd,char *logapp) {
  char loclog[MAX_BYTE],sernum[DefaultSize];
  char *pbuf,*pbufend;
  unsigned int crdSernum,j;

  loclog[0] = '\0';
  runppc_select(ttyfd,(char*)"printenv",loclog);

  if( (pbuf = strstr(loclog,"sernum=")) != NULL ) {
    if( (pbufend = strstr(pbuf,"\n")) != NULL ) {
      *(++pbufend)= '\0';
      sprintf(sernum,"%s",pbuf);
    }
    else {
      strncpy(sernum,pbuf,(strlen("sernum=")+strlen(serial_prefix[0][0])));
      sernum[strlen("sernum=")+strlen(serial_prefix[0][0])+1] = '\0';
    }
    appendlog(logapp,(char*)" Serial number ",1); 
    appendlog(logapp,sernum,1);

    pbuf = strstr(sernum,"=") + 1;
    for( j = 0; j < (sizeof(serial_prefix)/sizeof(serial_prefix[0])); j++ ) 
      if( strncmp(pbuf,serial_prefix[j][0],strlen(serial_prefix[j][0])) == 0 ) 
	pbuf = pbuf + strlen(serial_prefix[j][0]);
    sscanf(pbuf,"%ud",&crdSernum);
  }
  else {
    appendlog(logapp,(char*)" No serial number available",1);
    return -1;
  }
  
  return crdSernum;
}

int initRobinMsg(int Ncrd,char *cardlog,char *errlog,short *cardSelect) {
  int icrd;
  char cmd[DefaultSize],tmpstr[DefaultSize],*loclog;
  const char *failmsg = "INFO: initRobinMsg failed. Check the amount of bigphys memory allocated\n" \
    "INFO: This error might be caused due to too small bigpys area and/or two many cards in the system\n" \
    "INFO: Try to reduce number of cards in the test system\n";
  int card_stat;
  
  loclog = cardlog + strlen(cardlog);
  sprintf(cmd,"cat /proc/robin");
  if( executedup(cmd,loclog,loclog) < 0 ) 
    return -1;

  card_stat  = 0;
  for( icrd = 0; icrd < Ncrd; icrd++ ) {
    if( cardSelect[icrd] ) {
      if( loclog != NULL && strlen(loclog) > 0 ) {
	sprintf(cmd,"There are 0 FIFO slots available on card %d",icrd);
	if( strstr(loclog,cmd) != NULL ) {
	  sprintf(cmd,"robinconfig %d 5",icrd);
	  if( executedup(cmd,loclog,loclog) < 0 || strstr(loclog,"exception") != NULL ) {
	    sprintf(tmpstr,"Card %d:\n%s\n",(icrd+1),failmsg);
	    appendlog(errlog,tmpstr,1);
	    if( cardSelect[icrd] ) {
	      setstatus(icrd,1,cardlog);
	      card_stat++;
	      cardSelect[icrd] = 0;
	    }
	  }
	} 
	else
	  appendlog(cardlog,(char*)"Card already configured\n",1);
      }
      else {
	appendlog(cardlog,(char*)"ERROR: cat /proc/robin failed\n",1);
	if( loclog == NULL )
	  appendlog(cardlog,(char*)"ERROR: too few entrues in /proc/robin\n",1);
	return -1;
      }
    }
  }    

  return card_stat;
}

int closeSerial(int **fdtty, int ncards) {
  unsigned int icrd;

  if( *fdtty != NULL ) {
    for( icrd = 0; icrd < (unsigned int)ncards; icrd++ )
      if( (*fdtty)[icrd] > 0 ) 
	if( close((*fdtty)[icrd]) ) {
	  return -1;
	}
    free(*fdtty);
    *fdtty = NULL;
  }
  return 0;
}

int driverUseCheck(char *log,char *errlog) {
  char tmpstr[DefaultSize];
  const char *drivername[] = {"robintty"};
  const char *cmd = "/sbin/lsmod";
  char *loclog,*pdriver;
  char *args[Max_NumItems];
  unsigned int idrv,drvfound;
  int nargs,usecount,cpos;

  loclog = log + strlen(log);
  if( executedup((char*)cmd,log,errlog) < 0 ) {
    appendlog(log,(char*)"Error: cannot read driver list\n",1);
    appendlog(errlog,(char*)"Error: cannot read driver list\n",0);
    return -1;
  }
  
  drvfound = 0;
  for( idrv = 0; idrv < (sizeof(drivername)/sizeof(drivername[0])); idrv++ ) {
    if( (pdriver = strstr(loclog,drivername[idrv])) != NULL ) {
      drvfound++;
      sprintf(tmpstr,"%s",pdriver);
      nargs = splitarg(tmpstr,args);
      cpos = 2;
      if( cpos < nargs ) {
	sscanf(args[cpos],"%d",&usecount);
	if( usecount != 0 ) {
	  sprintf(tmpstr,"Error: driver %s in use, cannot continue\n",drivername[idrv]);
	  appendlog(log,tmpstr,1);
	  appendlog(errlog,tmpstr,0);
	  return -1;
	}
      }
      else {
	appendlog(log,(char*)"Error: cannot read driver use count\n",1);
	appendlog(errlog,(char*)"Error: cannot read driver use count\n",0);
	return -1;
      }
    }
    else {
      sprintf(tmpstr,"ERROR: Cannot find driver %s check run-time options\n",drivername[idrv]);
      return -1;
    }
  }
  
  return 0;
}

int pciscript(char *filename,char *log,char *errlog,int dolar,unsigned int *boardsinfo) {
  FILE *fin;
  /* struct stat statb; */
  char tmpstr[DefaultSize],addstr[DefaultSize];
  char *pstr,*pcont,*pcontbuf,*pci,*pcitmp;
  char *file_text;
  const char *script_txt1 = {"spawn test_Robin [lrange $argv 0 1]\nset dev [lrange $argv 0 0]\n" \
			     "set test_size [lrange $argv 1 1]\nset ctrplus \"\\034\"\n\nexpect \" ? :\"\n" \
			     "send \"3\\n\"\nexpect \"Enter the value for <PhysicalAddress> \"\n" \
			     "expect \":\"\nsend \"$dev\\n\"\n" \
			     "expect \"Enter the value for <EbistEnabled>\"\nexpect \":\"\n" \
			     "send \"0\\n\"\nexpect \"Enter the value for <RevisionNumber>\"\n"\
			     "expect \":\"\nsend \"60011\\n\"\n" \
			     "expect \"Enter the value for <FPGAVersion>\"\nexpect \":\"\n" \
			     "send \"408300d\\n\"\nexpect \"Enter the value for <Timeout>\"\n"};
  const char *script_txt2 = {"expect \":\"\nsend \"10\\n\"\nexpect \"Enter the value for <ResetRobin>\"\n" \
			     "expect \":\"\nsend \"0\\n\"\nexpect \"Enter the value for <numberOfChannels>\"\n" \
			     "expect \":\"\nsend \"3\\n\"\n" \
			     "expect \"Enter the value for <NumberOfOutstandingReq>\"\nexpect \":\"\n" \
			     "send \"20\\n\"\nexpect \"Enter the value for <MsgInputMemorySize> in bytes\"\n" \
			     "expect \":\"\nsend \"2000\\n\"\n" \
			     "expect \"Enter the value for <MiscSize> in bytes\"\nexpect \":\"\n" \
			     "send \"2000\\n\"\nexpect \"Enter the value for <Interactive>\"\n" \
			     "expect \":\"\nsend \"1\\n\"\nexpect \"Enter the value for <Prescalefrag>\"\n"};
  const char *script_txt3 = {"expect \":\"\nsend \"3\\n\"\nexpect \"Enter the value for <SubDetectorId>\"\n" \
			     "expect \":\"\nsend \"51\\n\"\nexpect \"Enter the value for <Pagesize>\"\n" \
			     "expect \":\"\nsend \"512\\n\"\nexpect \"Enter the value for <Numpages>\"\n" \
			     "expect \":\"\nsend \"32768\\n\"\nexpect \"Enter the value for <MaxRxPages>\"\n" \
			     "expect \":\"\nsend \"16\\n\"\n\n" \
			     "for { set ichan 0} {$ichan<3} {incr ichan} { \n" \
			     "expect \"Enter the value for <ChannelId#$ichan>\"\nexpect \":\"\n" \
			     "send \"$ichan\\n\"\nexpect \"Enter the value for <ROLPhysicalAddress#$ichan>\"\n" \
			     "expect \":\"\nsend \"$ichan\\n\"\n"};
  const char *script_txt4 = {"expect \"Enter the value for <memoryPoolNumPages#$ichan>\"\nexpect \":\"\n" \
			     "send \"10\\n\"\nexpect \"Enter the value for <memoryPoolPageSize#$ichan> in bytes\"\n" \
			     "expect \":\"\nsend \"32848\\n\"\n" \
			     "expect \"Do you want to enter more ROL specific ROBIN configuration parameters (1=yes  0=no)\"\n" \
			     "expect \":\"\nsend \"1\\n\"\nexpect \"Enter the value for <RolDataGen#$ichan>\"\n"};
  const char *script_txt5 = {"expect \":\"\nsend \"1\\n\"\nexpect \"Enter the value for <Rolemu#$ichan>\"\n" \
			     "expect \":\"\nsend \"0\\n\"\nexpect \"Enter the value for <TestSize#$ichan>\"\n" \
			     "expect \":\"\nsend \"$test_size\\n\"\n" \
			     "expect \"Enter the value for <Keepfrags#$ichan>\"\n" \
			     "expect \":\"\nsend \"0\\n\"\nexpect \"Enter the value for <ForceL1ID#$ichan>\"\n" \
			     "expect \":\"\nsend \"0\\n\"\n" \
			     "expect \"Enter the value for <CrcCheckInterval#$ichan>\"\n" \
			     "expect \":\"\nsend \"\\n\"\n" \
			     "}\n\n" \
			     "#expect \"Config: key --> numberOfChannels        Config: value -->\"\nsleep 2;\n\n" 
			     };
  const char *script_txt6 = {"expect \"Enable debug mode \"\nexpect \":\"\nsend \"0\\n\"\nexpect \"Enable data checking \"\n" \
			     "expect \":\"\nsend \"1\\n\"\nexpect \"Dump raw data \"\nexpect \":\"\nsend \"0\\n\"\n"
			     "expect \"How many events do you want to process\"\nexpect \":\"\nsend \"500000\\n\"\n\n"\
			     "expect \"Running.\"\n\n"
			     "set done 0\nwhile { $done==0 } { \n# sleep 2;\n send $ctrplus\n expect {\n" \
			     "    \"Time per event =\" {\n     set done 1\n   }\n }\n}\n\nexpect \"? :\"\n" \
			     "send \"100\\n\"\n\nif { $done==1 } {\n  send \"end of program\\n\"\n}\n\n" \
			     "expect eof\n\nexit\n\n"};
  const char *lookfor[] = {"<RolDataGen#$ichan>\"\nexpect \":\"\nsend \"",
			   "<RevisionNumber>\"\nexpect \":\"\nsend \"",
			   "<FPGAVersion>\"\nexpect \":\"\nsend \"",NULL};
  unsigned int ilab,idesign,tlen,headlen;

  headlen = (strlen(script_txt1)+strlen(script_txt2)+strlen(script_txt3)+strlen(script_txt4)+
	     strlen(script_txt5)+strlen(script_txt6)+1)*sizeof(char);
  if( !(file_text = (char*)malloc(headlen)) ) {
    sprintf(tmpstr, "ERROR: Can't alloc enough space for file_text script\n");
    appendlog(errlog,tmpstr,1);
    return -1;
  }
  sprintf(file_text,"%s%s%s%s%s%s",script_txt1,script_txt2,script_txt3,script_txt4,script_txt5,script_txt6);
  
  ilab = 0; idesign = 4; pci = file_text;
  while( lookfor[ilab] != NULL ) {
    if( (pstr = strstr(pci,lookfor[ilab])) != NULL ) {
      pstr+= strlen(lookfor[ilab]);
      headlen = tlen = (pstr - pci)/sizeof(char);

      pcontbuf = NULL;
      if( (pcont = index(pstr,'\n')) != NULL ) {
	pcont+= strlen("\n");
	pcontbuf = malloc(((strlen(pcont)+1)*sizeof(char)));
	strcpy(pcontbuf,pcont);
	tlen+= strlen(pcontbuf);
      }
      
      if( ilab == 0 ) {
	if( dolar ) 
	  sprintf(addstr,"0\\n\"\n");
	else
	  sprintf(addstr,"1\\n\"\n");
      }
      else
	sprintf(addstr,"%x\\n\"\n",boardsinfo[idesign++]);
      tlen+= strlen(addstr);

      /*fprintf(stderr,"found %s, line %s\n",lookfor[ilab],addstr);*/

      pcitmp = malloc((tlen+1)*sizeof(char));
      strncpy(pcitmp,pci,headlen*sizeof(char));
      pcitmp[headlen] = '\0';
      strcat(pcitmp,addstr);
      if( pcontbuf != NULL ) {
	strcat(pcitmp,pcontbuf);
	free(pcontbuf);
      }
      free(pci);
      pcitmp[tlen] = '\0';
      
      pci = malloc((tlen+1)*sizeof(char));
      strcpy(pci,pcitmp);
      free(pcitmp);

    }
    else {
      sprintf(tmpstr, "ERROR: Problem with the pci script: cannot find a %s flag!\n",lookfor[ilab]);
      fprintf(stderr,"ERROR: Problem with the pci script: cannot find a %s flag!\n",lookfor[ilab]);
      fprintf(stderr,"%s\n",file_text);
      appendlog(errlog,tmpstr,1);
      return -1;
    }
    ilab++;
  }


  if( (fin = fopen(filename, "w")) == NULL ) {
    sprintf(tmpstr,"Cannot create %s\n",filename);
    appendlog(errlog,tmpstr,1);
    return -1;
  }

  if( !fwrite(pci,sizeof(char), strlen(pci),fin) ) {
   fprintf(stderr, "Error: cannot write pci script file!\n");
   return -1;
  }

  free(pci);
  fclose(fin);
  
  log = log; /* unused */
	 
  return 0;
}

int checRepeat(char *log, char *strappend) {
  char *last_start,*last_end,*prev_end,*pos,*appendlist;
  char **argstline;
  char tmpstr[DefaultSize];
  int strsize,iline,largs,space_left;
  int repeated,nlines,ncopy;
  const int minimufree = strlen("\n\tLOG FULL !!!\n");  

  if( log == NULL ) 
    return -1;

  if( strstr(log,"LOG FULL") != NULL ) 
    return -1;

  if( (space_left = (MAX_BYTE - (strlen(log) + strlen(strappend)))) < minimufree ) {
      strncat(log,strappend,(MAX_BYTE-(minimufree+strlen(log))));
      strcat(log,"\n\tLOG FULL !!!\n");
      return -1;
  }

  if( (last_end = rindex(log,'\n')) == NULL || strlen(strappend) <= 1 ) {
    strcat(log,strappend);
    return 0;
  }

  if( (appendlist = malloc((strlen(strappend)+1)*sizeof(char))) == NULL ) 
    return -1;
  strcpy(appendlist,strappend);

  last_start = pos = log;
  while( (prev_end = strstr(pos,"\n")) != NULL && prev_end != last_end  && (strsize = last_end - prev_end) > 1 ) {
    pos+=strlen("\n");
    last_start = prev_end + 1;
  }

  nlines = checkNlines(appendlist);
  if( (argstline = malloc((nlines+1)*sizeof(char*))) == NULL ) 
    return -1;

  largs = split2lines(appendlist,argstline); repeated = 0;
  for( iline = 0; iline < (largs-1); iline++ ) {
    if( (int)(argstline[iline+1]-argstline[iline]) > 1 ) { 
      /* fprintf(stderr,"last_start:\n %s\nNew text:\n%s",last_start,argstline[iline]); */
      if( strncmp(last_start,argstline[iline],strlen(last_start)) != 0 ) {
	if( repeated > 1 ) {
	  sprintf(tmpstr,"Last line repeated %d times\n",repeated);
	strcat(log,tmpstr);
	repeated = 0;
	}
	ncopy = argstline[iline+1]-argstline[iline];
	last_start = log + strlen(log);
	strncat(log,argstline[iline],ncopy);
	/* strcat(log,"\n"); */
	/* last_start = argstline[iline]; */
      }
      else {
	if( strlen(argstline[iline]) > 1 )
	  repeated++;
      }
    }
    /*else
      strncat(log,argstline[iline],(size_t)(argstline[iline+1]-argstline[iline]));*/
  }
  strcat(log,argstline[iline]);
  free(argstline);
  free(appendlist);

  return 0;
}

int chckpci(char *log, int max_cards,robinInfo_t *cardsinfo) {
  const char *lookfor[] = {"CERN/ECP/EDU: Unknown device 0144","(rev ac)","Subsystem: Unknown device 2151:1087"};
  const char *cmd = "/sbin/lspci -vv -d *:144";
  char locallog[MAX_BYTE],tmpstr[DefaultSize10];
  char **argstline;
  int nlines,largs,iline,icrd,crd_err,err_stat;

  locallog[0] = '\0';
  executedup((char*)cmd,locallog,locallog);

  nlines = checkNlines(locallog);
  if( (argstline = malloc((nlines+1)*sizeof(char*))) == NULL ) 
    return -1;
  largs = split2lines(locallog,argstline);
  
  icrd = 0; err_stat = crd_err = 0;
  for( iline = 0; iline < (largs-1); iline+=2 ) {
    if( strstr(argstline[iline],lookfor[0]) != NULL ) {
      if( strstr(argstline[iline],lookfor[1]) == NULL ) {
	sprintf(tmpstr,"Card %d has wrong revision number:\n",icrd);
	appendlog(log,tmpstr,1);
	appendlog(log,argstline[iline+1],1);
	crd_err = 1;
      }

      if( (iline+1) == (largs-1) ) {
	appendlog(log,(char*)"Unexpected end of lpci info\n",1);
	return -1;
      }
      if( strstr(argstline[iline+1],lookfor[2]) == NULL ) {
	sprintf(tmpstr,"Card %d has wrong subsystem info:\n",icrd);
	appendlog(log,tmpstr,1);
	appendlog(log,argstline[iline+1],1);
	crd_err = 1;
      }

      err_stat-= crd_err;
      crd_err|= cardsinfo[icrd].boardspec[6];
      setstatus(icrd,crd_err,NULL);
      crd_err = 0;
      if( ++icrd == max_cards )
	break;
    }
  }
  free(argstline);
  if( icrd < max_cards ) {
    err_stat-= -1;
    appendlog(log,(char*)"Not all cards seen on the pcibus\n",1);
  }

  return err_stat;
}

int getifname(if_info_t *iflist) {
  struct ifconf ifall;
  struct ifreq *ifcp;
  struct ifreq ifr;
  int ifcout;
  int tsoc;
  unsigned char *hwaddr;

  if( (tsoc = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
    perror("soc_open: ");
    return -1;
  }

  ifall.ifc_len = MAX_IF*sizeof(struct ifreq);
  if( (ifall.ifc_buf = (char*)calloc(sizeof(struct ifreq),ifall.ifc_len)) == NULL ) {
    perror("ifc_buf");
    return -1;
  }

  if( ioctl(tsoc, SIOCGIFCONF,&ifall) < 0 ) {
    perror("ioctl SIOCGIFCONF: ");
    return -1;
  }
  ifcp = (struct ifreq*)(ifall.ifc_req);

  ifcout = 0;
  while( strlen((*ifcp).ifr_name) > 0 ) {
    if( strstr((*ifcp).ifr_name,"lo") == NULL && ifcout < MAX_IF) {
      strcpy(iflist[ifcout].if_name,(*ifcp).ifr_name);
      strcpy(ifr.ifr_name, iflist[ifcout].if_name);

      if( (hwaddr = doioctl(tsoc,SIOCGIFHWADDR, &ifr)) == NULL ) 
	return -1;
      add2str(hwaddr,iflist[ifcout].if_hwadd);

      if( (hwaddr = doioctl(tsoc,SIOCGIFADDR, &ifr)) == NULL ) 
	return -1;
      inetadd2str(hwaddr,iflist[ifcout].if_inetadd);

      ifcout++;
    }
    ifcp+=1;
  }
  free(ifall.ifc_buf);

  if( tsoc >= 0 ) 
    close(tsoc);

  return ifcout;
}

void add2str(unsigned char *addr,char *str_add) {
  char tmp_dump[5];
  int i;

  *str_add = '\0';
  sprintf(tmp_dump,"%02X",(int)*addr++);
  strcat(str_add,tmp_dump);
  for( i = 1; i < 6; i++ ) {
    sprintf(tmp_dump,":%02X",(int)*addr++); 
    strcat(str_add,tmp_dump);
  }
}

void inetadd2str(unsigned char *addr,char *str_add) {
  char tmp_dump[7];
  int i;

  *str_add = '\0';
  for( i = 0; i < 7; i++ ) 
    if( addr[i] != 0 )
      break;
  if( i >=6 ) 
    return;

  for( ; i < 5; i++ ) {
    sprintf(tmp_dump,"%u.",addr[i]);
    strcat(str_add,tmp_dump);
  }
  sprintf(tmp_dump,"%u",addr[i]);
  strcat(str_add,tmp_dump);
}

unsigned char *doioctl(int soc, int req, struct ifreq *ifr) {
  struct sockaddr *saddr;

  if (ioctl(soc, req, (char*)ifr) < 0) {
    perror( "ioctl  error:");
    return NULL;
  }
  saddr = (struct sockaddr*)&(ifr->ifr_hwaddr);
  
  return((unsigned char*)&saddr->sa_data);
}
